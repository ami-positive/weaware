package com.positiveapps.weaware.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.weaware.database.BaseContentProvider;
import com.positiveapps.weaware.database.DatabaseHelper;
import com.positiveapps.weaware.database.tables.TablePersons;

/**
 * Created by Izakos on 04/10/2015.
 */
public class PersonsContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "dev.positiveapps.weaware.android.db.persons.contentprovider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "persons";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TablePersons.TABLE_PERSONS;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TablePersons.COLUMN_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TablePersons.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}
