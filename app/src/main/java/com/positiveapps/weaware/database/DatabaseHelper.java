/**
 * 
 */
package com.positiveapps.weaware.database;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.weaware.database.tables.TablePersons;
import com.positiveapps.weaware.database.tables.TableReports;

/**
 * @author natiapplications
 * 
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	// database name
	private static final String DATABASE_NAME = "weaware.db";
	// database version
	private static final int DATABASE_VERSION = 10;

	

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		TablePersons.onCreate(database);
		TableReports.onCreate(database);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
						  int newVersion) {
		TablePersons.onUpgrade(database, oldVersion, newVersion);
		TableReports.onUpgrade(database, oldVersion, newVersion);
	}

}
