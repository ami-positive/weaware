package com.positiveapps.weaware.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.positiveapps.weaware.database.BaseContentProvider;
import com.positiveapps.weaware.database.ContentProvaiderDataSourseAdapter;
import com.positiveapps.weaware.database.providers.PersonsContentProvider;
import com.positiveapps.weaware.objects.Person;

/**
 * Created by Izakos on 04/10/2015.
 */
public class TablePersons extends ContentProvaiderDataSourseAdapter<Person>{

    // table name
    public static final String TABLE_PERSONS = "persons";


    // columns names
    public static final String COLUMN_ID = "_id";
    public static final String SERVER_ID = "serverId";
    public static final String CATEGORY = "category";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String USER_NAME = "userName";
    public static final String CITY = "city";
    public static final String STATUS = "status";
    public static final String PROFILE_IMAGE = "ProfileImage";
    public static final String PROFILE_IMAGE_BLOB = "ProfileImageBlob";

    public static final String[] ALL_COLUMNS = {
            COLUMN_ID, SERVER_ID,  CATEGORY , STATUS, LAT,LNG,USER_NAME,CITY,
            PROFILE_IMAGE,PROFILE_IMAGE_BLOB
    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_PERSONS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + SERVER_ID + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + CATEGORY + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + LAT + " real DEFAULT " + DEFAULT_INT_VALUE + ", "
            + LNG + " real DEFAULT " + DEFAULT_INT_VALUE + ", "
            + USER_NAME + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + CITY + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + STATUS + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + PROFILE_IMAGE + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + PROFILE_IMAGE_BLOB + " blob"
            +");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONS);
        onCreate(database);
    }


    public static TablePersons instance;



    public static TablePersons getInstance (Context context){
        if (instance == null){
            instance = new TablePersons(context);
        }
        return instance;
    }


    private TablePersons(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return PersonsContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null){
            databasehelper.close();
        }

        instance = null;
    }



    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }



    @Override
    public Person cursorToEntity(Cursor cursor) {

        Person person = new Person();
        person.setDatabaseId(cursor.getLong(cursor.getColumnIndexOrThrow(TablePersons.COLUMN_ID)));
        person.setServerId(cursor.getLong(cursor.getColumnIndexOrThrow(TablePersons.SERVER_ID)));
        person.setCategory(cursor.getInt(cursor.getColumnIndexOrThrow(TablePersons.CATEGORY))); ;
        person.setLat(cursor.getDouble(cursor.getColumnIndexOrThrow(TablePersons.LAT)));
        person.setLng(cursor.getDouble(cursor.getColumnIndexOrThrow(TablePersons.LNG)));
        person.setUserName(cursor.getString(cursor.getColumnIndexOrThrow(TablePersons.USER_NAME)));
        person.setCity(cursor.getString(cursor.getColumnIndexOrThrow(TablePersons.CITY)));
        person.setStatus(cursor.getString(cursor.getColumnIndexOrThrow(TablePersons.STATUS)));
        person.setProfileImage(cursor.getString(cursor.getColumnIndexOrThrow(TablePersons.PROFILE_IMAGE)));
        person.setProfileImageBlob(cursor.getBlob(cursor.getColumnIndexOrThrow(TablePersons.PROFILE_IMAGE_BLOB)));
        return person;
    }

    @Override
    public ContentValues entityToContentValue(Person entity) {
        ContentValues values = new ContentValues();

        values.put(TablePersons.SERVER_ID, entity.getServerId());
        values.put(TablePersons.CATEGORY, entity.getCategory());
        values.put(TablePersons.LAT, entity.getLat());
        values.put(TablePersons.LNG, entity.getLng());
        values.put(TablePersons.USER_NAME, entity.getUserName());
        values.put(TablePersons.CITY, entity.getCity());
        values.put(TablePersons.PROFILE_IMAGE, entity.getProfileImage());
        values.put(TablePersons.STATUS, entity.getStatus());
        values.put(TablePersons.PROFILE_IMAGE_BLOB, entity.getProfileImageBlob());
        return values;
    }



    @Override
    protected BaseContentProvider createContentProvider() {
        return new PersonsContentProvider();
    }


    @Override
    public synchronized Person insert(Person entity) {
        return super.insert(entity);

    }

    public Person isEntryAllradyExsit (Person toCheck){

        try {
            Person msg = readEntityByID(toCheck.getDatabaseId());
            if (msg != null){
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public Person insertOrUpdate (Person toAdd) {
        Person check = isEntryAllradyExsit(toAdd);
        if (check != null){
            return update(check.getDatabaseId(), toAdd);
        }else{
            return insert(toAdd);
        }
    }


}
