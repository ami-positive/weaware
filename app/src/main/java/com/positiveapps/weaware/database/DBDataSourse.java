/**
 * 
 */
package com.positiveapps.weaware.database;

import java.util.ArrayList;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

/**
 * @author natiapplications
 *
 */
public abstract class DBDataSourse<T> {
	
	
	protected Context context;
	protected SQLiteOpenHelper helper;
	protected SQLiteDatabase database;
	protected String[] allColumns;
	protected String tableName;
	protected String columnID;
	
	public DBDataSourse (Context context,SQLiteOpenHelper helper){
		this.context = context;
		onCreateDataSourse(helper);
	}
	
	protected void onCreateDataSourse (SQLiteOpenHelper databaseHelper){
		helper = databaseHelper;
		database = databaseHelper.getWritableDatabase();
	}
	
	
	public abstract T cursorToEntity(Cursor cursor);
	public abstract ContentValues entityToContentValue(T entity);
	public abstract void close();
	
	
	synchronized public T insert(T entity) {
		
		long insertId = database.insert
				(tableName, null,entityToContentValue(entity));
		return readEntityByID(insertId);
	}

	
	
	synchronized public boolean delete(long id,T entity) {
		long deletedID = database.delete(tableName, columnID + " = " + id, null);
		boolean result = deletedID > 0 ? true:false;
		return result;
	}

	synchronized public void deleteAll() {
		database.delete(tableName, null, null);
	}
	
	
	synchronized public void deleteByFillter(Bundle conditions) {
		String conString = cretateCondintionsStringByBundle(conditions);
		database.delete(tableName, conString, null);
	}
	
	synchronized public void deleteByFillter(String selection) {
		database.delete(tableName, selection, null);
	}
	
	synchronized public T update(long id,T entity) {
		
		database.update(tableName, entityToContentValue(entity)
				,columnID + " = " + id,null);
		
		return entity;
	}

	
	
	synchronized public T readEntityByID(long entityId) {
		Cursor cursor = database.query(tableName,
				allColumns, columnID + " = " + entityId, null,
				null, null, null);
		cursor.moveToFirst();
		T newItem = cursorToEntity(cursor);
		cursor.close();
		return newItem;
	}
	
	
	synchronized public ArrayList<T> readAll(String sortedBy) {
		return read("", sortedBy);
	}

	
	synchronized public ArrayList<T> read(Bundle condintions, String sortedBy) {
		Cursor cursor = null;
		
		if (condintions == null){
			cursor = database.query(tableName,
					allColumns, null, null, null, null, sortedBy);
		}else{
			String conString = cretateCondintionsStringByBundle(condintions);
			cursor = database.query(tableName,
					allColumns, conString, null, null, null, sortedBy);
		}
		
		ArrayList<T> items = new ArrayList<T>();
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				T item = cursorToEntity(cursor);
				items.add(item);
				cursor.moveToNext();
			}
			cursor.close();
		}
		
		return items;
	}

	
	synchronized public ArrayList<T> read(String  selection, String sortedBy) {
		Cursor cursor = null;
		
		if (selection.isEmpty()){
			cursor = database.query(tableName,
					allColumns, null, null, null, null, sortedBy);
		}else{
			cursor = database.query(tableName,
					allColumns, selection, null, null, null, sortedBy);
		}
		
		ArrayList<T> items = new ArrayList<T>();
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				T item = cursorToEntity(cursor);
				items.add(item);
				cursor.moveToNext();
			}
			cursor.close();
		}
		
		return items;
	}
	
	synchronized public Cursor readCursor(String sortedBy) {
		return readCursor(sortedBy);
	}

	
	synchronized public Cursor readCursor(Bundle condintions, String sortedBy) {
		Cursor cursor = null;
		if (condintions == null){
			cursor = database.query(tableName,
					allColumns, null, null, null, null, sortedBy);
		}else{
			String conString = cretateCondintionsStringByBundle(condintions);
			cursor = database.query(tableName,
					allColumns, conString, null, null, null, sortedBy);
		}
		return cursor;
	}
	
	
	synchronized public Cursor readCursor(String selection, String sortedBy) {
		Cursor cursor = null;
		if (selection == null){
			cursor = database.query(tableName,
					allColumns, null, null, null, null, sortedBy);
		}else{
			cursor = database.query(tableName,
					allColumns, selection, null, null, null, sortedBy);
		}
		return cursor;
	}
	
	public  String cretateCondintionsStringByBundle (Bundle condintions){
		StringBuilder conString = new StringBuilder();
		Set<String> keys = condintions.keySet();
		int counter = 0;
		for (String key : keys) {
			if (condintions.get(key) instanceof String){
				conString.append(key + " = " + "\""+ condintions.get(key) + "\"");
			}else{
				conString.append(key + " = " + condintions.get(key));
			}
			
			if (counter < keys.size()-1){
				conString.append(" and ");
			}
			counter++;
		}
		return conString.toString();
	}
}
