/**
 * 
 */
package com.positiveapps.weaware.database;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 * @author natiapplications
 * 
 */
public abstract class BaseContentProvider extends ContentProvider {

	// database
	protected SQLiteOpenHelper databaseHelper;
	protected SQLiteDatabase database;

	// used for the UriMacher
	protected static final int ENTITEIS = 10;
	protected static final int ENTITY_ID = 20;

	protected String AUTHORITY;
	protected String BASE_PATH;
	public static Uri CONTENT_URI;
	protected UriMatcher URIMatcher;
	
	protected String TABLE_NAME;
	protected String[] ALL_COLUMNS;
	protected String PROVIDER_COLUMN_ID;

	{
		AUTHORITY = provideAuthority();
		BASE_PATH = provideBasePath();
		TABLE_NAME = provideTableName();
		Log.e("columnsIDlog", "columnsID = " + provideColumnID());
		PROVIDER_COLUMN_ID = provideColumnID();
		ALL_COLUMNS = provideAllColumns();
		CONTENT_URI = Uri.parse("content://" + AUTHORITY+ "/" + BASE_PATH);
		URIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		URIMatcher.addURI(AUTHORITY, BASE_PATH, ENTITEIS);
		URIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", ENTITY_ID);
	}
	
	protected abstract String provideAuthority();
	protected abstract String provideBasePath();
	protected abstract String provideTableName();
	protected abstract String provideColumnID();
	protected abstract String[] provideAllColumns();
	protected abstract SQLiteOpenHelper provideSQLiteHalper();
	
	@Override
	public boolean onCreate() {
		databaseHelper = provideSQLiteHalper();
		database = databaseHelper.getWritableDatabase();
		database.isOpen();
		
		return false;
	}

	

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		
		if (projection != null && projection.length > 0){
			String chek = projection[0];
			if("spatialQuery".equalsIgnoreCase(chek)){
				Cursor cursor = database.rawQuery(selection, null);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				if (cursor != null){
					Log.e("spatialQueryLog", "cursor != null ! " + cursor.getCount());
					
					return cursor;
				}
				
			}
		}
		
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		checkColumns(projection);
		
		queryBuilder.setTables(TABLE_NAME);

		int uriType = URIMatcher.match(uri);
		switch (uriType) {
		case ENTITEIS:
			break;
		case ENTITY_ID:
			queryBuilder.appendWhere(PROVIDER_COLUMN_ID + "="
					+ uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		Cursor cursor = queryBuilder.query(database, projection, selection,
				selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = URIMatcher.match(uri);
		long id = 0;
		switch (uriType) {
		case ENTITEIS:
			id = database.insert(TABLE_NAME, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = URIMatcher.match(uri);
		int rowsDeleted = 0;
		switch (uriType) {
		case ENTITEIS:
			rowsDeleted = database.delete(TABLE_NAME, selection,
					selectionArgs);
			break;
		case ENTITY_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = database.delete(TABLE_NAME,
						PROVIDER_COLUMN_ID + "=" + id, null);
			} else {
				rowsDeleted = database.delete(TABLE_NAME,
						PROVIDER_COLUMN_ID + "=" + id + " and " + selection,
						selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = URIMatcher.match(uri);
		int rowsUpdated = 0;
		switch (uriType) {
		case ENTITEIS:
			Log.e("updateDBLog", "update ! uri = " + uri  + " values = "
					+ values.getAsString(PROVIDER_COLUMN_ID) + 
					" selection  = " + selection);
			rowsUpdated = database.update(TABLE_NAME, values,
					selection, selectionArgs);
			break;
		case ENTITY_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = database.update(TABLE_NAME, values,
						PROVIDER_COLUMN_ID + "=" + id, null);
			} else {
				rowsUpdated = database.update(TABLE_NAME, values,
						PROVIDER_COLUMN_ID + "=" + id + " and " + selection,
						selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}

	
	protected void checkColumns(String[] projection) {
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(ALL_COLUMNS));
			// check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException(
						"Unknown columns in projection");
			}
		}
		
	}

	/**
	 * @return the databaseHelper
	 */
	public SQLiteOpenHelper getDatabaseHelper() {
		return databaseHelper;
	}

	/**
	 * @return the database
	 */
	public SQLiteDatabase getDatabase() {
		return database;
	}

	/**
	 * @return the tABLE_NAME
	 */
	public String getTABLE_NAME() {
		return TABLE_NAME;
	}

	/**
	 * @return the aLL_COLUMNS
	 */
	public String[] getALL_COLUMNS() {
		return ALL_COLUMNS;
	}

	/**
	 * @return the cOLUMN_ID
	 */
	public String getCOLUMN_ID() {
		return PROVIDER_COLUMN_ID;
	}
	
	

}
