package com.positiveapps.weaware.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.weaware.database.BaseContentProvider;
import com.positiveapps.weaware.database.DatabaseHelper;
import com.positiveapps.weaware.database.tables.TableReports;

/**
 * Created by Izakos on 04/10/2015.
 */
public class ReportsContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "dev.positiveapps.weaware.android.db.reports.contentprovider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "reports";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TableReports.TABLE_REPORTS;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableReports.COLUMN_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableReports.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}
