package com.positiveapps.weaware.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.positiveapps.weaware.database.BaseContentProvider;
import com.positiveapps.weaware.database.ContentProvaiderDataSourseAdapter;
import com.positiveapps.weaware.database.providers.ReportsContentProvider;
import com.positiveapps.weaware.objects.Report;

/**
 * Created by Izakos on 04/10/2015.
 */
public class TableReports extends ContentProvaiderDataSourseAdapter<Report> {

    // table name
    public static final String TABLE_REPORTS = "reports";
    
    // columns names
    public static final String COLUMN_ID = "_id";
    public static final String SERVER_ID = "serverId";
    public static final String CATEGORY = "category";
    public static final String CONFIRM = "confirm";
    public static final String OPENED = "opened";
    public static final String LAT = "lat";
    public static final String LON = "lon";
    public static final String REPORT_DATE = "reportDate";
    public static final String REPORTER_IMAGE = "reporterImage";
    public static final String REPORTER_NAME = "reporterName";
    public static final String REPORTER_NUM_STARS = "reporterNumStars";
    public static final String REPORTER_RANK = "reporterRank";
    public static final String PHOTO_ATTACHED = "photoAttached";
    public static final String AUDIO_ATTACHED = "audioAttached";
    public static final String DESCRIPTION = "description";
    public static final String USERS_CONFIRM = "usersConfirm";

    public static final String[] ALL_COLUMNS = {
            COLUMN_ID, SERVER_ID,CATEGORY,CONFIRM,
            OPENED, LAT, LON, REPORT_DATE, REPORTER_IMAGE, REPORTER_NAME, PHOTO_ATTACHED,AUDIO_ATTACHED,
            DESCRIPTION, USERS_CONFIRM, REPORTER_NUM_STARS, REPORTER_RANK
    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_REPORTS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + SERVER_ID + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + CATEGORY + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + CONFIRM + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + OPENED + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + LAT + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + LON + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + REPORT_DATE + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + REPORTER_IMAGE + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + REPORTER_NAME + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + REPORTER_RANK + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + REPORTER_NUM_STARS + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + PHOTO_ATTACHED + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + AUDIO_ATTACHED + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + DESCRIPTION + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + USERS_CONFIRM + " text not null DEFAULT " + DEFAULT_TEXT_VALUE
            +");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_REPORTS);
        onCreate(database);
    }


    public static TableReports instance;



    public static TableReports getInstance (Context context){
        if (instance == null){
            instance = new TableReports(context);
        }
        return instance;
    }


    private TableReports(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return ReportsContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null){
            databasehelper.close();
        }

        instance = null;
    }



    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }


    @Override
    public Report cursorToEntity(Cursor cursor) {

        Report report = new Report();
        report.setDatabaseId(cursor.getLong(cursor.getColumnIndexOrThrow(TableReports.COLUMN_ID)));
        report.setServerId(cursor.getLong(cursor.getColumnIndexOrThrow(TableReports.SERVER_ID)));
        report.setCategory(cursor.getInt(cursor.getColumnIndexOrThrow(TableReports.CATEGORY)));
        report.setConfirm(cursor.getInt(cursor.getColumnIndexOrThrow(TableReports.CONFIRM)));
        report.setOpened(cursor.getInt(cursor.getColumnIndexOrThrow(TableReports.OPENED)));
        report.setLat(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.LAT)));
        report.setLon(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.LON)));
        report.setReportDate(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.REPORT_DATE)));
        report.setReporterImage(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.REPORTER_IMAGE)));
        report.setReporterName(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.REPORTER_NAME)));
        report.setReporterRank(cursor.getInt(cursor.getColumnIndexOrThrow(TableReports.REPORTER_RANK)));
        report.setReporterNumStars(cursor.getInt(cursor.getColumnIndexOrThrow(TableReports.REPORTER_NUM_STARS)));
        report.setPhotoAttached(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.PHOTO_ATTACHED)));
        report.setAudioAttached(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.AUDIO_ATTACHED)));
        report.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.DESCRIPTION)));
        report.setUsersConfirm(cursor.getString(cursor.getColumnIndexOrThrow(TableReports.USERS_CONFIRM)));
        return report;
    }

    @Override
    public ContentValues entityToContentValue(Report entity) {
        ContentValues values = new ContentValues();

        values.put(TableReports.SERVER_ID, entity.getServerId());
        values.put(TableReports.CATEGORY, entity.getCategory());
        values.put(TableReports.CONFIRM, entity.getConfirm());
        values.put(TableReports.OPENED, entity.getOpened());
        values.put(TableReports.LAT, entity.getLatString());
        values.put(TableReports.LON, entity.getLonString());
        values.put(TableReports.REPORT_DATE, entity.getReportDate());
        values.put(TableReports.REPORTER_IMAGE, entity.getReporterImage());
        values.put(TableReports.REPORTER_NAME, entity.getReporterName());
        values.put(TableReports.REPORTER_RANK, entity.getReporterRank());
        values.put(TableReports.REPORTER_NUM_STARS, entity.getReporterNumStars());
        values.put(TableReports.PHOTO_ATTACHED, entity.getPhotoAttached());
        values.put(TableReports.AUDIO_ATTACHED, entity.getAudioAttached());
        values.put(TableReports.DESCRIPTION, entity.getDescription());
        values.put(TableReports.USERS_CONFIRM, entity.getUsersConfirm());

        return values;
    }



    @Override
    protected BaseContentProvider createContentProvider() {
        return new ReportsContentProvider();
    }


    @Override
    public synchronized Report insert(Report entity) {
        return super.insert(entity);

    }

    public Report isEntryAllradyExsit (Report toCheck){

        try {
            Report msg = readEntityByID(toCheck.getDatabaseId());
            if (msg != null){
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public Report insertOrUpdate (Report toAdd) {
        Report check = isEntryAllradyExsit(toAdd);
        if (check != null){
            return update(check.getDatabaseId(), toAdd);
        }else{
            return insert(toAdd);
        }
    }
}
