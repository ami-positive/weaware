package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.unfriendResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class unfriendRequest extends RequestObject<unfriendResponse>{

    private String FriendID;

    public unfriendRequest() {
    }

    public unfriendRequest(String FriendID) {
        this.FriendID = FriendID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<unfriendResponse>> callback) {
        apiInterface.unfriend(FriendID, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
