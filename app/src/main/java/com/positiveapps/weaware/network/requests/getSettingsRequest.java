package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getSettingsResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class getSettingsRequest extends RequestObject<getSettingsResponse>{


    public getSettingsRequest() {
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<getSettingsResponse>> callback) {
        apiInterface.getsettings(NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
