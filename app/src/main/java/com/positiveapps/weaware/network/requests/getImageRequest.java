package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getImageResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class getImageRequest extends RequestObject<getImageResponse>{

    private String ID;

    public getImageRequest() {
    }

    public getImageRequest(String ID) {
        this.ID = ID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<getImageResponse>> callback) {
        apiInterface.getimage(ID, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
