package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getByAppuserResponse;
import com.positiveapps.weaware.objects.User;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class getByAppuserRequest extends RequestObject<getByAppuserResponse>{

    public getByAppuserRequest() {
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<getByAppuserResponse>> callback) {
        apiInterface.getAll(User.getAccessToken(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
