package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Friend;

import java.util.ArrayList;


/***************************************---- Response Class ------********************/

public class searchResponse extends ResponseObject<searchResponse>{

    @SerializedName("Appusers")
    private ArrayList<Friend> Friends;

    public ArrayList<Friend> getFriends() {
        return Friends;
    }

    public void setFriends(ArrayList<Friend> friends) {
        this.Friends = Friends;
    }
}

