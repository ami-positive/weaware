package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Reports;

import java.util.ArrayList;


/***************************************---- Response Class ------********************/

public class getByAppuserResponse extends ResponseObject<getByAppuserResponse>{

    @SerializedName("Reports")
    private ArrayList<Reports> reports;

    public ArrayList<Reports> getReports() {
        return reports;
    }

    public void setReports(ArrayList<Reports> reports) {
        this.reports = reports;
    }
}

