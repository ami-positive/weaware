package com.positiveapps.weaware.network.requests;

import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getFriendRequestsResponse;

import retrofit.Callback;

/**
 * Created by Izakos on 03/03/2016.
 */
public class getFriendRequestsRequest  extends RequestObject<getFriendRequestsResponse>{

    public getFriendRequestsRequest() {
    }


    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<getFriendRequestsResponse>> callback) {
        apiInterface.getfriendsRequest(NetworkManager.userParams(), callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }

  /*  public static void getFriends(final onErrorCallBack callBack){
        MyApp.networkManager.makeRequest(new getFriendsRequest(), new NetworkCallback<getFriendsResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<getFriendsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    updateFriends(response.getData().getFriends());
                } else {
                    if (callBack != null) {
                        callBack.onError(errorDesc);
                    }else{
                        ToastUtil.toster(errorDesc, true);
                    }
                }
            }
        });
    }*/
}
