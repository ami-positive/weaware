package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Appuser;

import java.util.ArrayList;


/***************************************---- Response Class ------********************/

public class getFriendsResponse extends ResponseObject<getFriendsResponse>{


    @SerializedName("Friends")
    private ArrayList<Appuser> friends;

    public ArrayList<Appuser> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<Appuser> friends) {
        this.friends = friends;
    }
}

