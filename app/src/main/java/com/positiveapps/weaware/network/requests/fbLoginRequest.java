package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.fbLoginResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class fbLoginRequest extends RequestObject<fbLoginResponse>{

    private String FBID;
    private String FBAccessToken;
    private String UDID;

    public fbLoginRequest() {
    }

    public fbLoginRequest(String FBID,String FBAccessToken,String UDID) {
        this.FBID = FBID;
        this.FBAccessToken = FBAccessToken;
        this.UDID = UDID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<fbLoginResponse>> callback) {
        apiInterface.fblogin(FBID,FBAccessToken,UDID, callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
