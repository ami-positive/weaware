package com.positiveapps.weaware.network.response;
import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Images;


/***************************************---- Response Class ------********************/

public class getImageResponse extends ResponseObject<getImageResponse>{


    @SerializedName("Image")
    private Images Image;

    public Images getImages() {
        return Image;
    }

    public void setImages(Images images) {
        this.Image = images;
    }

}

