package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/***************************************---- Response Class ------********************/

public class getSettingsResponse extends ResponseObject<getSettingsResponse>{

    @SerializedName("RankColors")
    private ArrayList<String> rankColors;

    public ArrayList<String> getRankColors() {
        return rankColors;
    }

    public void setRankColors(ArrayList<String> rankColors) {
        this.rankColors = rankColors;
    }
}

