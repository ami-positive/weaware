package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.updateLocationResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class updateLocationRequest extends RequestObject<updateLocationResponse>{

    private String Lat;
    private String Lng;

    public updateLocationRequest() {
    }

    public updateLocationRequest(String Lat,String Lng) {
        this.Lat = Lat;
        this.Lng = Lng;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<updateLocationResponse>> callback) {
        apiInterface.updatelocation(Lat,Lng, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
