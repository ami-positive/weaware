package com.positiveapps.weaware.network.requests;

import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.searchResponse;
import com.positiveapps.weaware.objects.User;

import retrofit.Callback;



/***************************************---- Request Class ------********************/

public class searchRequest extends RequestObject<searchResponse> {

    private String Keyword;

    public searchRequest() {
    }

    public searchRequest(String Keyword) {
        this.Keyword = Keyword;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<searchResponse>> callback) {
        apiInterface.search(Keyword, User.getAccessToken(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
