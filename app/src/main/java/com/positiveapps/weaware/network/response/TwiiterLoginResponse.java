package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Appuser;

/**
 * Created by Izakos on 21/02/2016.
 */
public class TwiiterLoginResponse extends ResponseObject<TwiiterLoginResponse>{

    @SerializedName("Appuser")
    private Appuser appUser;
    @SerializedName("access_token")
    private String AccessToken;

    public Appuser getAppUser() {
        return appUser;
    }

    public void setAppUser(Appuser appUser) {
        this.appUser = appUser;
    }
    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String AccessToken) {
        this.AccessToken = AccessToken;
    }
}
