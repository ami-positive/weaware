package com.positiveapps.weaware.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.requests.RequestObject;
import com.positiveapps.weaware.objects.User;

import java.util.HashMap;
import java.util.Map;

import retrofit.RestAdapter;


/**
 * Created by natiapplications on 16/08/15.
 */
public class NetworkManager {

    public static final String BASE_URL = "http://*";

    public static final int REMOTE_CONT_TYPE_Gateway = 0;
    public static final int REMOTE_CONT_TYPE_CLOUD = 1;


    private Context context;
    private RestAdapter restAdapter;
    private ApiInterface apiInterface;
    // connectivity manager to check network state before send the requests
    private ConnectivityManager connectivityManager;

    private NetworkManager(Context context) {
        this.context = context;
    }

    private static NetworkManager instance;

    public static NetworkManager getInstance (Context context) {
        if (instance == null){
            instance = new NetworkManager(context);
        }
        instance.initRestAdapter();
        return instance;
    }

    public ApiInterface initRestAdapter (){

       /* OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(15, TimeUnit.SECONDS);
        OkClient okClient = new OkClient(client);*/



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL.replace("*","weaware.positive-apps.com/api/v1/"))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        apiInterface = restAdapter.create(ApiInterface.class);

        return  apiInterface;

    }


    public void makeRequest (RequestObject request,NetworkCallback<?> callback){
        if (request != null){
            request.request(this.apiInterface,callback);
        }
    }




    public static Map<String, String> userParams() {
        Map<String, String> userParams = new HashMap<String, String>();
        userParams.put("access_token", User.getAccessToken());
        return userParams;
    }


    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) MyApp.appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isWifiAvailable () {
        ConnectivityManager connManager = (ConnectivityManager)MyApp.appContext.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            return true;
        }
        return false;
    }



//        MyApp.networkManager.makeRequest(new GeneralDataRequest(), new NetworkCallback<GeneralData>() {
//            @Override
//            public void onResponse(boolean success, String errorDesc, ResponseObject<GeneralData> response) {
//                super.onResponse(success, errorDesc, response);
//                if (success) {
//
//
//                    showErrorDialog(response.getData().getApplicationName() + response.getData().getContactEmail());
//                    helloWorld.setText(response.getData().getSeerverTime() + response.getData().getVersion());
//                } else {
//                    helloWorld.setText(errorDesc);
//                }
//            }
//        });





}
