package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.confirmResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class confirmRequest extends RequestObject<confirmResponse>{

    private String ID;

    public confirmRequest() {
    }

    public confirmRequest(String ID) {
        this.ID = ID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<confirmResponse>> callback) {
        apiInterface.confirm(ID, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
