package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.removeImageResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class removeImageRequest extends RequestObject<removeImageResponse>{

    private String ImageID;

    public removeImageRequest() {
    }

    public removeImageRequest(String ImageID) {
        this.ImageID = ImageID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<removeImageResponse>> callback) {
        apiInterface.removeimage(ImageID, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
