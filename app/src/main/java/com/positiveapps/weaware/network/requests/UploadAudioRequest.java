package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.UploadAudioResponse;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.objects.User;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.mime.TypedFile;

/***************************************---- Request Class ------********************/

public class UploadAudioRequest extends RequestObject<UploadAudioResponse>{

    private TypedFile myfile;
    public UploadAudioRequest(TypedFile typedFile, NetworkCallback<UploadAudioResponse> networkCallback) {
    }

    public UploadAudioRequest(TypedFile myfile) {
        this.myfile = myfile;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<UploadAudioResponse>> callback) {
        apiInterface.upload(myfile, userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }

    public  Map<String, String> userParams() {
        Map<String, String> userParams = new HashMap<String, String>();
        userParams.put("access_token", User.getAccessToken());
        userParams.put("ReportID", Report.getCurrentReport().getServerId()+"");
        return userParams;
    }
}


