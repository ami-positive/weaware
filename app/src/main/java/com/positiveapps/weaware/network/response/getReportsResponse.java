package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Images;
import com.positiveapps.weaware.objects.Reports;

import java.util.ArrayList;


/***************************************---- Response Class ------********************/

public class getReportsResponse extends ResponseObject<getReportsResponse>{

    @SerializedName("Report")
    private Reports reports;
    @SerializedName("Images")
    private ArrayList<Images> images;

    public Reports getReports() {
        return reports;
    }

    public void setReports(Reports Reports) {
        this.reports = Reports;
    }

    public ArrayList<Images> getImages() {
        return images;
    }

    public void setImages(ArrayList<Images> images) {
        this.images = images;
    }
}

