package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.response.LoginResponse;
import com.positiveapps.weaware.network.response.ResponseObject;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class LoginRequest extends RequestObject<LoginResponse>{

    private String Username;
    private String Password;

    public LoginRequest() {
    }

    public LoginRequest(String Username,String Password) {
        this.Username = Username;
        this.Password = Password;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<LoginResponse>> callback) {
        apiInterface.login(Username,Password, callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
