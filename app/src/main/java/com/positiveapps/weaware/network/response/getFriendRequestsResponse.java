package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Appuser;

import java.util.ArrayList;

/**
 * Created by Izakos on 03/03/2016.
 */
public class getFriendRequestsResponse {

    @SerializedName("Friends")
    private ArrayList<Appuser> friends;

    public ArrayList<Appuser> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<Appuser> friends) {
        this.friends = friends;
    }
}