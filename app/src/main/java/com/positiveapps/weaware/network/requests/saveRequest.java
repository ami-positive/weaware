package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.saveResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class saveRequest extends RequestObject<saveResponse>{

    private String FullName;
    private String City;
    private String Email;
    private String Phone;
    private String Avatar;
    private int IsPoliceman;

    public saveRequest() {
    }

    public saveRequest(String FullName,String City,String Email,String Phone,String Avatar,int IsPoliceman) {
        this.FullName = FullName;
        this.City = City;
        this.Email = Email;
        this.Phone = Phone;
        this.Avatar = Avatar;
        this.IsPoliceman = IsPoliceman;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<saveResponse>> callback) {
        apiInterface.save(FullName,City,Email,Phone,Avatar,IsPoliceman, "jpg", NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
