package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.uploadImageResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class uploadImageRequest extends RequestObject<uploadImageResponse>{

    private String ReportID;
    private String Image;

    public uploadImageRequest() {
    }

    public uploadImageRequest(String ReportID,String Image) {
        this.ReportID = ReportID;
        this.Image = Image;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<uploadImageResponse>> callback) {
        apiInterface.uploadimage(ReportID,Image, "jpg",NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
