package com.positiveapps.weaware.network.twitter;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by Izakos on 11/10/2015.
 */
public class MyTwitterApiClient extends TwitterApiClient {


    public MyTwitterApiClient(TwitterSession session) {
        super(session);
    }

    public TwitterUsersService getUsersService() {
        return getService(TwitterUsersService.class);
    }
}
