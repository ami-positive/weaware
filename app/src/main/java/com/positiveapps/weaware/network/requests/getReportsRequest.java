package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getReportsResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class getReportsRequest extends RequestObject<getReportsResponse>{

    private String ID;

    public getReportsRequest() {
    }

    public getReportsRequest(String ID) {
        this.ID = ID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<getReportsResponse>> callback) {
        apiInterface.getreports(ID, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
