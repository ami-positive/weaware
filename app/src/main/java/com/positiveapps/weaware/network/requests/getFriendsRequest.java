package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getFriendsResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class getFriendsRequest extends RequestObject<getFriendsResponse>{


    public getFriendsRequest() {
    }


    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<getFriendsResponse>> callback) {
        apiInterface.getfriends(NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
