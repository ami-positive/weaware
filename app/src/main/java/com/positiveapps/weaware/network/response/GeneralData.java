package com.positiveapps.weaware.network.response;

/**
 * Created by natiapplications on 16/08/15.
 */
public class GeneralData extends ResponseObject<GeneralData> {


   private String ApplicationName;
   private String Version;
   private String SeerverTime;
   private String ContactPhone;
   private String ContactEmail;

   public GeneralData (){

   }

   public String getApplicationName() {
      return ApplicationName;
   }

   public void setApplicationName(String applicationName) {
      ApplicationName = applicationName;
   }

   public String getVersion() {
      return Version;
   }

   public void setVersion(String version) {
      Version = version;
   }

   public String getSeerverTime() {
      return SeerverTime;
   }

   public void setSeerverTime(String seerverTime) {
      SeerverTime = seerverTime;
   }

   public String getContactPhone() {
      return ContactPhone;
   }

   public void setContactPhone(String contactPhone) {
      ContactPhone = contactPhone;
   }

   public String getContactEmail() {
      return ContactEmail;
   }

   public void setContactEmail(String contactEmail) {
      ContactEmail = contactEmail;
   }
}
