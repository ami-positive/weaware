package com.positiveapps.weaware.network;


import com.positiveapps.weaware.network.response.ApproveFriendResponse;
import com.positiveapps.weaware.network.response.LoginResponse;
import com.positiveapps.weaware.network.response.RegisterResponse;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.TwiiterLoginResponse;
import com.positiveapps.weaware.network.response.UpdateProfileResponse;
import com.positiveapps.weaware.network.response.UploadAudioResponse;
import com.positiveapps.weaware.network.response.addFriendResponse;
import com.positiveapps.weaware.network.response.closeResponse;
import com.positiveapps.weaware.network.response.confirmResponse;
import com.positiveapps.weaware.network.response.fbLoginResponse;
import com.positiveapps.weaware.network.response.getByAppuserResponse;
import com.positiveapps.weaware.network.response.getFriendRequestsResponse;
import com.positiveapps.weaware.network.response.getFriendsResponse;
import com.positiveapps.weaware.network.response.getImageResponse;
import com.positiveapps.weaware.network.response.getReportsResponse;
import com.positiveapps.weaware.network.response.getSettingsResponse;
import com.positiveapps.weaware.network.response.removeImageResponse;
import com.positiveapps.weaware.network.response.saveReportResponse;
import com.positiveapps.weaware.network.response.saveResponse;
import com.positiveapps.weaware.network.response.searchResponse;
import com.positiveapps.weaware.network.response.setPushTokenResponse;
import com.positiveapps.weaware.network.response.unfriendResponse;
import com.positiveapps.weaware.network.response.updateLocationResponse;
import com.positiveapps.weaware.network.response.uploadImageResponse;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

/**
 * Created by natiapplications on 16/08/15.
 */
public interface ApiInterface {

	/***************************************---- Api Interface ------********************/

	public static final String REGISTER = "/appuser/Register";
	public static final String LOGIN = "/appuser/Login";
	public static final String FBLOGIN = "/appuser/fbLogin";
	public static final String TWITTER_LOGIN = "/appuser/twitterLogin";
	public static final String GET_APPUSER = "/appuser/get";
	public static final String SAVE = "/appuser/save";
	public static final String SEARCH = "/appuser/search";
	public static final String ADDFRIEND = "/appuser/addFriend";
	public static final String UNFRIEND = "/appuser/unfriend";
	public static final String SAVE_REPORT = "/report/save";
	public static final String GETBYAPPUSER = "/report/getByAppuser";
	public static final String GET_ALL = "/report/getAll";
	public static final String UPLOADIMAGE = "/report/uploadImage";
	public static final String CLOSE = "/report/close";
	public static final String GETREPORTS = "/report/get";
	public static final String GETFRIENDS = "/appuser/getFriends";
	public static final String APPROVE_FRIEND = "/appuser/approveFriend";
	public static final String GETFRIENDS_REQUESTS = "/appuser/getRequests";
	public static final String UPDATELOCATION = "/appuser/updateLocation";
	public static final String CONFIRM = "/report/confirm";
	public static final String GETSETTINGS = "/general/getSettings";
	public static final String REMOVEIMAGE = "/report/removeImage";
	public static final String GETIMAGE = "/report/getImage";
	public static final String SETPUSHTOKEN = "/appuser/setPushToken";


	public static final String PUSHTOKEN = "PushToken";
	public static final String IMAGEID = "ImageID";
	public static final String REPORTID = "ReportID";
	public static final String IMAGE = "Image";
	public static final String DESCRIPTION = "Description";
	public static final String LAT = "Lat";
	public static final String LNG = "Lng";
	public static final String FRIENDID = "FriendID";
	public static final String KEYWORD = "Keyword";
	public static final String USERNAME = "Username";
	public static final String PASSWORD = "Password";
	public static final String UD_ID = "UDID";
	public static final String TWITTER_TOKEN = "Token";
	public static final String TWITTER_SECRET = "Secret";
	public static final String FBID = "FBID";
	public static final String ID = "ID";
	public static final String FBACCESSTOKEN = "FBAccessToken";
	public static final String FULLNAME = "FullName";
	public static final String CITY = "City";
	public static final String EMAIL = "Email";
	public static final String PHONE = "Phone";
	public static final String AVATAR = "Avatar";
	public static final String ISPOLICEMAN = "IsPoliceman";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String DEVICE = "Device";
	public static final String IMAGETTYPE = "ImageFileType";


	@FormUrlEncoded
	@POST(SETPUSHTOKEN)
	public void setpushtoken (
			@Field(PUSHTOKEN)String PushToken,
			@Field(DEVICE)String Device,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<setPushTokenResponse>> retroCallback);


	@GET(GETIMAGE)
	public void getimage (
			@Query(ID)String id,
			@QueryMap Map<String, String> userParams,
			Callback<ResponseObject<getImageResponse>> retroCallback);



	@FormUrlEncoded
	@POST(REMOVEIMAGE)
	public void removeimage (
			@Field(IMAGEID)String ImageID,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<removeImageResponse>> retroCallback);


	@FormUrlEncoded
	@POST(GETSETTINGS)
	public void getsettings (
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<getSettingsResponse>> retroCallback);

	@FormUrlEncoded
	@POST(CONFIRM)
	public void confirm (
			@Field(ID)String Id,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<confirmResponse>> retroCallback);

	@FormUrlEncoded
	@POST(APPROVE_FRIEND)
	public void approveFriend (
			@Field(FRIENDID)String FriendID,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<ApproveFriendResponse>> retroCallback);


	@FormUrlEncoded
	@POST(REGISTER)
	public void register (
			@Field(USERNAME)String Username,
			@Field(PASSWORD)String Password,
			@Field(UD_ID)String UDID,
			Callback<ResponseObject<RegisterResponse>> retroCallback);



	@FormUrlEncoded
	@POST(LOGIN)
	public void login (
			@Field(USERNAME)String Username,
			@Field(PASSWORD)String Password,
			Callback<ResponseObject<LoginResponse>> retroCallback);



	@FormUrlEncoded
	@POST(FBLOGIN)
	public void fblogin (
			@Field(FBID)String fbid,
			@Field(FBACCESSTOKEN)String FBAccessToken,
			@Field(UD_ID)String UDID,
			Callback<ResponseObject<fbLoginResponse>> retroCallback);

	@FormUrlEncoded
	@POST(TWITTER_LOGIN)
	public void Twitterlogin (
			@Field(TWITTER_TOKEN)String Token,
			@Field(TWITTER_SECRET)String Secret,
			@Field(UD_ID)String UDID,
			Callback<ResponseObject<TwiiterLoginResponse>> retroCallback);


	@GET(GET_APPUSER)
	public void getAppUserProfile(
			@Query(ID) String id,
			Callback<ResponseObject<UpdateProfileResponse>> callback);



	@FormUrlEncoded
	@POST(SAVE)
	public void save (
			@Field(FULLNAME)String FullName,
			@Field(CITY)String City,
			@Field(EMAIL)String Email,
			@Field(PHONE)String Phone,
			@Field(AVATAR)String Avatar,
			@Field(ISPOLICEMAN)int IsPoliceman,
			@Field(IMAGETTYPE)String ImageFileType,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<saveResponse>> retroCallback);


	@GET(SEARCH)
	public void search (
			@Query(KEYWORD)String Keyword,
			@Query(ACCESS_TOKEN) String accessToken,
			Callback<ResponseObject<searchResponse>> retroCallback);



	@FormUrlEncoded
	@POST(ADDFRIEND)
	public void addfriend (
			@Field(FRIENDID)String FriendID,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<addFriendResponse>> retroCallback);




	@FormUrlEncoded
	@POST(UNFRIEND)
	public void unfriend (
			@Field(FRIENDID)String FriendID,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<unfriendResponse>> retroCallback);



	@FormUrlEncoded
	@POST(SAVE_REPORT)
	public void savereport (
			@Field(ID)String Id,
			@Field(DESCRIPTION)String Description,
			@Field(LAT)String Lat,
			@Field(LNG)String Lng,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<saveReportResponse>> retroCallback);



	@GET(GET_ALL)
	public void getAll (
			@Query(ACCESS_TOKEN) String accessToken,
			Callback<ResponseObject<getByAppuserResponse>> retroCallback);


	@GET(GETFRIENDS)
	public void getfriends (
			@QueryMap Map<String, String> userParams,
			Callback<ResponseObject<getFriendsResponse>> retroCallback);

	@GET(GETFRIENDS_REQUESTS)
	public void getfriendsRequest (
			@QueryMap Map<String, String> userParams,
			Callback<ResponseObject<getFriendRequestsResponse>> retroCallback);

	@Multipart
	@POST("/report/uploadAudio")
	public void upload(@Part("audios") TypedFile file,
					   @QueryMap Map<String, String> userParams,
					   Callback<ResponseObject<UploadAudioResponse>> retroCallback);



	@FormUrlEncoded
	@POST(UPLOADIMAGE)
	public void uploadimage (
			@Field(REPORTID)String ReportID,
			@Field(IMAGE)String Image,
			@Field(IMAGETTYPE)String ImageFileType,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<uploadImageResponse>> retroCallback);



	@FormUrlEncoded
	@POST(CLOSE)
	public void close (
			@Field(ID)String id,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<closeResponse>> retroCallback);



	@FormUrlEncoded
	@POST(UPDATELOCATION)
	public void updatelocation (
			@Field(LAT)String Lat,
			@Field(LNG)String Lng,
			@FieldMap Map<String, String> userParams,
			Callback<ResponseObject<updateLocationResponse>> retroCallback);



	@GET(GETREPORTS)
	public void getreports (
			@Query(ID) String id,
			@QueryMap Map<String, String> userParams,
			Callback<ResponseObject<getReportsResponse>> retroCallback);





}
