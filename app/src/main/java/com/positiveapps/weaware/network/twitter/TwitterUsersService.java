package com.positiveapps.weaware.network.twitter;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.models.User;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Izakos on 11/10/2015.
 */
public interface TwitterUsersService {

    @GET("/1.1/users/show.json")
    void show(@Query("user_id") Long userId,
              @Query("screen_name") String screenName,
              @Query("include_entities") Boolean includeEntities,
              @Query("size") String size,
              Callback<User> cb);
}
