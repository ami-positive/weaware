package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.response.RegisterResponse;
import com.positiveapps.weaware.network.response.ResponseObject;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class RegisterRequest extends RequestObject<RegisterResponse>{

    private String Username;
    private String Password;
    private String UDID;

    public RegisterRequest() {
    }

    public RegisterRequest(String Username,String Password,String UDID) {
        this.Username = Username;
        this.Password = Password;
        this.UDID = UDID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<RegisterResponse>> callback) {
        apiInterface.register(Username,Password,UDID, callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
