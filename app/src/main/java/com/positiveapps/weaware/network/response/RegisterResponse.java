package com.positiveapps.weaware.network.response;


import com.google.gson.annotations.SerializedName;

/***************************************---- Response Class ------********************/

public class RegisterResponse extends ResponseObject<RegisterResponse>{

    private String ID;
    @SerializedName("access_token")
    private String AccessToken;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String AccessToken) {
        this.AccessToken = AccessToken;
    }
}
