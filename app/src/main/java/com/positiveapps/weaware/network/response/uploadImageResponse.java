package com.positiveapps.weaware.network.response;
import com.google.gson.annotations.SerializedName;


/***************************************---- Response Class ------********************/

public class uploadImageResponse extends ResponseObject<uploadImageResponse>{

    @SerializedName("ID")
    private int id;
    @SerializedName("Url")
    private String imageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

