package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.addFriendResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class addFriendRequest extends RequestObject<addFriendResponse>{

    private String FriendID;

    public addFriendRequest() {
    }

    public addFriendRequest(String FriendID) {
        this.FriendID = FriendID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<addFriendResponse>> callback) {
        apiInterface.addfriend(FriendID, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
