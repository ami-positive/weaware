package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.closeResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class closeRequest extends RequestObject<closeResponse>{

    private String ID;

    public closeRequest() {
    }

    public closeRequest(String ID) {
        this.ID = ID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<closeResponse>> callback) {
        apiInterface.close(ID, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
