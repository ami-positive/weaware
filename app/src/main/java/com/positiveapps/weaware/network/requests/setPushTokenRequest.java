package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.setPushTokenResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class setPushTokenRequest extends RequestObject<setPushTokenResponse>{

    private String PushToken;

    public setPushTokenRequest() {
    }

    public setPushTokenRequest(String PushToken) {
        this.PushToken = PushToken;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<setPushTokenResponse>> callback) {
        apiInterface.setpushtoken(PushToken, "android", NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
