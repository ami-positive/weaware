package com.positiveapps.weaware.network.requests;


import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.saveReportResponse;

import retrofit.Callback;

/***************************************---- Request Class ------********************/

public class saveReportRequest extends RequestObject<saveReportResponse>{

    private String ID;
    private String Description;
    private String Lat;
    private String Lng;

    public saveReportRequest() {
    }

    public saveReportRequest(String ID,String Description,String Lat,String Lng) {
        this.ID = ID;
        this.Description = Description;
        this.Lat = Lat;
        this.Lng = Lng;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<saveReportResponse>> callback) {
        apiInterface.savereport(ID,Description,Lat,Lng, NetworkManager.userParams(), callback);
    }

    @Override
        protected boolean showDefaultToastsOnError() {
            return true;
        }
}
