package com.positiveapps.weaware.network.response;
import com.google.gson.annotations.SerializedName;


/***************************************---- Response Class ------********************/

public class saveReportResponse extends ResponseObject<saveReportResponse>{

    @SerializedName("ID")
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

