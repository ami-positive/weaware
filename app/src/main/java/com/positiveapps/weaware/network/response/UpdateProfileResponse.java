package com.positiveapps.weaware.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.weaware.objects.Appuser;
import com.positiveapps.weaware.objects.Reports;


/***************************************---- Response Class ------********************/

public class UpdateProfileResponse extends ResponseObject<UpdateProfileResponse>{

    @SerializedName("Appuser")
    private Appuser appUser;
    @SerializedName("OpenReport")
    private Reports reports;


    public Reports getReports() {
        return reports;
    }

    public void setReports(Reports reports) {
        this.reports = reports;
    }

    public Appuser getAppUser() {
        return appUser;
    }

    public void setAppUser(Appuser appUser) {
        this.appUser = appUser;
    }
}

