package com.positiveapps.weaware.network.requests;

import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.UpdateProfileResponse;

import retrofit.Callback;

/**
 * Created by Izakos on 26/11/2015.
 */
public class UpdateProfileRequest  extends RequestObject<UpdateProfileResponse> {

    private String Id;

    public UpdateProfileRequest() {
    }

    public UpdateProfileRequest(String Id) {
        this.Id = Id;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<UpdateProfileResponse>> callback) {
        apiInterface.getAppUserProfile(Id, callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }

}
