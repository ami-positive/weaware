package com.positiveapps.weaware.network.requests;

import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.network.response.ApproveFriendResponse;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.unfriendResponse;

import retrofit.Callback;

/**
 * Created by Izakos on 03/03/2016.
 */
public class ApproveFriendRequest  extends RequestObject<ApproveFriendResponse>{

    private String FriendID;

    public ApproveFriendRequest() {
    }

    public ApproveFriendRequest(String FriendID) {
        this.FriendID = FriendID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<ApproveFriendResponse>> callback) {
        apiInterface.approveFriend(FriendID, NetworkManager.userParams(), callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
