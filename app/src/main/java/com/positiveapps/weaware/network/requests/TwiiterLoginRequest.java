package com.positiveapps.weaware.network.requests;

import com.positiveapps.weaware.network.ApiInterface;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.TwiiterLoginResponse;

import retrofit.Callback;

/**
 * Created by Izakos on 21/02/2016.
 */
public class TwiiterLoginRequest  extends RequestObject<TwiiterLoginResponse>{

    private String Token;
    private String Secret;
    private String UDID;

    public TwiiterLoginRequest() {
    }

    public TwiiterLoginRequest(String Token,String Secret,String UDID) {
        this.Token = Token;
        this.Secret = Secret;
        this.UDID = UDID;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ResponseObject<TwiiterLoginResponse>> callback) {
        apiInterface.Twitterlogin(Token,Secret,UDID, callback);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }
}
