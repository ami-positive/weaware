/**
 * 
 */
package com.positiveapps.weaware.animations;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Smart Custom Animation class that also affects the view itself during animation
 * @author Maor
 *
 */
public class MyScaleAnimation extends Animation {

	public static final int RESIZE = 1;
	public static final int RESIZE_AND_RETURN_TO_ORIGINAL = 2;
	public static final int EXPAND_FROM_ZERO_TO_OROGINAL = 3;
	
	private View targetView;
	private int originalWidth;
	private int originalHeight;
	private int newWidth;
	private int newHeight;
	private int animationType;
	
	/**
	 * 
	 * @param targetView - the view to change - has to be at least 1dp width/height
	 * @param newWidth - the new width (when resize - 0 for no changes)
	 * @param newHeight - the new height (when resize - 0 for no changes)
	 */
	public MyScaleAnimation(View targetView, int newWidth, int newHeight, int animationType) {
				
		this.targetView = targetView;
		this.originalWidth = targetView.getWidth();
		this.originalHeight = targetView.getHeight();
		this.newWidth = newWidth;
		this.newHeight = newHeight;
		this.animationType = animationType;
	}

	
	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		// TODO Auto-generated method stub
		super.applyTransformation(interpolatedTime, t);
		
        ViewGroup.LayoutParams lp = targetView.getLayoutParams();
        if(animationType == RESIZE){
        	if(newWidth != 0){
        		lp.width = (int) (originalWidth + (newWidth * interpolatedTime));
        	}
        	if(newHeight != 0){
        		lp.height = (int) (originalHeight + (newHeight * interpolatedTime));
        	}
        }else if(animationType == EXPAND_FROM_ZERO_TO_OROGINAL){
        	if(newWidth > 0){
        		lp.width = (int) (newWidth * interpolatedTime);
        	}

        	if(newHeight > 0){
        		lp.height = (int) (newHeight * interpolatedTime);
        	}
        }else if(animationType == RESIZE_AND_RETURN_TO_ORIGINAL){
        	if(newWidth != 0){
        		lp.width = (int) (originalWidth + (newWidth * interpolatedTime));
        	}
        	if(newHeight != 0){
        		lp.height = (int) (originalHeight + (newHeight * interpolatedTime));
        	}
        	
        	if(interpolatedTime == 1.0f){
        		lp.width = originalWidth;
        		lp.height = originalHeight;
        	}
        	
        }

        targetView.setLayoutParams(lp);

	}
	
}
