package com.positiveapps.weaware.util;

/**
 * Created by Izakos on 08/10/2015.
 */
public final class Static {

    public static final int ALERT_MODE = 1;
    public static final int NORMAL_MODE = 0;

    public static final int GALLARY_REQUEST_CODE = 1001;
    public static final int CAMERA_REQUEST_CODE = 1002;

    public static final int OPEN_EVENT = 1;
    public static final int CONFIRM_EVENT = 2;
    public static final int CLOSE_EVENT = 3;

    public static final String APP_TAG = "weware";
    public static final String APP_FOLDER = "/weware";
    public static final String CAMERA_PROFILE_PATH = APP_FOLDER + "/picture.jpg";
    public static final String URL = "url";
    public static final String URL_GIFT = "http://weaware.co/giftbox.html";
    public static final String URL_TUTORIAL_VID = "http://sdr.eu.com/en/";
    public static final String URL_UNIVERSITY = "http://sdr.eu.com/en/";


    public static final String REPORT_ID = "reportId";
    public static final String REPORT_SERVER_ID = "reportServerId";
    public static final String PUSH_TYPE = "pushType";
    public static final String SHOW_MAP = "showMap";
}