/**
 * 
 */
package com.positiveapps.weaware.util;

import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.location.StaticMarker;

import java.util.ArrayList;



/**
 * @author natiapplications
 *
 */
public class StaticMapUtil {
	
	public static String createStaticMapUrl(double lat, double lon, int size) {

		String url = "http://maps.google.com/maps/api/staticmap?center=" + lat
				+ "," + lon + "&zoom=17&size=" + size + "x" + size
				+ "&sensor=true&markers=color:red%7Clabel:S%7Csize:mid%7C" + lat
				+ "," + lon;
		return url;
	}
	
	
	public static String createStaticMapUrl(double lat, double lon, int with,int height, boolean withMarker) {

		String url = "http://maps.google.com/maps/api/staticmap?" +
				"center=" + lat + "," + lon +
				"&zoom=17" +
				"&size=" + with + "x" + height +
				"&sensor=true";
		if (withMarker){
			url += "&markers=color:red%7Clabel:A%7Csize:mid%7C" + lat + "," + lon;
		}
		
		return url;
	}

	
	public static String createStaticMapUrl(double lat, double lon, int with,int height,int zoom,
			boolean withMarker) {

		String url = "http://maps.google.com/maps/api/staticmap?" +
				"center=" + lat + "," + lon +
				"&zoom=" + zoom +
				"&size=" + with + "x" + height +
				"&sensor=true";
		if (withMarker){
			url += "&markers=color:red%7Clabel:A%7Csize:mid%7C" + lat + "," + lon;
		}
		
		return url;
	}
	
	public static String createStaticMapUrl(double lat, double lon, int with,int height,int zoom,
			ArrayList<StaticMarker> markers) {

		String url = "http://maps.google.com/maps/api/staticmap?" +
				"center=" + lat + "," + lon +
				"&zoom=" + zoom +
				"&size=" + with + "x" + height +
				"&sensor=true";
		for (int i = 0; i < markers.size(); i++) {
			StaticMarker temp = markers.get(i);
			url += "&markers=" +
					"color:" + temp.color +
					"%7C" +
					"label:" + temp.lable +
					"%7C" +
					"size:" + temp.size +
					"%7C" + temp.lat + "," + temp.lon;
		}
		
		
		
		return url;
	}
	
	public static  int calculateZoomLevel(int distance) {
	    double equatorLength = distance; // in meters
	    double widthInPixels = MyApp.generalSettings.getScreenWidth();
	    double metersPerPixel = equatorLength / 256;
	    int zoomLevel = 1;
	    while ((metersPerPixel * widthInPixels) > 2000) {
	        metersPerPixel /= 2;
	        ++zoomLevel;
	    }
	    
	    return zoomLevel;
	}
	
}
