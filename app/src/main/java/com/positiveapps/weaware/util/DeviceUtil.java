/**
 * 
 */
package com.positiveapps.weaware.util;

import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;

import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.R;

/**
 * @author Nati Gabay
 *
 */
public class DeviceUtil {
	
	public static String getUserEmail(){
		String possibleEmail = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; 
		Account[] accounts = AccountManager.get(MyApp.appContext).getAccounts();
		for (Account account : accounts) {
		    if (emailPattern.matcher(account.name).matches()) {
		    	possibleEmail = account.name;
		    }
		}
		return possibleEmail;
	}
	
	public static String getDeviceUDID() {

		return Secure.getString(MyApp.appContext.getContentResolver(),
				Secure.ANDROID_ID);
	}
	
	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	public static String getDeviceVersion () {
		
		String myVersion = "";
		if (Build.VERSION.RELEASE != null){
			myVersion = Build.VERSION.RELEASE;
		}
		return myVersion;
	}
	
	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	} 
	
	public static String GetCountryZipCode(Context context){
	    String CountryID="";
	    String CountryZipCode="";

	    TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	    //getNetworkCountryIso
	    CountryID= manager.getSimCountryIso().toUpperCase();
	    Log.e("zipCodeLog", "countryID = " + CountryID);
	    String[] rl=context.getResources().getStringArray(R.array.CountryCodes);
	    for(int i=0;i<rl.length;i++){
	        String[] g=rl[i].split(",");
	        if(g[1].trim().equals(CountryID.trim())){
	            CountryZipCode=g[0];
	            break;  
	        }
	    }
	    Log.e("zipCodeLog", "countryZipCode = " + CountryZipCode);
	    return CountryZipCode;
	}
	
	public static String GetCountryZipCode(Context context,String countryCode){
	    String CountryID=countryCode;
	    String CountryZipCode="";

	    Log.e("zipCodeLog", "countryID = " + CountryID);
	    String[] rl=context.getResources().getStringArray(R.array.CountryCodes);
	    for(int i=0;i<rl.length;i++){
	        String[] g=rl[i].split(",");
	        if(g[1].trim().equals(CountryID.trim())){
	            CountryZipCode=g[0];
	            break;  
	        }
	    }
	    Log.e("zipCodeLog", "countryZipCode = " + CountryZipCode);
	    return CountryZipCode;
	}
	
	public static String GetCountryID(){
	    String CountryID="";
	    TelephonyManager manager = (TelephonyManager) 
	    		MyApp.appContext.getSystemService(Context.TELEPHONY_SERVICE);
	    CountryID= manager.getSimCountryIso().toUpperCase();
	    if (CountryID.trim().isEmpty()){
	    	CountryID = "IL";
	    }
	    return CountryID;
	}
	
	
	
	public static ArrayList<String> getCurrencysList() {

		Locale [] locales = Locale.getAvailableLocales();
		ArrayList<String> currencyArray = new ArrayList<String>();

		for (int i = 0; i < locales.length; i++) {
			try {
				String temp = locales[i].getDisplayCountry().trim();
				
				if (temp != null && !temp.isEmpty())
					currencyArray.add(temp);
			}catch(Exception e){
				
			}
			
		}

		return currencyArray;

	}
	

}
