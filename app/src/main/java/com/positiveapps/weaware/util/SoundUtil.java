/**
 * 
 */
package com.positiveapps.weaware.util;

import android.media.MediaPlayer;

import com.positiveapps.weaware.main.MyApp;


/**
 * @author natiapplications
 *
 */
public class SoundUtil {
	
	public static void playSound(int soundFileId) {
		MediaPlayer mediaPlayer = MediaPlayer.create(MyApp.appContext,
				soundFileId);
		mediaPlayer.start();
	}

}
