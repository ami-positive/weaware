package com.positiveapps.weaware.util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.adapters.ReportsCursorAdapter;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.getSettingsRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getSettingsResponse;
import com.positiveapps.weaware.objects.User;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Izakos on 08/10/2015.
 */
public abstract class Helper {


    public static HashMap<Integer, String> colors;


    public static void createSDFolder() {
        File folder = new File(Environment.getExternalStorageDirectory() + Static.APP_FOLDER);
        if (!folder.exists()) {
            Log.d(Static.APP_TAG, "create folder on sd card " + Static.APP_FOLDER);
            folder.mkdir();
        }
    }

    public static void openCamera(Fragment fragment) {
        Log.d(Static.APP_TAG, "open camera");
        createSDFolder();
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri imageFileUri = Uri.parse("file:///sdcard/" + Static.CAMERA_PROFILE_PATH);
        takePicture.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
        fragment.startActivityForResult(takePicture, Static.CAMERA_REQUEST_CODE);
    }

    public static void openCamera(Activity activity) {
        Log.d(Static.APP_TAG, "open camera");
        createSDFolder();
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri imageFileUri = Uri.parse("file:///sdcard/" + Static.CAMERA_PROFILE_PATH);
        takePicture.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
        activity.startActivityForResult(takePicture, Static.CAMERA_REQUEST_CODE);
    }

    public static void openGallery(Fragment fragment) {
        Log.d(Static.APP_TAG, "open galery");
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        fragment.startActivityForResult(pickPhoto, Static.GALLARY_REQUEST_CODE);

    }

    public static void openGallery(Activity activity) {
        Log.d(Static.APP_TAG, "open galery");
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(pickPhoto, Static.GALLARY_REQUEST_CODE);

    }

    public static String getProfileImageAsBase64(Bitmap selectedImage) {
        if (selectedImage != null) {
            return BitmapUtil.getImageAsStringEncodedBase64(selectedImage);
        }
        return "";
    }

    public static void textWatcher(EditText editText, final TextView textView) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textView.setText(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static void textWatcherWithFilter(EditText editText, final ReportsCursorAdapter adapter) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    public static String getColorByRank(final int rank) {

        if(colors == null) {
            setRankColorsQuery(new onGetSetting() {
                @Override
                public void settingDone() {
                    getColorByRank(rank);
                }
            });
        }else{
            for (java.util.Map.Entry<Integer, String> entry : colors.entrySet()) {
                if (rank == entry.getKey()) {
                    return entry.getValue();
                }
            }
        }

        return "#FFFFFF";
    }

    public static void setRankColorsQuery(final onGetSetting callback){
        MyApp.networkManager.makeRequest(new getSettingsRequest(), new NetworkCallback<getSettingsResponse>() {
                    @Override
                    public void onResponse(boolean success, String errorDesc, ResponseObject<getSettingsResponse> response) {
                        super.onResponse(success, errorDesc, response);
                        if (success) {
                            colors =  colors = new HashMap<>();
                            for (int i = 0; i < response.getData().getRankColors().size(); i++) {
                                colors.put(i, response.getData().getRankColors().get(i));
                                if(User.getCurrentGrade() == i){
                                    MyApp.userProfile.setLevelColor(response.getData().getRankColors().get(i));
                                }
                            }
                            callback.settingDone();
                        } else {
                            ToastUtil.toster(errorDesc, true);
                        }
                    }
                });
    }

    public interface onGetSetting{
        public void settingDone();
    }

    public static void play(int audioSource){
        //set up MediaPlayer
        MediaPlayer mp = new MediaPlayer();
        try {
            mp= MediaPlayer.create(MyApp.appContext, audioSource);
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static ShapeDrawable drawCircle (int color) {
        int size = (int) AppUtil.convertDpToPixel(250f, MyApp.appContext);
        ShapeDrawable oval = new ShapeDrawable (new OvalShape());
        oval.setIntrinsicHeight (size);
        oval.setIntrinsicWidth (size);
        oval.getPaint ().setColor(color);
        return oval;
    }

    public static ShapeDrawable drawSmallCircle (int color) {
        int size = (int) AppUtil.convertDpToPixel(220f, MyApp.appContext);
        ShapeDrawable oval = new ShapeDrawable (new OvalShape());
        oval.setIntrinsicHeight (size);
        oval.setIntrinsicWidth (size);
        oval.getPaint ().setColor(color);
        return oval;
    }

    public static Drawable bigButton(int color){
        Drawable myIcon = MyApp.appContext.getResources().getDrawable( R.drawable.untitled );
        int size = (int) AppUtil.convertDpToPixel(15f, MyApp.appContext);
        int sizePlus = (int) AppUtil.convertDpToPixel(20f, MyApp.appContext);
        Drawable[] drawarray = {drawCircle(color), drawSmallCircle(color), myIcon};
        LayerDrawable layerdrawable = new LayerDrawable(drawarray);
        layerdrawable.setLayerInset(1, size, size, size, size);
        layerdrawable.setLayerInset(2, sizePlus, size, size, size);
        return layerdrawable;
    }





}

