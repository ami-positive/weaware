/**
 * 
 */
package com.positiveapps.weaware.util;

import com.positiveapps.weaware.main.MyApp;

import java.text.DecimalFormat;


/**
 * @author natiapplications
 *
 */
public class DistanceDescriptor {
	
	private double value;
	private String description;
	public DistanceDescriptor (double value){
		this.value = value;
		buildDescription ();
	}
	
	
	private void buildDescription() {
	/*	if (value >= MyApp.userSettings.getDistanceUnitFactor()){
			value /= MyApp.userSettings.getDistanceUnitFactor();
			description  = new DecimalFormat("##.##").format(value) +
					" " + MyApp.userSettings.getDistanceUnitName();
		}else {
			description  = new DecimalFormat("##.##").format(value) +
					" " + "m";
		}*/
		
		
	}
	
	public String describe () {
		if (description != null && !description.isEmpty()){
			return description;
		}
		return "0.0" +
				" " + "m";
	}

}
