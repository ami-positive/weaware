/**
 * 
 */
package com.positiveapps.weaware.util;

import android.R;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.widget.ImageView;

import com.positiveapps.weaware.main.MyApp;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;


/**
 * @author natiapplications
 *
 */
public class ImageLoader {
	
	public static final int IMAGE_TYPE_FROM_CAMERA = 1;
	public static final int IMAGE_TYPE_FROM_GALLERAY = 2;
	
	public static final int DEAFULT_PLACE_HOLDER = R.drawable.ic_menu_gallery;
	
	
	
	
	public static Bitmap byUri(int type,Uri uri,ImageView into){
		return byUri(type, uri, into, DEAFULT_PLACE_HOLDER, false, true, true);
	}
	
	public static Bitmap byUri(int type,Uri uri,ImageView into,int deafult){
		return byUri(type, uri, into, deafult, true, true, true);
	}
	
	
	public static Bitmap byUri(int type,Uri uri,ImageView into,int deafult
			,boolean lowQuality,boolean centerCrup,boolean rotate) {
		
		if (uri == null){return null;}
		
		Bitmap bitmap = null;
		try {
			bitmap = Media.getBitmap(MyApp.appContext.getContentResolver(), uri);
		} catch (Exception e) {}
		
		if (bitmap == null){
			try {
				bitmap = BitmapFactory.decodeFile(getPathOfImagUri(uri));	
			} catch (Exception e) {}
			if (bitmap == null){
				try {
					bitmap = BitmapFactory.decodeStream
							(MyApp.appContext.getContentResolver().openInputStream(uri));
				} catch (Exception e) {}
			}
		}
		
		if (bitmap == null){
			setImageInto(bitmap, into, deafult);
			return null;
		}
		
		if (lowQuality) bitmap = lowQualityOfImage(uri, bitmap);
		if (centerCrup) bitmap  = crupAndScale(bitmap);
		if (rotate)  bitmap = rotate(type, uri, bitmap);
	    if (into != null) setImageInto(bitmap, into, deafult); 
	    
	    
	    return bitmap;
	}
	
	
	public static void byUriUsingPicaso(int type,Uri uri,ImageView into){
		byUriUsingPicaso(type, uri, into, DEAFULT_PLACE_HOLDER, true, true, true,0,0,null);
	}
	
	public static void byUriUsingPicaso(int type,Uri uri,ImageView into,int deafult){
		byUriUsingPicaso(type, uri, into, deafult, true, true, true,0,0,null);
	}
	
	public static void byUriUsingPicaso(final int type,final Uri uri,
			ImageView into,int deafult,final boolean lowQuality,
			final boolean centerCrup,final boolean rotate,
			int newWidth,int newHieght,
			Callback callback){
		
		if (uri == null){return;}
		
		RequestCreator requestCreator = Picasso.with(MyApp.appContext)
		.load(uri).placeholder(deafult).error(deafult);
		
		if (centerCrup){
			requestCreator.transform(new Transformation() {
				@Override
				public Bitmap transform(Bitmap source) {
					if (lowQuality) source = lowQualityOfImage(uri, source);
					Bitmap result  = crupAndScale(source);
					if (rotate)  result = rotate(type, uri, result);
					return result;
				}

				@Override
				public String key() {
					return "square()";
				}
			});
		}
		
		if (newWidth > 0 && newHieght > 0){
			if (!centerCrup){
				requestCreator.resize(newWidth, newHieght);
			}
		}
			
		if (callback != null){
			requestCreator.into(into,callback);
		}else{
			requestCreator.into(into);
		}
	}
	
	
	public static Bitmap byStringPath(String path,ImageView into){
		File file = new File(path);
		return byFile( file, into,DEAFULT_PLACE_HOLDER,true,true);
	}
	
	public static Bitmap byStringPath(String path,ImageView into,int deafult){
		File file = new File(path);
		return byFile(file, into,deafult,true,true);
	}
	
	
	public static Bitmap byStringPath(String path,ImageView into,int deafult
			,boolean lowQuality,boolean centerCrup) {
		File file = new File(path);
		return byFile(file, into, deafult, lowQuality, centerCrup);
	}
	
	
	
	public static Bitmap byFile(File file,ImageView into){
		return byFile(file, into,DEAFULT_PLACE_HOLDER,true,true);
	}
	
	public static Bitmap byFile(File file,ImageView into,int deafult){
		return byFile(file, into,deafult,true,true);
	}
	
	
	
	public static Bitmap byFile(File file,ImageView into,int deafult
			,boolean lowQuality,boolean centerCrup) {
		
		if (file == null){return null;}
		
		Bitmap bitmap = null;
		try {
			bitmap = BitmapFactory.decodeFile(file.getPath());
		} catch (Exception e) {}
		if (bitmap == null){
			try {
				bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
			} catch (Exception e) {}
		}
		
		if (bitmap == null){
			setImageInto(bitmap, into, deafult);
			return null;
		}
		
		if (lowQuality) bitmap = lowQualityOfImage(file.getAbsolutePath(), bitmap);
		if (centerCrup) bitmap  = crupAndScale(bitmap);
	    if (into != null) setImageInto(bitmap, into, deafult); 
	    
	    return bitmap;
	}
	
	
	
	
	public static void byStringPathUsingPicso(String path,ImageView into){
		File file = new File(path);
		byFileUsingPicaso(file, into);
	}
	
	public static void byStringPathUsingPicso(String path,ImageView into,int deafult){
		File file = new File(path);
		byFileUsingPicaso(file, into, deafult);
	}
	
	public static void byFileUsingPicaso(String path,ImageView into,int deafult,int newWidth,int newHieght){
		File file = new File(path);
		byFileUsingPicaso(file, into, deafult, false, false, newWidth, newHieght, null);
	}
	public static void byStringPathUsingPicso(final String path, 
			final ImageView into,final int deafult,
			final boolean lowQuality,final boolean centerCrup,
			int newWidth,int newHieght,
			Callback callback) {
		File file = new File(path);
		byFileUsingPicaso(file, into, deafult, lowQuality,
				centerCrup, newWidth, newHieght, callback);
	}
	
	
	
	public static void byFileUsingPicaso(File file,ImageView into){
		byFileUsingPicaso(file, into,DEAFULT_PLACE_HOLDER,true,true,0,0,null);
	}
	
	public static void byFileUsingPicaso(File file,ImageView into,int deafult){
		byFileUsingPicaso(file, into,deafult,true,true,0,0,null);
	}
	
	
	public static void byFileUsingPicaso(File file,ImageView into,int deafult,int newWidth,int newHieght){
		byFileUsingPicaso(file, into, deafult, false, false, newWidth, newHieght, null);
	}
	
	public static void byFileUsingPicaso(final File file, 
			final ImageView into,final int deafult,
			final boolean lowQuality,final boolean centerCrup,
			final int newWidth,final int newHieght,
			final Callback callback){
		
		if (file == null){return;}
		
		RequestCreator requestCreator = Picasso.with(MyApp.appContext)
		.load(file).placeholder(deafult).error(deafult);
		
		
		if (centerCrup) {
			requestCreator.transform(new Transformation() {

				@Override
				public Bitmap transform(Bitmap source) {
					Bitmap result = null;
					if (newWidth > 0 && newHieght > 0){
						result = scaleCenterCrop(source, newWidth, newHieght);
					}else{
						result = crupAndScale(source);
					}
				    
					return result;
				}

				@Override
				public String key() {
					return "square()";
				}
			});
		}
		
		
		if (newWidth > 0 && newHieght > 0){
			if (!centerCrup){
				requestCreator.resize(newWidth, newHieght);
			}
		}
		
		requestCreator.into(into, new Callback() {

			@Override
			public void onError() {
				// TODO Auto-generated method stub
				if (callback != null)
					callback.onError();
			}

			@Override
			public void onSuccess() {
				// TODO Auto-generated method stub
				reRequestFile(file, into, deafult, lowQuality, centerCrup,
						newWidth, newHieght, callback);

			}

		});
		
		
	}



	public static void ByUrl(String url, final ImageView into,final Callback callback){
		ByUrl(url, into, DEAFULT_PLACE_HOLDER, true, 0, 0, null);
	}

	public static void ByUrl(String url, final ImageView into){
		ByUrl(url, into, DEAFULT_PLACE_HOLDER, true, 0, 0, null);
	}
	
	public static void ByUrl(String url, final ImageView into, 
			final int deafult){
		ByUrl(url, into, deafult, true, 0, 0, null);
	}
	
	public static void ByUrl(String url, final ImageView into, 
			final int deafult,final int newWidth, final int newHieght){
		ByUrl(url, into, deafult, false,newWidth, newHieght, null);
	}
	
	
	public static void ByUrl(String url, final ImageView into,
			final int deafult,final boolean centerCrup,
			final int newWidth, final int newHieght, 
			final Callback callback){

		final String copyUrl = url;
    	if(url == null || url.isEmpty()){
    		url = "no_image";
    	}
    	
		RequestCreator requestCreator = Picasso.with(MyApp.appContext)
		.load(url).placeholder(deafult).error(deafult);
		
		if (centerCrup){
			requestCreator.transform(new Transformation() {

				@Override
				public Bitmap transform(Bitmap source) {
					Bitmap result = null;
					if (newWidth > 0 && newHieght > 0){
						result = scaleCenterCrop(source, newWidth, newHieght);
					}else{
						result = crupAndScale(source);
					}
					
					return result;
				}
				@Override
				public String key() {
					return "square()";
				}
			});
			
		}
		
		if (newWidth > 0 && newHieght > 0){
			if (!centerCrup){
				requestCreator.resize(newWidth, newHieght);
			}
		}
		
		requestCreator.into(into,new Callback(){
			@Override
			public void onError() {
				Log.e("reRequestLog", "onError");
				if (callback != null)
					callback.onError();
			}
			@Override
			public void onSuccess() {
				Log.e("reRequestLog", "reRequest");
				reRequestUrl(copyUrl, into, deafult, 
						centerCrup, newWidth, newHieght, callback);
			}
		});
		
    }
	
	private static void reRequestFile (final File file, 
			final ImageView into,final int deafult,
			final boolean lowQuality,final boolean centerCrup,
			final int newWidth,final int newHieght,
			Callback callback){
		
		if (file == null){return;}
		
		RequestCreator requestCreator = Picasso.with(MyApp.appContext)
		.load(file).placeholder(deafult).error(deafult);
		
		
		if (centerCrup) {
			requestCreator.transform(new Transformation() {

				@Override
				public Bitmap transform(Bitmap source) {
					Bitmap result = null;
					if (newWidth > 0 && newHieght > 0){
						result = scaleCenterCrop(source, newWidth, newHieght);
					}else{
						result = crupAndScale(source);
					}
					return result;
				}

				@Override
				public String key() {
					return "square()";
				}
			});
		}
		
		
		if (newWidth > 0 && newHieght > 0){
			if (!centerCrup){
				requestCreator.resize(newWidth, newHieght);
			}
		}
		
		if (callback != null){
			requestCreator.into(into,callback);
		}else{
			requestCreator.into(into);
		}
	}
	
	private static void reRequestUrl (String url, final ImageView into, 
			final int deafult,final boolean centerCrup,
			final int newWidth, final int newHieght, 
			final Callback callback){
		
		if(url == null || url.isEmpty()){
    		url = "no_image";
    	}
    	
		RequestCreator requestCreator = Picasso.with(MyApp.appContext)
		.load(url).placeholder(deafult).error(deafult);
		
		if (centerCrup){
			requestCreator.transform(new Transformation() {

				@Override
				public Bitmap transform(Bitmap source) {
					Bitmap result = null;
					if (newWidth > 0 && newHieght > 0){
						result = scaleCenterCrop(source, newWidth, newHieght);
					}else{
						result = crupAndScale(source);
					}
					return result;
				}
				@Override
				public String key() {
					return "square()";
				}
			});
			
		}
		
		if (newWidth > 0 && newHieght > 0){
			if (!centerCrup){
				requestCreator.resize(newWidth, newHieght);
			}
		}
		if (callback != null){
			requestCreator.into(into,callback);
		}else{
			requestCreator.into(into);
		}
		
	}

	public static void setImageInto (Bitmap image,ImageView into,int deafult) {
		if (image != null){
			into.setImageBitmap(image);
	    }else{
	    	into.setImageResource(deafult);
	    }
	}
	
	public static  Bitmap crupAndScale (Bitmap source){
		
		int size = Math.min(source.getWidth(), source.getHeight());
	    int x = (source.getWidth() - size) / 2;
	    int y = (source.getHeight() - size) / 2;
		int sizeA = (int)AppUtil.convertDpToPixel(130, MyApp.appContext);
	    Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
	    result = Bitmap.createScaledBitmap(result, sizeA, sizeA, false);
	    if (result != source) {
		   source.recycle();
		}
	    
		return result;
	}
	
	public static  String getPathOfImagUri (Uri selectedUri){
		String[] filePathColumn = { Media.DATA };
		Cursor cursor = MyApp.appContext.getContentResolver().query(selectedUri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();
		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String picturePath = cursor.getString(columnIndex);
		cursor.close();
		return picturePath;
	}
	

	

	public static Bitmap lowQualityOfImage(Uri uri,Bitmap source) {
		
		if (source == null){return source;}
		
		int simpleSize = 1;
		int width = source.getWidth();
		int height = source.getHeight();
		int bigSide = width >= height ? width : height;
		if (bigSide > 300) {
			simpleSize = bigSide / 300;
		}
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
		
		Bitmap result = null;
		try {
			result = BitmapFactory.decodeFile(getPathOfImagUri(uri),options);	
		} catch (Exception e) {}
		if (result == null){
			try {
				result = BitmapFactory.decodeStream(MyApp.appContext.getContentResolver().openInputStream(uri),
						null, options);
			} catch (Exception e) {}
		}
		if (result == null){return source;}
		
		return result;
	}
	
	public static Bitmap lowQualityOfImage(String path) {
		// option that resize the bitmap
		int simpleSize = 1;
		Bitmap temp = BitmapFactory.decodeFile(path);
		int width = temp.getWidth();
		int height = temp.getHeight();
		int bigSide = width >= height ? width : height;
		if (bigSide > 300) {
			simpleSize = bigSide / 300;
		}
		
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
		
		return BitmapFactory.decodeFile(path, options);
	}
	
	public static Bitmap lowQualityOfImage(String path,Bitmap source) {
		
		if (source == null){return source;}
		
		int simpleSize = 1;
		int width = source.getWidth();
		int height = source.getHeight();
		int bigSide = width >= height ? width : height;
		if (bigSide > 300) {
			simpleSize = bigSide / 300;
		}
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
		
		Bitmap result = null;
		try {
			result = BitmapFactory.decodeFile(path,options);	
		} catch (Exception e) {}
		if (result == null){
			try {
				result = BitmapFactory.decodeStream(new FileInputStream(new File(path)),
						null, options);
			} catch (Exception e) {}
		}
		if (result == null){return source;}
		
		return result;
	}
	
	
	/*
	 * rotate image
	 * */
	public static Bitmap rotate(int type,Uri imageUri,Bitmap image) {
		int oriantation =0;
		if (type == IMAGE_TYPE_FROM_CAMERA){
			 oriantation = getCameraPhotoOrientation(imageUri);
		}else if (type == IMAGE_TYPE_FROM_GALLERAY){
			 oriantation = getOrientation(imageUri);
		}
		
		Log.e("oriantationLog", "ori = " + oriantation);
		if (oriantation  <= 0){
			return image;
		}
		int w = image.getWidth();
		int h = image.getHeight();

		Matrix mtx = new Matrix();
		mtx.postRotate(oriantation);

		return Bitmap.createBitmap(image, 0, 0, w, h, mtx, true);
	}

	
	public static int getOrientation(Uri photoUri) {
	    Cursor cursor = MyApp.appContext.getContentResolver().query(photoUri,
	            new String[] { MediaStore.Images.ImageColumns.ORIENTATION },
	            null, null, null);

	    try {
	        if (cursor.moveToFirst()) {
	            return cursor.getInt(0);
	        } else {
	            return -1;
	        }
	    } finally {
	        cursor.close();
	    }
	}
	
	/*
	 * 
	 * check if image needed rotating
	 * */
	public static int getCameraPhotoOrientation(Uri imageUri) {
		int rotate = 0;
		try {
			MyApp.appContext.getContentResolver().notifyChange(imageUri, null);

			ExifInterface exif = new ExifInterface(imageUri.getPath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rotate;
	}

	public static void loadImageAndSaveItOnStorage (final String imageName,final String url,final LoadImageCallback callback){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
			Bitmap bitmap = null;
			 try {
				String fileName = BitmapUtil.cretatStringPathForStoringMedia(imageName); 
				bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
				if (bitmap == null){
					return;
				}
				FileOutputStream out = new FileOutputStream(fileName);
			    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); 
			    if (callback != null){
			    	callback.onLoadImage(true, bitmap,fileName);
			    }
			} catch (Exception e) {
				e.printStackTrace();
				if (callback != null){
			    	callback.onLoadImage(false, bitmap,null);
			    }
			}
			}
		}).start();
	}
	
	
	public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
	    int sourceWidth = source.getWidth();
	    int sourceHeight = source.getHeight();

	    // Compute the scaling factors to fit the new height and width, respectively.
	    // To cover the final image, the final scaling will be the bigger 
	    // of these two.
	    float xScale = (float) newWidth / sourceWidth;
	    float yScale = (float) newHeight / sourceHeight;
	    float scale = Math.max(xScale, yScale);

	    // Now get the size of the source bitmap when scaled
	    float scaledWidth = scale * sourceWidth;
	    float scaledHeight = scale * sourceHeight;

	    // Let's find out the upper left coordinates if the scaled bitmap
	    // should be centered in the new size give by the parameters
	    float left = (newWidth - scaledWidth) / 2;
	    float top = (newHeight - scaledHeight) / 2;

	    // The target rectangle for the new, scaled version of the source bitmap will now
	    // be
	    RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

	    // Finally, we create a new bitmap of the specified size and draw our new,
	    // scaled bitmap onto it.
	    Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
	    Canvas canvas = new Canvas(dest);
	    canvas.drawBitmap(source, null, targetRect, null);

	    if (dest != source) {
			source.recycle();
		}
	    Log.e("scaleCenterCrop", "scaleCenterCrop W = " + dest.getWidth()
	    		+ " H = " + dest.getHeight() );
	    return dest;
	}
	
	public interface LoadImageCallback {
		public void onLoadImage(boolean success, Bitmap image, String fileName);
	}
	
}
