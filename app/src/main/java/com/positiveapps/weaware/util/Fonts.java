/**
 * 
 */
package com.positiveapps.weaware.util;

/**
 * @author natiapplications
 *
 */
public class Fonts {
	
	public static final String 	NEXA_BOLD = "nexa_bold.otf";
	public static final String 	NEXA_LIGHT = "nexa_light.otf";
	public static final String 	NEXA_REGULAR = "nexa_regular.ttf";
	

}
