package com.positiveapps.weaware.util;

import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

/**
 * @author Andrey Tretiakov 12.06.2015.
 */
public class FilterHelper {

    public static final InputFilter amountFilter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//            Log.d("filter", "source = " + source + ", start = " + start + ", end = " + end + ", dest = " + dest + ", dsstart = " + dstart + ", dend = " + dend);

            StringBuilder filteredStringBuilder = new StringBuilder();
            if (dest.length() == 8)
                return filteredStringBuilder.toString();

            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);
                if (Character.isDigit(currentChar) || currentChar == '.') {
                    String s = String.valueOf(dest);
                    filteredStringBuilder.append(currentChar);
                }
            }

            return filteredStringBuilder.toString();
        }
    };

    public static final InputFilter onlyDigits = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            StringBuilder filteredStringBuilder = new StringBuilder();
            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);
                if (Character.isDigit(currentChar))
                    filteredStringBuilder.append(currentChar);
            }

            return filteredStringBuilder.toString();
        }
    };

    public static final InputFilter onlyDigitsPin = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            StringBuilder filteredStringBuilder = new StringBuilder();

            if (dest.length() > 4)
                return filteredStringBuilder.toString();

            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);
                if (Character.isDigit(currentChar))
                    filteredStringBuilder.append(currentChar);
            }

            return filteredStringBuilder.toString();
        }
    };

    public static final InputFilter appendCharactersAndDigitsAndLockFirstSpace = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source instanceof SpannableStringBuilder) {
                SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder)source;
                for (int i = end - 1; i >= start; i--) {
                    char currentChar = source.charAt(i);
                    if (!Character.isLetterOrDigit(currentChar) && !Character.isSpaceChar(currentChar)) {
                        sourceAsSpannableBuilder.delete(i, i+1);
                    }
                }
                return source;

            } else {
                StringBuilder filteredStringBuilder = new StringBuilder();
                if (dest != null && dest.length() == 0 && source.length() != 0 && source.charAt(0) == ' ')
                    return filteredStringBuilder.toString();

                if (dest != null && dest.length() > 19)
                    return filteredStringBuilder.toString();

                for (int i = start; i < end; i++) {
                    char currentChar = source.charAt(i);
                    if (Character.isLetterOrDigit(currentChar) || Character.isSpaceChar(currentChar)) {
                        filteredStringBuilder.append(currentChar);
                    }
                }

                return filteredStringBuilder.toString();
            }
        }
    };
}
