/**
 * 
 */
package com.positiveapps.weaware.util;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import android.util.Log;
import android.widget.TextView;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * @author natiapplications
 *
 */
public class TextUtil {
	
	
	public static boolean baseFieldsValidation (TextView... toValid){
		boolean result = true;
		for (int i = 0; i < toValid.length; i++) {
			if (toValid[i].getText().toString().isEmpty()){
				toValid[i].setError("Required field");
				result =  false;
			}else{
				try {
					toValid[i].setError(null);
				} catch (Exception e) {}
			}
		}
		return result;
	}
	
	public static String stringByAddingPercentEscapesUsingEncoding( String input, String charset ) throws UnsupportedEncodingException {
	    byte[] bytes = input.getBytes(charset);
	    StringBuilder sb = new StringBuilder(bytes.length);
	    for( int i = 0; i < bytes.length; ++i ) {
	        int cp = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
	        if( cp <= 0x20 || cp >= 0x7F || (
	            cp == 0x22 || cp == 0x25 || cp == 0x3C ||
	            cp == 0x3E || cp == 0x20 || cp == 0x5B ||
	            cp == 0x5C || cp == 0x5D || cp == 0x5E ||
	            cp == 0x60 || cp == 0x7b || cp == 0x7c ||
	            cp == 0x7d
	            )) {
	            sb.append( String.format( "%%%02X", cp ) );
	        }
	        else {
	            sb.append( (char)cp );
	        }
	    }
	    return sb.toString();
	}

	public static String stringByAddingPercentEscapesUsingEncoding( String input ) {
	    try {
	        return stringByAddingPercentEscapesUsingEncoding(input, "UTF-8");
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("Java platforms are required to support UTF-8");
	        // will never happen
	    }
	}
	
	public static String getNumberFromString (String toParse){
		String result = "";
		for (int i = 0; i < toParse.length(); i++) {
			char temp = toParse.charAt(i);
			if (temp >='0'&&temp <='9'){
				result+=temp;
			}
		}
		return result;
	}
	
	public static String normalizePhoneNumber (String phone){
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		if (phone.contains("+")){
			
			return "+"+getNumberFromString(phone);
		}
		String clean = getNumberFromString(phone);
		PhoneNumber num = null;
		try {
		Log.e("locallog","locale = " + DeviceUtil.GetCountryID());
		   num = phoneUtil.parse(clean,  DeviceUtil.GetCountryID());
		} catch (NumberParseException e) {
		  System.err.println("NumberParseException was thrown: " + e.toString());
		  return null;
		}
		if(num == null){
			return null;
		}
		if (phoneUtil.isValidNumber(num)){
			return phoneUtil.format(num, PhoneNumberFormat.E164);
		}
		return null;
		
	}

	
	public static String[] splitDouble (String doubleString){
		String[] result = new String[2];
		String a = "";
		String b = "";
		boolean isEndSide = false;
		for (int i = 0; i < doubleString.length(); i++) {
			char temp = doubleString.charAt(i);
			if (temp == '.'){
				isEndSide = true;
				continue;
			}
			if (isEndSide){
				b+=temp;
			}else{
				a+=temp;
			}
			
		}
		if (a.isEmpty()){
			a ="0";
		}if (b.isEmpty()){
			b = "00";
		}
		result[0] = a;
		result[1] = b;
		return result;
	}
	
	public static int getStartDoubleLength (double value){
		String priceString  = new DecimalFormat("##.##").format(value);
		int counter = 0;
		for (int i = 0; i < priceString.length(); i++) {
			char temp = priceString.charAt(i);
			if (temp != '.'){
				counter++;
			}else{
				break;
			}
		}
		return counter;
	}
	
	public static void setContentText (TextView toSet,int maxLength) {
		String text = toSet.getText().toString();
		if (text.length() > maxLength){
			text = toSet.getText().toString().substring(0,maxLength);
		}
		AppUtil.linkifyReadMore(toSet, text);
	}
	
	public static boolean isValidValue (String str){
		if (str!=null&&!str.trim().isEmpty()){
			return true;
		}
		return false;
	}
	

}
