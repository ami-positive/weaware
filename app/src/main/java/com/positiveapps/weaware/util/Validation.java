package com.positiveapps.weaware.util;

import android.content.res.Resources;
import android.widget.EditText;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.phone.PhoneNumberParser;


/**
 * Created by Izakos on 19/10/2015.
 */
public abstract class Validation {

    public static Resources resource = MyApp.appContext.getResources();

    public static boolean password(EditText pass, EditText phone, EditText name, PhoneNumberParser numberParser){

        String Name = name.getText().toString().trim();
        String Pass = pass.getText().toString().trim();
        String Phone = phone.getText().toString().trim();

        if(Name.isEmpty()){
            name.setError(resource.getString(R.string.user_name_missing));
            return false;
        }else if(pass.length() < 4){
            pass.setError(resource.getString(R.string.passwords_length_must_have_a_minimum_of_8_characters));
            return false;
        }else if(Phone.isEmpty()){
            phone.setError(resource.getString(R.string.provide_phone));
            return false;
        }else if(!numberParser.attemptLogin()){
            return false;
        }
        return true;
    }

    public static boolean password(TextView passET, String pass){
        if(pass.length() < 4){
            passET.setError(resource.getString(R.string.passwords_length_must_have_a_minimum_of_8_characters));
            return false;
        }
        return true;
    }


}
