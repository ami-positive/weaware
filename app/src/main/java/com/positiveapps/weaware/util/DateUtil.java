/**
 * 
 */
package com.positiveapps.weaware.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.util.Log;

/**
 * @author Nati Gabay
 *
 */
public class DateUtil {
	
	public static final String 	FORMAT_AMERICAN_SERVER = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_JUST_DATE = "dd/MM/yyyy";
	public static final String FORMAT_JUST_TIME = "HH:mm:ss";
	public static final String FORMAT_DEAFULT = "dd/MM/yyyy HH:mm:ss";
	
	
	public static final int DESC_DOW_M_DOM_Y = 0;
	public static final int DESC_M_DOM_Y = 1;
	public static final int DESC_DOW_M_DOM = 2;
	public static final int DESC_M_DOM = 3;
	public static final int DESC_DOW_M = 4;
	public static final int DESC_TIME_DOW_M_DOM_Y = 5;
	public static final int DESC_TIME_M_DOM_Y = 6;
	
	public static String[] months = {"January","February","March","April",
		"May","June","July","August","September","October","November","December"};
	public static String[] daysOfWeek = {"Sunday","Monday","Thursday","Wednesday",
		"Tuesday","Friday","Saturday"};


	public static String getCurrentTimeStamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	public static long milliFromString(String dateString) {
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(date == null){
			return 0;
		}
		return date.getTime();
	}

	public static String stringFromMilli (long milli) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(calendar.getTime());
	}
	
	
	public static String getCurrentDateAsString (String format) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(calendar.getTime());
	}
	
	public static long getCurrentDateInMilli () {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTimeInMillis();
	}
	
	public static String getDateAsStringByMilli (String format,long milli) {
		Log.d("dataPikerLog", "millis = " + milli);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		return sdf.format(calendar.getTime());
	}
	
	public static String getTimeAsStringByMilli (String format,long milli) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		return sdf.format(calendar.getTime());
	}
	
	public static String getMonthName (int month){
		if (month < 1){
			month = 1;
		}
		if (month > 12){
			month = 12;
		}
		return months[month-1];
	}
	
	
	public static String getDayOfWeekName(int dow){
		if (dow >= daysOfWeek.length){
			dow = daysOfWeek.length -1;
		}
		return daysOfWeek[dow];
	}
	
	public static String getDayOfWeekName(int dow,Calendar calendar){
		if (calendar.getTime().compareTo(Calendar.getInstance().getTime()) == 0){
			return "Today";
		}else if (calendar.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)
				&&calendar.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)
				&&calendar.get(Calendar.DAY_OF_MONTH) -1 == 
				Calendar.getInstance().get(Calendar.DAY_OF_MONTH)){
			return "Tommarow";
		}else{
			return daysOfWeek[dow];
		}
	}
	
	public static String parseDateStringByFormat (String fromformat,String date,String toFormat){
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromformat);
		Date d = null;
		try {
			d = simpleDateFormat.parse(date);
		} catch (ParseException e) {}
		if (d != null){
			simpleDateFormat.applyPattern(toFormat);
			return simpleDateFormat.format(d);
		}else{
			return "";
		}
		
	}
	
	public static long getMillisOfParsingDateString (String fromformat,String date){
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromformat);
		Date d = null;
		try {
			d = simpleDateFormat.parse(date);
		} catch (ParseException e) {}
		if (d == null){
			return getCurrentDateInMilli();
		}
		return d.getTime();
		
	}
	
	public static long getMillisOfParsingDateString (String fromformat,String date,int fallback){
		
		if (date.equalsIgnoreCase("0000-00-00 00:00:00")){
			return fallback;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromformat);
		Date d = null;
		try {
			d = simpleDateFormat.parse(date);
		} catch (ParseException e) {}
		if (d == null){
			return fallback;
		}
		return d.getTime();
		
	}
	
	public static long getMillisOfParsingDateStringSetTimeZone (String fromformat,String date,int fallback){
		
		if (date.equalsIgnoreCase("0000-00-00 00:00:00")){
			return fallback;
		}
		
		SimpleDateFormat sdf =  new SimpleDateFormat(fromformat);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		try {
			Date parseDate = sdf.parse(date);
			
			SimpleDateFormat sdff =  new SimpleDateFormat(fromformat);
			sdff.setTimeZone(TimeZone.getDefault());
			
			Date d = sdff.parse(sdff.format(parseDate));
			
			return d.getTime();
		} catch (Exception e) {}
		return fallback;
		
	}
	
	
	
	
	public static long getDateInMilliByPickerFields (int day, int month, int yeers,int houers,int minutes,int seconds){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, yeers);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.HOUR, houers);
		calendar.set(Calendar.MINUTE, minutes);
		calendar.set(Calendar.SECOND, seconds);
		return calendar.getTimeInMillis();
	}
	
	
	
	public static String getDateAsStringByPickerFields (String format,
			int day, int month, int yeers,int houers,int minutes,int seconds) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, yeers);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.HOUR, houers);
		calendar.set(Calendar.MINUTE, minutes);
		calendar.set(Calendar.SECOND, seconds);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(calendar.getTime());
	}
	
	public static long getTimeInMilli(int hower, int seconds){
		long h = hower*360000;
		long s = seconds*60000;
		return h+s;
	}
	
	public static int[] getBestDateForDatePicker (String date){
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		try {
			String curretnSetDate[] = date.split("-");
			if (curretnSetDate.length == 3){
				year = Integer.parseInt(curretnSetDate[2]);
				month = Integer.parseInt(curretnSetDate[1])-1;
				day = Integer.parseInt(curretnSetDate[0]);
			}
		} catch (Exception e) {}
		
		int[] result = {day,month,year};
		return result;
	}
	
	public static int[] getBestTime (String time){
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		
		String[] curretnSetTime = time.split(":");
		if (curretnSetTime.length == 2){
			hour = Integer.parseInt(curretnSetTime[0]);
			minute = Integer.parseInt(curretnSetTime[1]);
		}
		int[] result = {hour,minute};
		return result;
	}
	
	
	
	public static String getDateDescriptionByDate (Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String month = months[calendar.get(Calendar.MONTH)];
		
		return month + " " + calendar.get(Calendar.DAY_OF_MONTH)
				+ "." + calendar.get(Calendar.YEAR);
	}
	
	
	
	public static long getRangBetweenTwoDates (Date date1,Date date2) {
		long from = date1.getTime();
		long to = date2.getTime();
		long disTance = to - from;
		if (disTance <=0){
			return 0;
		}
		long result = disTance/60000;
		return result;
	}
	

	//DESCRIPTOR
	
	public static String getDateDescription (String fromFormat,String date,int descFormat){
		long millis = getMillisOfParsingDateString(fromFormat, date);
		return getDateDescription(millis, descFormat);
	}
	
	
	public static String getDateDescription (int day, int month, int yeers,int houers,int minutes,int seconds,int descFormat){
		long millis = getDateInMilliByPickerFields(day, month, yeers, houers, minutes, seconds);
		return getDateDescription(millis, descFormat);
	}
	
	public static String getDateDescription (long millis,int descFormat){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		StringBuilder result = new StringBuilder();
		switch (descFormat) {
		case DESC_DOW_M_DOM_Y:
			result.append(getDayOfWeekName(calendar.get(Calendar.DAY_OF_WEEK)-1));
			result.append(", ");
			result.append(getMonthName(calendar.get(Calendar.MONTH)));
			result.append(" ");
			result.append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
			result.append(", ");
			result.append(String.valueOf(calendar.get(Calendar.YEAR)));
			break;
		case DESC_DOW_M_DOM:
			result.append(getDayOfWeekName(calendar.get(Calendar.DAY_OF_WEEK)-1));
			result.append(", ");
			result.append(getMonthName(calendar.get(Calendar.MONTH)));
			result.append(" ");
			result.append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
			result.append(", ");
			break;
		case DESC_M_DOM_Y:
			result.append(getMonthName(calendar.get(Calendar.MONTH)));
			result.append(" ");
			result.append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
			result.append(", ");
			result.append(String.valueOf(calendar.get(Calendar.YEAR)));
			break;
		case DESC_M_DOM:
			result.append(getMonthName(calendar.get(Calendar.MONTH)));
			result.append(" ");
			result.append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
			break;
		case DESC_DOW_M:
			result.append(getDayOfWeekName(calendar.get(Calendar.DAY_OF_WEEK)-1));
			result.append(", ");
			result.append(getMonthName(calendar.get(Calendar.MONTH)));
			break;
		case DESC_TIME_DOW_M_DOM_Y:
			result.append(new SimpleDateFormat("HH:mm").format(calendar.getTime()));
			result.append(" ");
			result.append(getDayOfWeekName(calendar.get(Calendar.DAY_OF_WEEK)-1));
			result.append(", ");
			result.append(getMonthName(calendar.get(Calendar.MONTH)));
			result.append(" ");
			result.append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
			result.append(", ");
			result.append(String.valueOf(calendar.get(Calendar.YEAR)));
			break;
		case DESC_TIME_M_DOM_Y:
			result.append(new SimpleDateFormat("HH:mm").format(calendar.getTime()));
			result.append(" ");
			result.append(getMonthName(calendar.get(Calendar.MONTH)));
			result.append(" ");
			result.append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
			result.append(", ");
			result.append(String.valueOf(calendar.get(Calendar.YEAR)));
			break;
		}
		return result.toString();
	}
	

}
