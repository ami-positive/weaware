package com.positiveapps.weaware.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

import com.positiveapps.weaware.main.MyApp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;









public class BitmapUtil {
	

	public static final int IMAGE_TYPE_FROM_CAMERA = 1;
	public static final int IMAGE_TYPE_FROM_GALLERAY = 2;
	
	/** CONSTANTS **/
	
	// name of the folder that file saved in
	private static  final String MEDIA_FLODER_NAME = "/Pikpack/Image";
	
	// name of the file
	private final String IMAGE_NAME = "image.jpeg";
	public static final String IMAGE_EXTENATION = "jpeg";
	
	
	/** CLASS METHODS **/


    public static  Bitmap loadImageIntoByStringPath(String path,ImageView into,int scale,int simpleSize,int deafult) {

    // option that resize the bitmap
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inSampleSize = simpleSize;

    // create bitmap by image uri with the option
    Log.e("imagePathLog", "path = " + path);
    Bitmap image = BitmapFactory.decodeFile(path, options);
    int angle = getCameraPhotoOrientation(MyApp.appContext, null, path);
    if(image != null){
        image = RotateBitmap(image, angle);
    }
    if (into != null)
    setImageInto(image, into, scale, deafult);
    return image;
}

    public static void setImageInto (Bitmap image,ImageView into,int scale,int deafult) {
        if (image != null){
            if (scale > 0){
                image = crupAndScale(image, scale);
            }

            into.setImageBitmap(image);
        }else{
            into.setImageResource(deafult);
        }
    }

    public static  Bitmap crupAndScale (Bitmap source,int scale){

        int factor = source.getHeight() <= source.getWidth() ? source.getHeight(): source.getWidth();
        int longer = source.getHeight() >= source.getWidth() ? source.getHeight(): source.getWidth();
        int x = source.getHeight() >= source.getWidth() ?0:(longer-factor)/2;
        int y = source.getHeight() <= source.getWidth() ?0:(longer-factor)/2;
        source = Bitmap.createBitmap(source, x, y, factor, factor);
        source = Bitmap.createScaledBitmap(source, scale, scale, false);
        return source;

    }

    public static Bitmap RotateBitmap(Bitmap source, int angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath){
        int rotate = 0;
        try {
            if(imageUri != null){
                context.getContentResolver().notifyChange(imageUri, null);
            }
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }


            Log.v("camera", "Exif orientation: " + orientation);
        } catch (Exception e) {
            e.printStackTrace();
        }



        return rotate;
    }
	
	@SuppressWarnings("finally")
	public static String getImageAsStringEncodedBase64(Bitmap bitmap) {
		if (bitmap == null){
			return "";
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
		byte[] byteArray = stream.toByteArray();
		try {
			stream.close();
		} catch (IOException e) {}finally{
			return encodeImageTobase64(byteArray);
		}
	}

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }
	
	@SuppressWarnings("finally")
	public static String getMediaAsStringEncodedBase64(byte[] data) {
		if (data == null){
			return "";
		}
		return encodeImageTobase64(data);
	}
	
	public  Bitmap decodeBase64(String input) 
	{
	    byte[] decodedByte = Base64.decode(input, 0);
	    if (decodedByte.length == 0){
	    	return null;
	    }
	    return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}
	
	private static String encodeImageTobase64(byte[] imageBytes) {
		String test = "";
		try {  
		String encodedString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		test= "";
		Log.e("base64", encodedString);
		
			test = new String(encodedString.getBytes(),"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			return "";
		}catch (Error e) {
				
				Log.e("error" , "error = " + e.getMessage());
				return "";
		}
		return test;
	}
	
	
	/*
	 * automatically generates path of the image file
	 * */
	public static Uri createUriPathForStoringImage(String imageName) {

		String root = Environment.getExternalStorageDirectory()
				.getAbsolutePath();

		// add with image folder name
		File f = new File(root + MEDIA_FLODER_NAME);
		f.mkdirs();
		String image_fath = DateUtil.getCurrentDateAsString(DateUtil.FORMAT_DEAFULT) + imageName + "." + IMAGE_EXTENATION;

		// create the file after complete creating of the absolute file path
		File Image_file = new File(f, image_fath);
		
		// convert file to uri format
		Uri imageUri = Uri.fromFile(Image_file);
		return imageUri;

	}
	
	public static String cretatStringPathForStoringMedia (String fileName){
		
		String root = Environment.getExternalStorageDirectory()
				.getAbsolutePath();
        File dir = new File (root + MEDIA_FLODER_NAME);
        if(dir.exists()==false) {
             dir.mkdirs();
        }
        File result = new File(dir, fileName);
        
		return result.getAbsolutePath();
	}

	public static String saveImageToSDCard (String fileName,Bitmap toSave){
		File file = new File(cretatStringPathForStoringMedia(fileName));
	    saveImageToSDCard(file, toSave);
	    return file.getAbsolutePath();
	}
	
	
	public static String saveImageToSDCard (File file,Bitmap toSave){
		FileOutputStream outStream  = null;
	    try {
	        outStream = new FileOutputStream(file);
	        toSave.compress(Bitmap.CompressFormat.JPEG, 100, outStream); 
	        outStream.flush();
	        outStream.close();
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return file.getAbsolutePath();
	}
	

	
	
	public static void getVideoFrame(String path,ImageView imageView) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(new File(Uri.parse(path).getPath()).getAbsolutePath());
            Bitmap bitmap = retriever.getFrameAtTime();
            if (bitmap != null){
            	imageView.setImageBitmap(bitmap);
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
            }
        }
    }
	

	
	
	public static byte[] getMediaBytesDataByUri(Uri uri)  {
	       
		InputStream inputStream = null;
		ByteArrayOutputStream byteBuffer = null;
		try {
			inputStream = MyApp.appContext.getContentResolver().openInputStream(uri);

			byteBuffer = new ByteArrayOutputStream();
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int len = 0;
			while ((len = inputStream.read(buffer)) != -1) {
				byteBuffer.write(buffer, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}catch (OutOfMemoryError error){
			return null;
		}
		return byteBuffer.toByteArray();
     }
	
	
	public interface LoadImageCallback {
		public void onLoadImage(boolean success, Bitmap image);
	}
	
	public static Bitmap takeScreenShot(Activity activity)  {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        int width = activity.getWindowManager().getDefaultDisplay().getWidth();
        int height = activity.getWindowManager().getDefaultDisplay().getHeight();

        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height  - statusBarHeight);
        view.destroyDrawingCache();
        return b;
    }
	
	public static Bitmap takeScreenShot(View view)  {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        view.getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        int width = view.getWidth();
        int height = view.getHeight();

        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height  - statusBarHeight);
        view.destroyDrawingCache();
        return b;
    }
	
	public static Bitmap fastblur(Bitmap sentBitmap, int radius) {
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
            sir[1] = (p & 0x00ff00) >> 8;
        sir[2] = (p & 0x0000ff);
        rbs = r1 - Math.abs(i);
        rsum += sir[0] * rbs;
        gsum += sir[1] * rbs;
        bsum += sir[2] * rbs;
        if (i > 0) {
            rinsum += sir[0];
            ginsum += sir[1];
            binsum += sir[2];
        } else {
            routsum += sir[0];
            goutsum += sir[1];
            boutsum += sir[2];
        }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
            sir[1] = (p & 0x00ff00) >> 8;
            sir[2] = (p & 0x0000ff);

            rinsum += sir[0];
            ginsum += sir[1];
            binsum += sir[2];

            rsum += rinsum;
            gsum += ginsum;
            bsum += binsum;

            stackpointer = (stackpointer + 1) % div;
            sir = stack[(stackpointer) % div];

            routsum += sir[0];
            goutsum += sir[1];
            boutsum += sir[2];

            rinsum -= sir[0];
            ginsum -= sir[1];
            binsum -= sir[2];

            yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

	
	@SuppressLint("NewApi")
	public static void animateChangeImageInsideImageView (ImageView  toAnimate,
			int newImage,int duration){
		Drawable[] layers = new Drawable[2];
		layers[0] = toAnimate.getDrawable();
		layers[1] = MyApp.appContext.getResources().getDrawable(newImage);
		TransitionDrawable transitionDrawable = new TransitionDrawable(layers);
		toAnimate.setImageDrawable(transitionDrawable);
		transitionDrawable.setCrossFadeEnabled(true);
		transitionDrawable.setAlpha(0);
		transitionDrawable.startTransition(duration);
	}
	
	public static  Bitmap createBitmapFromView (ViewGroup container,View v){
    	container.addView(v);
	    if (v.getMeasuredHeight() <= 0) {
	        v.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
	        Canvas c = new Canvas(b);
	        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
	        v.draw(c);
	        return b;
	    }
	    Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);                
	    Canvas c = new Canvas(b);
	    v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
	    v.draw(c);
	    return b;
	    
	}
}
