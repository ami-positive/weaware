package com.positiveapps.weaware.objects;

import android.database.Cursor;
import android.os.CountDownTimer;
import android.util.Log;

import com.positiveapps.weaware.database.tables.TablePersons;
import com.positiveapps.weaware.database.tables.TableReports;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.main.onErrorCallBack;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.getByAppuserRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getByAppuserResponse;
import com.positiveapps.weaware.util.DateUtil;
import com.positiveapps.weaware.util.ToastUtil;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Izakos on 03/10/2015.
 */
public class Report implements Serializable {

    private static final long serialVersionUID = 0x98F22BF5;

    private long databaseId;
    private long serverId;

    private int category; // 0- citizen | 1 - policeman
    private int confirm;  // 0 - false | 1 - true
    private int opened;  // 0 - false | 1 - true
    private int reporterRank;
    private int reporterNumStars;

    private Double lat;
    private Double lon;

    private String reportDate = "";
    private String photoAttached = "";
    private String reporterImage = "";
    private String reporterName = "";
    private String audioAttached = "";
    private String description = "";
    private String usersConfirm = "";  // users name and time;

    public static CountDownTimer countDownTimer;

    private TimerListener timerListener;

    public int getReporterRank() {
        return reporterRank;
    }

    public void setReporterRank(int reporterRank) {
        this.reporterRank = reporterRank;
    }

    public int getReporterNumStars() {
        return reporterNumStars;
    }

    public void setReporterNumStars(int reporterNumStars) {
        this.reporterNumStars = reporterNumStars;
    }

    public Double getLon() {
        return lon;
    }

    public String getLonString() {
        return lon+"";
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public void setLon(String lon) {
        this.lon = Double.parseDouble(lon);
    }

    public Double getLat() {
        return lat;
    }

    public String getLatString() {
        return lat+"";
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLat(String lat) {
        this.lat = Double.parseDouble(lat);
    }

    public String getReporterImage() {
        return reporterImage;
    }

    public void setReporterImage(String reporterImage) {
        this.reporterImage = reporterImage;
    }

    public long getDatabaseId() {
        return databaseId;
    }

    public String getReporterName() {
        return reporterName;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public void setDatabaseId(long databaseId) {
        this.databaseId = databaseId;
    }

    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getConfirm() {
        return confirm;
    }

    public void setConfirm(int confirm) {
        this.confirm = confirm;
    }

    public int getOpened() {
        return opened;
    }

    public void setOpened(int opened) {
        this.opened = opened;
    }

    public String getReportDate() {
        return reportDate;
    }

    public String getDateEndCustom() {
        String dateString = reportDate;
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm");
        String reformattedStr = "";
        try {

            reformattedStr = newFormat.format(oldFormat.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return reformattedStr;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getPhotoAttached() {
        if(photoAttached == null){
            return "";
        }
        return photoAttached;
    }

    public void setPhotoAttached(String photoAttached) {
        this.photoAttached = photoAttached;
    }

    public String getAudioAttached() {
        if(audioAttached == null){
            return "";
        }
        return audioAttached;
    }

    public void setAudioAttached(String audioAttached) {
        this.audioAttached = audioAttached;
    }

    public String getDescription() {
        if(description == null){
            return "";
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsersConfirm() {
        return usersConfirm;
    }

    public void setUsersConfirm(String usersConfirm) {
        this.usersConfirm = usersConfirm;
    }

    public void setUsersConfirm(String name, String url) {
        this.usersConfirm = usersConfirm;
    }

    public static void populateTablee() {
        MyApp.tableReports.deleteAll();
            Report report = new Report();
            report.setDescription("event 1 bla bla bla bla bla bla");
            report.setReportDate("16:00");
        report.setServerId(1);
        report.setLat(32.166700);
        report.setLon(34.834728);
            report.setReporterName("Julie");
            report.setReporterImage("http://www.substance.com/wp-content/uploads/2014/03/Samantha-Felix_avatar_1396283011-128x128.jpg");
        report.setPhotoAttached("https://upload.wikimedia.org/wikipedia/commons/c/cb/BackyardParty.jpg");
//            report.setUsersConfirm("julie", "http://www.substance.com/wp-content/uploads/2014/03/Samantha-Felix_avatar_1396283011-128x128.jpg");
            report.setConfirm(0);
            MyApp.tableReports.insert(report);

        report = new Report();
        report.setDescription("event 1 bla bla bla bla bla bla");
        report.setReportDate("16:00");
        report.setReporterName("Hana");
        report.setServerId(2);
        report.setLat(32.087180);
        report.setLon(34.875112);
        report.setReporterImage("http://www.samanthasnipes.com/wp-content/uploads/2013/10/cropped-Sam-Snipes-e1381339279711.jpg");
        report.setPhotoAttached("https://upload.wikimedia.org/wikipedia/commons/c/cb/BackyardParty.jpg");
//            report.setUsersConfirm("julie", "http://www.substance.com/wp-content/uploads/2014/03/Samantha-Felix_avatar_1396283011-128x128.jpg");
        report.setConfirm(1);
        MyApp.tableReports.insert(report);

        report = new Report();
        report.setDescription("event 1 bla bla bla bla bla bla");
        report.setReportDate("16:00");
        report.setReporterName("Yafa");
        report.setServerId(3);
        report.setLat(32.155075);
        report.setLon(34.885840);
        report.setReporterImage("http://image.cdn.ispot.tv/topic/kms/samantha-lynch.png");
        report.setPhotoAttached("https://upload.wikimedia.org/wikipedia/commons/c/cb/BackyardParty.jpg");
//            report.setUsersConfirm("julie", "http://www.substance.com/wp-content/uploads/2014/03/Samantha-Felix_avatar_1396283011-128x128.jpg");
        report.setConfirm(1);
        MyApp.tableReports.insert(report);

        report = new Report();
        report.setDescription("event 1 bla bla bla bla bla bla");
        report.setReportDate("16:00");
        report.setReporterName("Noa");
        report.setServerId(4);
        report.setLat(32.183652);
        report.setLon(34.871893);
        report.setReporterImage("http://38.media.tumblr.com/avatar_f5d749efa5db_128.png");
        report.setPhotoAttached("https://upload.wikimedia.org/wikipedia/commons/c/cb/BackyardParty.jpg");
//            report.setUsersConfirm("julie", "http://www.substance.com/wp-content/uploads/2014/03/Samantha-Felix_avatar_1396283011-128x128.jpg");
        report.setConfirm(0);
        MyApp.tableReports.insert(report);

    }

    public static long getReportID(){
        return MyApp.userProfile.getReportID();
    }

    public static void setReportID(long ReportID){
        MyApp.userProfile.setReportID(ReportID);
    }

    public static int getmode(){
        return MyApp.userProfile.getMode();
    }

    public static void setMode(int mode){
        MyApp.userProfile.setMode(mode);
    }


    public static Report getReportById(long reportID) {
        Cursor cursor = MyApp.tableReports.readCursor(TablePersons.COLUMN_ID);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(TableReports.SERVER_ID));
                if(id == reportID){
                    return MyApp.tableReports.cursorToEntity(cursor);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return null;
    }

    public static Report getReportByDbId(long reportID) {
        Cursor cursor = MyApp.tableReports.readCursor(TablePersons.COLUMN_ID);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(TableReports.COLUMN_ID));
                if(id == reportID){
                    return MyApp.tableReports.cursorToEntity(cursor);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return null;
    }

    public static Report getCurrentReport() {
        Cursor cursor = MyApp.tableReports.readCursor(TablePersons.COLUMN_ID);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(TableReports.SERVER_ID));
                if(id == Report.getReportID()){
                    return MyApp.tableReports.cursorToEntity(cursor);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return null;
    }

    public static void setReportTimer(final TimerListener timerListener){
/*        Log.d("TAG", "setReportTimer");
        long halfHourInMilli = 1800000L;
        long difference = System.currentTimeMillis() - User.getStartAlarmInMilli();// difference Between the Event And Now
        Log.d("TAG", "setReportTimer:System.currentTimeMillis() " + DateUtil.stringFromMilli(System.currentTimeMillis()));
        Log.d("TAG", "setReportTimer:User.getStartAlarmInMilli() " + DateUtil.stringFromMilli(User.getStartAlarmInMilli()));
        Log.d("TAG", "setReportTimer:difference " + difference);
        if(difference < 5000){
            return;
        }*/

        long difference = System.currentTimeMillis() - User.getStartAlarmInMilli();// difference Between the Event And Now
        Log.d("TAG", "setReportTimer:System.currentTimeMillis() " + DateUtil.stringFromMilli(System.currentTimeMillis()));
        Log.d("TAG", "setReportTimer:User.getStartAlarmInMilli() " + DateUtil.stringFromMilli(User.getStartAlarmInMilli()));
        Log.d("TAG", "setReportTimer:difference " + difference);

        long halfHourInMilli = 1800000L;
        long timeLeft = halfHourInMilli - difference;
        countDownTimer =  new CountDownTimer(timeLeft, 1000*60) {

            public void onTick(long millisUntilFinished) {
//                reportDescription.setText("seconds remaining: " + millisUntilFinished / 1000);
                Log.d("TAG", "onTick " +  (millisUntilFinished / 1000));
            }

            public void onFinish() {
                timerListener.onReportTimeOut();
            }
        };
    }

    public static void startReportTimer(){
        if(countDownTimer != null) {
            countDownTimer.start();
        }
    }

    public static void stopReportTimer(){
        if(countDownTimer != null){
            countDownTimer.cancel();
        }
    }



    public static void getReports(final onReportsUpdated reportsUpdated, final onErrorCallBack callBack){
        MyApp.networkManager.makeRequest(new getByAppuserRequest(), new NetworkCallback<getByAppuserResponse>() {
                    @Override
                    public void onResponse(boolean success, String errorDesc, ResponseObject<getByAppuserResponse> response) {
                        super.onResponse(success, errorDesc, response);
                        if (success) {
                            MyApp.tableReports.deleteAll();
                            for (Reports x : response.getData().getReports()) {
                                Report report = new Report();
                                report.setDescription(x.getDescription());
                                report.setReportDate(x.getCreated());
                                report.setServerId(Long.parseLong(x.getId()));
                                report.setLat(x.getLat());
                                report.setLon(x.getLng());
                                report.setReporterName(x.getAppuserName());
                                report.setReporterImage(x.getAppuserAvatar());
                                report.setReporterNumStars(x.getAppuserCredibility());
                                report.setReporterRank(x.getAppuserRank());
                                report.setCategory(Integer.parseInt(x.getIsPoliceman()));
                                report.setPhotoAttached(x.getImage());
                                report.setAudioAttached(x.getAudio());
                                report.setConfirm(x.getIsConfirmed());
                                MyApp.tableReports.insert(report);
                            }
                            if(reportsUpdated != null){
                                reportsUpdated.reportsUpdated();
                            }
                        } else {
                            if (callBack != null) {
                                callBack.onError(errorDesc);
                            }else{
                                ToastUtil.toster(errorDesc, true);
                            }
                        }
                    }
                });

    }

    public interface onReportsUpdated{
        public void reportsUpdated();
    }

    public static int getReportCounts(){
        int counter = 0;
        Cursor personCursor = MyApp.tableReports.readCursor(TablePersons.COLUMN_ID);
        if (personCursor.moveToFirst()) {
            do {
                counter++;
            } while (personCursor.moveToNext());
        }
        personCursor.close();
        return counter;
    }

    @Override
    public String toString() {
        return "Report{" +
                "databaseId=" + databaseId +
                ", serverId=" + serverId +
                ", category=" + category +
                ", confirm=" + confirm +
                ", opened=" + opened +
                ", lat=" + lat +
                ", lon=" + lon +
                ", reportDate='" + reportDate + '\'' +
                ", photoAttached='" + photoAttached + '\'' +
                ", reporterImage='" + reporterImage + '\'' +
                ", reporterName='" + reporterName + '\'' +
                ", audioAttached='" + audioAttached + '\'' +
                ", description='" + description + '\'' +
                ", usersConfirm='" + usersConfirm + '\'' +
                ", timerListener=" + timerListener +
                '}';
    }
}

