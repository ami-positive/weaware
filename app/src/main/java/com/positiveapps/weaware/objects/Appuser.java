package com.positiveapps.weaware.objects;
import com.google.gson.annotations.SerializedName;


/***************************************---- Appuser Class ------********************/

public class Appuser{

    @SerializedName("FullName")
    private String fullName;
    @SerializedName("Password")
    private String password;
    private String Username;
    private String UDID;
    private int Rank;
    private String Device;
    private String Token;
    private String Email;
    private String Avatar;
    private String Phone;
    private String FBAccessToken;
    private String FBID;
    private String TID;
    private String City;
    private int Credibility;
    private int IsPoliceman;
    private String Lat;
    private String Lng;
    private String Status;
    private String ID;
    private String Created;
    private String Modified;


    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUDID() {
        return UDID;
    }

    public void setUDID(String UDID) {
        this.UDID = UDID;
    }

    public int getRank() {
        return Rank;
    }

    public void setRank(int Rank) {
        this.Rank = Rank;
    }

    public String getDevice() {
        return Device;
    }

    public void setDevice(String Device) {
        this.Device = Device;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String Avatar) {
        this.Avatar = Avatar;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getFBAccessToken() {
        return FBAccessToken;
    }

    public void setFBAccessToken(String FBAccessToken) {
        this.FBAccessToken = FBAccessToken;
    }

    public String getFBID() {
        return FBID;
    }

    public void setFBID(String FBID) {
        this.FBID = FBID;
    }

    public String getTID() {
        return TID;
    }

    public void setTID(String TID) {
        this.TID = TID;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public int getCredibility() {
        return Credibility;
    }

    public void setCredibility(int Credibility) {
        this.Credibility = Credibility;
    }

    public int getIsPoliceman() {
        return IsPoliceman;
    }

    public void setIsPoliceman(int IsPoliceman) {
        this.IsPoliceman = IsPoliceman;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String Lat) {
        this.Lat = Lat;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String Lng) {
        this.Lng = Lng;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String Created) {
        this.Created = Created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String Modified) {
        this.Modified = Modified;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "Appuser{" +
                "fullName='" + fullName + '\'' +
                ", password='" + password + '\'' +
                ", UDID='" + UDID + '\'' +
                ", Rank='" + Rank + '\'' +
                ", Device='" + Device + '\'' +
                ", Token='" + Token + '\'' +
                ", Email='" + Email + '\'' +
                ", Avatar='" + Avatar + '\'' +
                ", Phone='" + Phone + '\'' +
                ", FBAccessToken='" + FBAccessToken + '\'' +
                ", FBID='" + FBID + '\'' +
                ", TID='" + TID + '\'' +
                ", City='" + City + '\'' +
                ", Credibility=" + Credibility +
                ", IsPoliceman=" + IsPoliceman +
                ", Lat='" + Lat + '\'' +
                ", Lng='" + Lng + '\'' +
                ", ID='" + ID + '\'' +
                ", Created='" + Created + '\'' +
                ", Modified='" + Modified + '\'' +
                '}';
    }
}
