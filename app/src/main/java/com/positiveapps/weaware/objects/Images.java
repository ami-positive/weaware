package com.positiveapps.weaware.objects;
import com.google.gson.annotations.SerializedName;


/***************************************---- Images Class ------********************/

public class Images{

    @SerializedName("Url")
    private String url;
    @SerializedName("ID")
    private String id;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
