package com.positiveapps.weaware.objects;
import com.google.gson.annotations.SerializedName;


public class Friend{

    @SerializedName("ID")
    private long id;
    @SerializedName("Phone")
    private String phone;
    @SerializedName("FullName")
    private String fullName;
    @SerializedName("Username")
    private String userName;
    @SerializedName("IsPoliceman")
    private int isPoliceman;
    @SerializedName("Lat")
    private String lat;
    @SerializedName("Lng")
    private String lng;
    @SerializedName("City")
    private String city;
    @SerializedName("Avatar")
    private String avatar;
    @SerializedName("IsFriend")
    private String isFriend;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getIsPoliceman() {
        return isPoliceman;
    }

    public void setIsPoliceman(int isPoliceman) {
        this.isPoliceman = isPoliceman;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(String isFriend) {
        this.isFriend = isFriend;
    }


}
