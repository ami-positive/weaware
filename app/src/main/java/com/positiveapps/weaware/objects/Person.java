package com.positiveapps.weaware.objects;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.maps.android.clustering.ClusterItem;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.database.tables.TablePersons;
import com.positiveapps.weaware.database.tables.TableReports;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.main.onErrorCallBack;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.ApproveFriendRequest;
import com.positiveapps.weaware.network.requests.getFriendsRequest;
import com.positiveapps.weaware.network.response.ApproveFriendResponse;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getFriendsResponse;
import com.positiveapps.weaware.util.ToastUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Izakos on 04/10/2015.
 */
public class Person implements Storable<Person>, ClusterItem {

    public static final int CITIZEN = 0;
    public static final int POLICEMAN = 1;

    private long databaseId;
    private long serverId;

    private int category; // 1- citizen | 2 - policeman

    private double lat;
    private double lng;

    private String userName = "";
    private String city = "";
    private String ProfileImage = "";
    private String Status = "";
    private byte[] ProfileImageBlob;


    private static Set<TaregtX> markersTargets = new HashSet<TaregtX>();

    public byte[] getProfileImageBlob() {
            if(ProfileImageBlob != null){
                return  ProfileImageBlob;
            }
        return new byte[0];
    }

    public void setProfileImageBlob(byte[] profileImageBlob) {
        ProfileImageBlob = profileImageBlob;
    }

    public void setProfileImageBlob() {
        TaregtX taregtX = new TaregtX(this, markersTargets);
        markersTargets.add(taregtX);
        if(!this.getProfileImage().isEmpty()) {
            Picasso.with(MyApp.appContext).load(this.getProfileImage()).resize(50, 50).into(taregtX);
        }
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public long getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(long databaseId) {
        this.databaseId = databaseId;
    }

    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Drawable getProfileImageDrawable(){
        return new BitmapDrawable(BitmapFactory.decodeByteArray(getProfileImageBlob(), 0, getProfileImageBlob().length));
    }


    @Override
    public Person fromJson(String objectAsString) {
        return new Gson().fromJson(objectAsString, this.getClass());
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this).toString();
    }

    public static void populateTablee() {
        MyApp.tablePersons.deleteAll();

        Person person = new Person();
        person.setUserName("Alexsa Mizrachi");
        person.setProfileImage("https://apply.jhu.edu/_/img/alexaProfile.jpg");
        person.setLat(32.034977);
        person.setLng(34.881957);
        person.setCategory(1);
        MyApp.tablePersons.insert(person);

        person = new Person();
        person.setUserName("Loni Ershkovitz");
        person.setProfileImage("https://media.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAFOAAAAJGZlN2JhZjliLTkzOGUtNDhkOS05OGVmLTY1ZGYzZjEwZDA2Yg.jpg");
        person.setLat(32.089168);
        person.setLng(34.781599);
        person.setCategory(1);
        MyApp.tablePersons.insert(person);

        person = new Person();
        person.setUserName("Avner Malik");
        person.setProfileImage("https://media.licdn.com/mpr/mpr/shrink_100_100/p/3/000/0ab/152/3403288.jpg");
        person.setLat(32.0724174);
        person.setLng(34.802585);
        person.setCategory(2);
        MyApp.tablePersons.insert(person);

        person = new Person();
        person.setUserName("Jeniffer Kolt");
        person.setProfileImage("https://yt3.ggpht.com/-_Amo0FxzoDY/AAAAAAAAAAI/AAAAAAAAAAA/qCpGoHwE3Ao/s100-c-k-no/photo.jpg");
        person.setLat(32.094622);
        person.setLng(34.799001);
        person.setCategory(1);
        MyApp.tablePersons.insert(person);

        person = new Person();
        person.setUserName("Julie Gotter");
        person.setProfileImage("http://twibbon.com/Content/Images/public/examples/girl.png");
        person.setLat(32.056439);
        person.setLng(34.779732);
        person.setCategory(2);
        MyApp.tablePersons.insert(person);

        person = new Person();
        person.setUserName("Ann Levi");
        person.setLat(32.044466);
        person.setLng(34.774486);
        person.setCategory(2);
        person.setProfileImage("http://www.lawyersweekly.com.au/images/LW_Media_Library/594partner-profile-pic-An.jpg");
        MyApp.tablePersons.insert(person);



    }


    @Override
    public LatLng getPosition() {
        return new LatLng(lat, lng);
    }






    @Override
    public String toString() {
        return "Person{" +
                "databaseId=" + databaseId +
                ", serverId=" + serverId +
                ", category=" + category +
                ", lat=" + lat +
                ", lng=" + lng +
                ", userName='" + userName + '\'' +
                ", city='" + city + '\'' +
                ", ProfileImage='" + ProfileImage + '\'' +
                '}';
    }


    public static void saveImagesAsBlob(){
        Cursor personCursor = MyApp.tablePersons.readCursor(TablePersons.COLUMN_ID);
        if (personCursor.moveToFirst()) {
            do {
                Person person = MyApp.tablePersons.cursorToEntity(personCursor);
                person.setProfileImageBlob();
            } while (personCursor.moveToNext());
        }
        personCursor.close();
    }

    public static void getFriends(final onErrorCallBack callBack){
        MyApp.networkManager.makeRequest(new getFriendsRequest(), new NetworkCallback<getFriendsResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<getFriendsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    updateFriends(response.getData().getFriends());
                } else {
                    if (callBack != null) {
                        callBack.onError(errorDesc);
                    } else {
                        ToastUtil.toster(errorDesc, true);
                    }
                }
            }
        });
    }

    public static void updateFriendStatus(long FriendID){
        Person person = getPersonById(FriendID);
        person.setStatus("friend");
        MyApp.tablePersons.update(person.getDatabaseId(), person);
    }

    public static void approveFriends(final TextView textView, final long FriendId){
        MyApp.networkManager.makeRequest(new ApproveFriendRequest(String.valueOf(FriendId)), new NetworkCallback<ApproveFriendResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<ApproveFriendResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    updateFriendStatus(FriendId);
                    textView.setBackgroundResource(R.drawable.confirm_green_list);
                    textView.animate().alpha(0);
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public static Person getPersonById(long FriendID) {
        Cursor cursor = MyApp.tablePersons.readCursor(TablePersons.COLUMN_ID);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(TableReports.SERVER_ID));
                if(id == FriendID){
                    return MyApp.tablePersons.cursorToEntity(cursor);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return null;
    }

    public static void updateFriends(ArrayList<Appuser> appusers){
        MyApp.tablePersons.deleteAll();
        for (int i = 0; i < appusers.size() ; i++) {
            Appuser appuser = appusers.get(i);
            Person person = new Person();
            person.setServerId(Long.parseLong(appuser.getID()));
            person.setUserName(appuser.getFullName());
            person.setProfileImage(appuser.getAvatar());
            person.setLat(Double.parseDouble(appuser.getLat()));
            person.setLng(Double.parseDouble(appuser.getLng()));
            person.setStatus(appuser.getStatus());
            person.setCategory(appuser.getIsPoliceman());
            MyApp.tablePersons.insertOrUpdate(person);
        }
    }


}


