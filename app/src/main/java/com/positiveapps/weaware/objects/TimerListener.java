package com.positiveapps.weaware.objects;

/**
 * Created by Izakos on 19/10/2015.
 */
public interface TimerListener {
    public void onReportTimeOut();
}
