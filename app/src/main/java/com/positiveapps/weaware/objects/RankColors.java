package com.positiveapps.weaware.objects;
import com.google.gson.annotations.SerializedName;


/***************************************---- RankColors Class ------********************/

public class RankColors{

    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
