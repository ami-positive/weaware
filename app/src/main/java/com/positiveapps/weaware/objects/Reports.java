package com.positiveapps.weaware.objects;
import com.google.gson.annotations.SerializedName;


/***************************************---- Reports Class ------********************/

public class Reports{

    @SerializedName("ID")
    private String id;
    @SerializedName("AppuserID")
    private String appuserID;
    @SerializedName("Description")
    private String description;
    @SerializedName("IsConfirmed")
    private int isConfirmed;
    @SerializedName("Status")
    private String status;
    @SerializedName("Lat")
    private String lat;
    @SerializedName("Lng")
    private String lng;
    @SerializedName("Audio")
    private String audio;
    @SerializedName("Image")
    private String image;
    private String Created;
    private String AppuserName;
    private String AppuserAvatar;
    private String Images;
    private String IsPoliceman;
    private int AppuserRank;
    private int AppuserCredibility;

    public int getAppuserRank() {
        return AppuserRank;
    }

    public void setAppuserRank(int appuserRank) {
        AppuserRank = appuserRank;
    }

    public int getAppuserCredibility() {
        return AppuserCredibility;
    }

    public void setAppuserCredibility(int appuserCredibility) {
        AppuserCredibility = appuserCredibility;
    }

    public String getAppuserAvatar() {
        return AppuserAvatar;
    }

    public void setAppuserAvatar(String appuserAvatar) {
        AppuserAvatar = appuserAvatar;
    }

    public String getIsPoliceman() {
        return IsPoliceman;
    }

    public void setIsPoliceman(String isPoliceman) {
        IsPoliceman = isPoliceman;
    }

    public String getAppuserName() {
        return AppuserName;
    }

    public void setAppuserName(String appuserName) {
        AppuserName = appuserName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppuserID() {
        return appuserID;
    }

    public void setAppuserID(String appuserID) {
        this.appuserID = appuserID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(int isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String Created) {
        this.Created = Created;
    }

    public String getImages() {
        return Images;
    }

    public void setImages(String Images) {
        this.Images = Images;
    }

    @Override
    public String toString() {
        return "Reports{" +
                "id='" + id + '\'' +
                ", appuserID='" + appuserID + '\'' +
                ", description='" + description + '\'' +
                ", isConfirmed='" + isConfirmed + '\'' +
                ", status='" + status + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", audio='" + audio + '\'' +
                ", image='" + image + '\'' +
                ", Created='" + Created + '\'' +
                ", AppuserName='" + AppuserName + '\'' +
                ", AppuserAvatar='" + AppuserAvatar + '\'' +
                ", Images='" + Images + '\'' +
                ", IsPoliceman='" + IsPoliceman + '\'' +
                '}';
    }
}
