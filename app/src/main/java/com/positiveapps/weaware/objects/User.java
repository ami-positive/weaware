package com.positiveapps.weaware.objects;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.dialogs.DialogManager;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.main.onErrorCallBack;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.UpdateProfileRequest;
import com.positiveapps.weaware.network.requests.updateLocationRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.UpdateProfileResponse;
import com.positiveapps.weaware.network.response.updateLocationResponse;
import com.positiveapps.weaware.network.twitter.MyTwitterApiClient;
import com.positiveapps.weaware.phone.PhoneNumberParser;
import com.positiveapps.weaware.ui.CircleImageView;
import com.positiveapps.weaware.util.DateUtil;
import com.positiveapps.weaware.util.DeviceUtil;
import com.positiveapps.weaware.util.Helper;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.ToastUtil;
import com.positiveapps.weaware.util.Validation;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.util.List;

/**
 * Created by Izakos on 08/10/2015.
 */
public abstract class User {

    public static int AVATAR = R.drawable.avatar;
    public static final int LOGIN_TYPE_NONE = 0;
    public static final int LOGIN_TYPE_TWITTER = 10;
    public static final int LOGIN_TYPE_FACEBOOK = 20;
    public static final int LOGIN_TYPE_SERVER = 30;
    public static final int GOOGLE_PLUS = 40;


    public static final String TAG = "User";
    public static boolean showLog;
    public static int currentLoginType;
    public static String accessToken;
    public static MaterialDialog dialog;
    private boolean showWarnDialogFlag = true;


    public static long getServerId() {
        return MyApp.userProfile.getServerId();
    }

    public static void setServerId(long serverId) {
        MyApp.userProfile.setServerId(serverId);
    }

    public static long getFacebookId() {
        return MyApp.userProfile.getFacebookId();
    }

    public static void setFacebookId(long facebookId) {
        MyApp.userProfile.setFacebookId(facebookId);
    }

    public static long getTweeterId() {
        return MyApp.userProfile.getTwitterId();
    }

    public static void setTweeterId(long tweeterId) {
        MyApp.userProfile.setTwitterId(tweeterId);
    }

    public static int getCategory() {
        return MyApp.userProfile.getCategory();
    }

    public static void setCategory(int category) {
        MyApp.userProfile.setCategory(category);
    }

    public static void setCategory(Switch category) {
        MyApp.userProfile.setCategory(category.isChecked() ? 1 : 0);
    }

    public static boolean isPoliceman() {
        return MyApp.userProfile.getCategory() == 1 ? true : false;
    }

    public static int isPolicemanInt() {
        return MyApp.userProfile.getCategory();
    }

    public static void setIsPoliceman(boolean isPoliceman) {
        MyApp.userProfile.setCategory(isPoliceman ? 1 : 0);
    }

    public static void setIsPoliceman(int isPoliceman) {
        MyApp.userProfile.setCategory(isPoliceman);
    }

    public static int getLoginType() {
        return currentLoginType;
    }

    public static void setLoginType(int currentLoginType) {
        User.currentLoginType = currentLoginType;
    }

    public static int getCurrentGrade() {
        return MyApp.userProfile.getCurrentGrade();
    }

    public static void setCurrentGrade(int currentGrade) {
        MyApp.userProfile.setCurrentGrade(currentGrade);
    }

    public static int getNumberOfStarts() {
        return MyApp.userProfile.getNumberOfStars();
    }

    public static void setNumberOfStarts(int numberOfStarts) {
        MyApp.userProfile.setNumberOfStars(numberOfStarts);
    }

    public static String getLatString() {
        return MyApp.generalSettings.getLat();
    }

    public static double getLat() {
        return Double.parseDouble(MyApp.generalSettings.getLat());
    }

    public static void setLat(double lat) {
        MyApp.generalSettings.setLat(lat + "");
    }

    public static void setLat(String lat) {
        MyApp.generalSettings.setLat(lat);
    }

    public static String getLonString() {
        return MyApp.generalSettings.getLon();
    }

    public static double getLon() {
        return Double.parseDouble(MyApp.generalSettings.getLon());
    }

    public static void setLon(double lon) {
        MyApp.generalSettings.setLon(lon + "");
    }

    public static void setLon(String lon) {
        MyApp.generalSettings.setLon(lon);
    }

    public static String getDateSignUp() {
        return MyApp.userProfile.getDateSignUp();
    }

    public static void setDateSignUp(String dateSignUp) {
        MyApp.userProfile.setDateSignUp(dateSignUp);
    }

    public static String getAccessToken() {
        return MyApp.userProfile.getAccessToken();
    }

    public static void setAccessToken(String accessToken) {
        MyApp.userProfile.setAccessToken(accessToken);
    }

    public static String getUserName() {
        return MyApp.userProfile.getUserName();
    }

    public static void setUserName(String userName) {
        MyApp.userProfile.setUserName(userName);
    }

    public static void setUserName(TextView userName) {
        String name = userName.getText().toString();

//        if(!name.isEmpty()){
//
//        }

        if (!name.matches(getUserName()) && !name.isEmpty()) {
            ToastUtil.toster("need check", false);
        }
        MyApp.userProfile.setUserName(name);
    }

    public static void setFullName(TextView fullName) {
        String name = fullName.getText().toString();
        MyApp.userProfile.setFullName(name);
    }

    public static void setFullName(String fullName) {
        MyApp.userProfile.setFullName(fullName);
    }

    public static String getFullName() {
        return MyApp.userProfile.getFullName();
    }

    public static String getGcm() {
        return MyApp.generalSettings.getGCMKey();
    }

    public static String getUDID() {
        return DeviceUtil.getDeviceUDID();
    }

    public static String getFirstName() {
        return MyApp.userProfile.getFirstName();
    }

    public static void setFirstName(String firstName) {
        MyApp.userProfile.setFirstName(firstName);
    }

    public static String getSureName() {
        return MyApp.userProfile.getLastName();
    }

    public static void setLastName(String lastName) {
        MyApp.userProfile.setLastName(lastName);
    }

    public static void setPhone(String phone) {
        MyApp.userProfile.setPhone(phone);
    }

    public static void setPhone(TextView phone) {
        MyApp.userProfile.setPhone(phone.getText().toString());
    }

    public static String getPhone() {
        return MyApp.userProfile.getPhone();
    }

    public static String getCity() {
        return MyApp.userProfile.getCity();
    }

    public static void setCity(String city) {
        MyApp.userProfile.setCity(city);
    }

    public static String getStartAlarm() {
        return MyApp.userProfile.getStartAlarm();
    }

    public static long getStartAlarmInMilli() {
        return DateUtil.milliFromString(MyApp.userProfile.getStartAlarm());
    }

    public static void setStartAlarm() {
        MyApp.userProfile.setStartAlarm(DateUtil.getCurrentTimeStamp());
    }

    public static void setStartAlarm(String startTime) {
        Log.i(TAG, "setStartAlarm --> " + startTime);
        MyApp.userProfile.setStartAlarm(startTime);
    }

    public static void setCity(TextView city) {
        MyApp.userProfile.setCity(city.getText().toString());
    }

    public static String getProfileImage() {
        return MyApp.userProfile.getProfileImage();
    }

    public static void setProfileImage(String profileImage) {
        MyApp.userProfile.setProfileImage(profileImage);
    }

    public static void setProfileImageB64(String profileImageBase64) {
        if (!profileImageBase64.isEmpty()) {
            MyApp.userProfile.setProfileImageBase64(profileImageBase64);
        }
    }

    public static String getProfileImageB64() {
        return MyApp.userProfile.getProfileImageBase64();
    }

    public static Bitmap getProfileImageB64Bitmap() {
        if (!getProfileImageB64().isEmpty()) {
            byte[] imageAsBytes = Base64.decode(getProfileImageB64(), Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
        }
        return BitmapFactory.decodeResource(MyApp.appContext.getResources(), AVATAR);
    }

    public static String getFacebookAvatar() {
        return MyApp.userProfile.getFacebookAvatar();
    }

    public static void setFacebookAvatar(String facebookAvatar) {
        MyApp.userProfile.setFacebookAvatar(facebookAvatar);
    }

    public static String getTweeterAvatar() {
        return MyApp.userProfile.getTwitterAvatar();
    }

    public static void setTweeterAvatar(String tweeterAvatar) {
        MyApp.userProfile.setTwitterAvatar(tweeterAvatar);
    }

    public static String getTweeterUserName() {
        return MyApp.userProfile.getTwitterUserName();
    }

    public static void setTweeterUserName(String tweeterUserName) {
        MyApp.userProfile.setTwitterUserName(tweeterUserName);
    }

    public static String getFacebookUserName() {
        return MyApp.userProfile.getFacebookUserName();
    }

    public static void setFacebookUserName(String facebookUserName) {
        MyApp.userProfile.setFacebookUserName(facebookUserName);
    }

    public static boolean isNew() {
        if (getServerId() == 0) {
            return true;
        }
        return false;
    }

    public static void Log(String text) {
        if (showLog) Log.d(TAG, text);
    }

    public static String printProfile() {
        StringBuilder sb = new StringBuilder();
        sb.append("Printing Profile --->");
        sb.append("\n");
        sb.append("loginType: " + getLoginType());
        sb.append("\n");
        sb.append("UserName: " + getUserName());
        sb.append("\n");
        sb.append("FullName: " + getFullName());
        sb.append("\n");
        sb.append("ProfileImage: " + getProfileImage());
        sb.append("\n");
        sb.append("Phone: " + getPhone());
        sb.append("\n");
        sb.append("City(): " + getCity());
        sb.append("\n");
        sb.append("TweeterId(): " + getTweeterId());
        sb.append("\n");
        sb.append("TweeterUserName: " + getTweeterUserName());
        sb.append("\n");
        sb.append("TweeterAvatar: " + getTweeterAvatar());
        sb.append("\n");
        sb.append("FacebookId(): " + getFacebookId());
        sb.append("\n");
        sb.append("FacebookUserName: " + getFacebookUserName());
        sb.append("\n");
        sb.append("FacebookAvatar: " + getFacebookAvatar());
        sb.append("\n");
        sb.append("Category: " + getCategory());
        sb.append("\n");
        sb.append("Lat: " + getLatString());
        sb.append("\n");
        sb.append("lon: " + getLonString());
        sb.append("\n");
        sb.append("UDID: " + getUDID());
        sb.append("\n");
        sb.append("GCM: " + getGcm());
        sb.append("\n");

        Log.i(TAG, sb.toString());
        return sb.toString();
    }

    public static void forgotPassword(FragmentManager fm) {
        DialogManager.showMessageDialog(fm, "Some Message", null);
    }


    public static void logToFacebook(final SimpleFacebook simpleFacebook, final onFacebookLogin onFacebookLogin) {
        currentLoginType = LOGIN_TYPE_FACEBOOK;
        final OnProfileListener onProfileListener = new OnProfileListener() {
            @Override
            public void onComplete(Profile profile) {
                setFacebookId(Long.parseLong(profile.getId()));
                setFacebookAvatar("http://graph.facebook.com/" + getFacebookId() + "/picture?type=square");
                setFacebookUserName(profile.getName());
                onFacebookLogin.onLogin(accessToken, profile.getId(), User.getUDID());
            }
        };

        OnLoginListener onLoginListener = new OnLoginListener() {


            @Override
            public void onException(Throwable throwable) {
                Log.i(TAG, "onException ");
            }

            @Override
            public void onFail(String s) {
                Log.i(TAG, "onFail ");
            }

            @Override
            public void onLogin(String s, List<Permission> list, List<Permission> list1) {
                Log.i(TAG, "Logged in");
                accessToken = simpleFacebook.getAccessToken().getToken();
                simpleFacebook.getProfile(onProfileListener);
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "onCancel ");
            }
        };

        simpleFacebook.login(onLoginListener);
    }

    public static void logOutFromFacebook(SimpleFacebook simpleFacebook) {
        OnLogoutListener onLogoutListener = new OnLogoutListener() {

            @Override
            public void onLogout() {
                Log.i(TAG, "You are logged out");
            }

        };

        simpleFacebook.logout(onLogoutListener);
    }

    public static void logToTwitter(final TwitterLoginButton twitterLoginButton, final onTwitterLogin callback) {
        currentLoginType = LOGIN_TYPE_TWITTER;
        final SwipeRefreshLayout refreshLayout = new SwipeRefreshLayout(MyApp.appContext);
        refreshLayout.setRefreshing(true);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                if (result != null) {
                    setTweeterUserName(result.data.getUserName());
                    setTweeterId(result.data.getUserId());
                    String AuthToken = String.valueOf(result.data.getAuthToken());
                    String[] data = AuthToken.split(",");
                    String AToken = data[0].replace("token=" , "");
                    String Secret = data[1].replace("secret=" , "");
                    callback.onLoginToTwitter(AToken, Secret, User.getUDID());

                    MyTwitterApiClient apiClient = new MyTwitterApiClient(result.data);
                    apiClient.getUsersService().show(getTweeterId(), null, true, "bigger",
                            new Callback<com.twitter.sdk.android.core.models.User>() {
                                @Override
                                public void success(Result<com.twitter.sdk.android.core.models.User> result) {

                                    String userProfileUrl = result.data.profileImageUrl.replace("_normal", "");
                                    setTweeterAvatar(userProfileUrl);
                                }

                                @Override
                                public void failure(TwitterException exception) {
                                    ToastUtil.toster(exception.getMessage(), false);
                                }
                            });
                }
            }

            @Override
            public void failure(TwitterException exception) {
                ToastUtil.toster(exception.getMessage(), false);
            }
        });

        twitterLoginButton.performClick();
    }


    public static void logOut() {
//        switch (getLoginType()){

//            case LOGIN_TYPE_FACEBOOK:
//                logOutFromFacebook();
//                break;
//            case LOGIN_TYPE_TWITTER:
//                break;
//            case LOGIN_TYPE_SERVER:
//                break;
//        }
//        Digits.getSessionManager().clearActiveSession();
        User.setLoginType(LOGIN_TYPE_NONE);

    }

    public static boolean register(EditText name, EditText pass, EditText phone, TextView code, PhoneNumberParser numberParser) {

        String buildingPhone = code.getText() + phone.getText().toString();
        if (buildingPhone.contains("+972")) {
            buildingPhone = buildingPhone.replace("+9720", "+972");
            buildingPhone = buildingPhone.replace("+", "");
            setPhone(buildingPhone.toString());
        }

        if (Validation.password(pass, phone, name, numberParser)) {
            return true;
        }

        return false;
//        currentLoginType = LOGIN_TYPE_SERVER;
    }


    public static void setImage(CircleImageView civ) {

        if (!getProfileImageB64().isEmpty()) {
            civ.setImageBitmap(User.getProfileImageB64Bitmap());
            return;
        }

        switch (getLoginType()) {
            case LOGIN_TYPE_FACEBOOK:
                ImageLoader.ByUrl(getFacebookAvatar(), civ, AVATAR);
                break;
            case LOGIN_TYPE_TWITTER:
                ImageLoader.ByUrl(getTweeterAvatar(), civ, AVATAR);
                break;
            case LOGIN_TYPE_SERVER:
                civ.setImageResource(AVATAR);
                break;
        }
    }

    public static void updateFromServer(Activity activity, final onUserUpdate onUserUpdate, final onErrorCallBack callBack) {
        MyApp.networkManager.makeRequest(new UpdateProfileRequest(String.valueOf(User.getServerId())), new NetworkCallback<UpdateProfileResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<UpdateProfileResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    update(response.getData().getAppUser());
                    getAppData(response.getData().getReports(), onUserUpdate);

                } else {
                    if (callBack != null) {
                        callBack.onError(errorDesc);
                    }
                    ;
                }
            }
        });
    }

    private static void getAppData(Reports reports, final onUserUpdate onUserUpdate) {
        MyApp.userProfile.setLevelColor(Helper.getColorByRank(User.getCurrentGrade()));
        onUserUpdate.userUpdated(reports);
    }


    public static void update(Appuser appuser) {
        setFullName(appuser.getFullName().isEmpty() ? appuser.getUsername() : appuser.getFullName());
        setUserName(appuser.getUsername());
        setProfileImage(appuser.getAvatar());
        setPhone(appuser.getPhone());
        setCity(appuser.getCity());
        setIsPoliceman(appuser.getIsPoliceman());
        setCurrentGrade(appuser.getRank());
        setNumberOfStarts(appuser.getCredibility());
    }

    public interface onFacebookLogin {
        public void onLogin(String accessToken, String id, String udid);
    }

    public interface onTwitterLogin {
        public void onLoginToTwitter(String Token, String secret, String udid);
    }

    public void warnDialog(final Activity activity, final String msg) {
        if (!showWarnDialogFlag) {
            return;
        }

        if (activity.isFinishing())
            return;

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(activity)
                        .title("Error")
                        .content(msg)
                        .positiveText(android.R.string.ok)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                            }
                        })
                        .show();
            }
        });
    }

    public static void progressDialog(final Activity activity) {
        if (activity.isFinishing()) return;

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                dialog = new MaterialDialog.Builder(activity)
                        .title(R.string.app_name)
                        .content(R.string.please_wait)
                        .progress(true, 0)
                        .show();
            }
        });
    }

    public static void dismissDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public interface onUserUpdate {
        public void userUpdated(Reports reportObj);
    }

    public static void updateLocation(final onErrorCallBack callBack) {
        MyApp.networkManager.makeRequest(new updateLocationRequest(User.getLatString()/*Lat*/, User.getLonString()/*Lng*/), new NetworkCallback<updateLocationResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<updateLocationResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ToastUtil.toster("Location Updated", true);
                } else {
                    if (callBack != null) {
                        callBack.onError(errorDesc);
                    }
                    ;
                }
            }
        });
    }
}

/*   /* private void loadAndSaveProfileImage (String url){
      PicpackApp.userProfil.setUserServerImageUrl(userProfileUrl);
      loadAndSaveProfileImage(userProfileUrl);
        if (url != null && !url.isEmpty()){
            ImageLoader.loadImageAndSaveItOnStorage("profile_image.jepg",
                    url, new ImageLoader.LoadImageCallback() {
                        @Override
                        public void onLoadImage(boolean success, Bitmap image, String fileName) {
                            if (success && fileName != null) {
                                // PicpackApp.userProfil.setUserLocalImagePath(fileName);
                            }

                        }
                    });
        }
    }*/

