package com.positiveapps.weaware.objects;

/**
 * Created by Izakos on 08/10/2015.
 */
public interface Storable<T> {
    public T fromJson(String objectAsString);
    public String toJson();
}
