package com.positiveapps.weaware.objects;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.positiveapps.weaware.main.MyApp;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.util.Set;

/**
 * Created by Izakos on 21/10/2015.
 */
public class TaregtX implements Target {

    private Set<TaregtX> markersTargets;
    Person person;

    public TaregtX(Person person, Set<TaregtX> markersTargets) {
        this.markersTargets = markersTargets;
        this.person = person;
    }


    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] profileImageBlob = bos.toByteArray();
        person.setProfileImageBlob(profileImageBlob);
        MyApp.tablePersons.insertOrUpdate(person);
        markersTargets.remove(this);
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        Log.e("BLOB", "profileImageBlo2b");
        markersTargets.remove(this);
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {
        Log.e("BLOB", "profileImageBlob3");
    }
}