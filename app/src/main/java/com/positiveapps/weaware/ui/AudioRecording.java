package com.positiveapps.weaware.ui;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;

/**
 * Created by Izakos on 23/11/2015.
 */
public class AudioRecording {

    private static final String LOG_TAG = "AudioRecordTest";
    private static String mFileName = null;

    //    private RecordButton mRecordButton = null;
    private MediaRecorder mRecorder = null;

    //    private PlayButton   mPlayButton = null;
    private MediaPlayer mPlayer = null;

    public AudioRecording() {
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audiorecordtest.3gp";
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }

//    private void setRecordingTimer(final TextView textView){
//      final int cnt = 0;
//        CountDownTimer t = new CountDownTimer( Long.MAX_VALUE , 1000) {
//
//            @Override
//            public void onTick(long millisUntilFinished) {
//
//
//                cnt++;
//                String time = new Integer(cnt).toString();
//
//                    long millis = cnt;
//                    int seconds = (int) (millis / 60);
//                int minutes = seconds / 60;
//                seconds     = seconds % 60;
//
//                textView.setText(String.format("%d:%02d:%02d", minutes, seconds,millis));
//
//            }
//
//            @Override
//            public void onFinish() {            }
//        };
//    }

    public static void startRecording(TextView textView){

    }

}
