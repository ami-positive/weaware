package com.positiveapps.weaware.ui;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by Izakos on 08/12/2015.
 */
public class ReportMapIconsAnimation {

    private LinearLayout audio;
    private LinearLayout camera;
    private LinearLayout text;
    private EditText editText;

    public ReportMapIconsAnimation(LinearLayout audio, LinearLayout camera, LinearLayout text, EditText editText) {
        this.audio = audio;
        this.camera = camera;
        this.text = text;
        this.editText = editText;
        initUI();
    }

    private void initUI(){
        int fullDistance = audio.getWidth() + camera.getWidth() + audio.getWidth();
        int distance = audio.getWidth() + camera.getWidth();
        editText.getLayoutParams().width = distance;
        editText.setTranslationX((float)fullDistance);
        editText.setVisibility(View.VISIBLE);
    }

    public void textStart(){
        float distance = (float)(audio.getWidth() + camera.getWidth());
        audio.animate().translationX(distance * -1);
        camera.animate().translationX(distance * -1);
        editText.animate().translationX(0);
    }

    public void textEnd(){
        float fullDistance = (float)(audio.getWidth() + camera.getWidth() + audio.getWidth());
        audio.animate().translationX(0);
        camera.animate().translationX(0);
        editText.animate().translationX(fullDistance);
    }

    public void audioStart(){

    }

    public void audioEnd(){

    }

    public void cameraStart(){

    }

    public void cameraEnd(){

    }
}
