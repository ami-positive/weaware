/**
 * 
 */
package com.positiveapps.weaware.ui;



import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.positiveapps.weaware.main.MyApp;

/**
 * @author natiapplications
 *
 */
public class SwipeDetector implements OnTouchListener {

	 
	private final GestureDetector gestureDetector;
	private boolean canSwipe;
	
	    
	public SwipeDetector(Context ctx) {
		gestureDetector = new GestureDetector(ctx, new GestureListener());
	}

	public boolean onTouch(final View view, final MotionEvent motionEvent) {
		Log.e("OnTouchLog", "onTouch");
		return gestureDetector.onTouchEvent(motionEvent);
	}
	
	public void onTouchEvent (MotionEvent me){
		gestureDetector.onTouchEvent(me);
	}

	private final class GestureListener extends SimpleOnGestureListener {

		private static final int SWIPE_THRESHOLD = 100;
		private static final int SWIPE_VELOCITY_THRESHOLD = 100;

		
		@Override
		public boolean onDown(MotionEvent e) {
			if (e.getRawX() > MyApp.generalSettings.getScreenWidth()-100){
				canSwipe = true;
			}else{
				canSwipe = false;
			}
			return true;
		}
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			Log.e("OnTouchLog", "onFling");
			boolean result = false;
			try {
				float diffY = e2.getY() - e1.getY();
				float diffX = e2.getX() - e1.getX();
				if (Math.abs(diffX) > Math.abs(diffY)) {
					if (Math.abs(diffX) > SWIPE_THRESHOLD
							&& Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
						if (diffX > 0) {
 							  onSwipeRight();
						} else {
 							if(canSwipe){
								onSwipeLeft();
							}
 						}
					}
				} else {
					if (Math.abs(diffY) > SWIPE_THRESHOLD
							&& Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
						if (diffY > 0) {
							onSwipeBottom();
						} else {
							onSwipeTop();
						}
					}
				}
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			return result;
		}
	}

	public void onSwipeRight() {
		Log.e("OnTouchLog", "R");
		
	}

	public void onSwipeLeft() {
		Log.e("OnTouchLog", "L");
		
	}

	public void onSwipeTop() {
		Log.e("OnTouchLog", "T");
		
	}

	public void onSwipeBottom() {
		Log.e("OnTouchLog", "B");
		
	}
	
}

	
   

   