package com.positiveapps.weaware.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.util.AppUtil;


/**
 * Created by Izakos on 20/12/2015.
 */
public class TextBox extends TextView{


    private int sideDrawable;
    private String theText;
    private boolean isEditText = false;
    private Context context;


    private Paint marginPaint;
    private Paint linePaint;
    private int paperColor;
    private float margin;

    private Resources resources;

    public TextBox(Context context) {
        super(context);
        init(context);
    }

    public TextBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextBox,
                0, 0);

        try {
            isEditText = a.getBoolean(R.styleable.TextBox_isEditText, false);
            theText = a.getString(R.styleable.TextBox_theText);
            sideDrawable = a.getInt(R.styleable.TextBox_sideDrawable, R.drawable.abc_ab_share_pack_mtrl_alpha);
        } finally {
            a.recycle();
        }

        init(context);
    }

    public TextBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top, Drawable right, Drawable bottom) {


        Drawable image = getResources().getDrawable(R.drawable.gradient);
        Drawable icon = getResources().getDrawable(R.drawable.mail_icon);

        int heigth = (int)AppUtil.convertDpToPixel(50);
        int width = (int)AppUtil.convertDpToPixel(90);

        int iconHeigth = (int)AppUtil.convertDpToPixel(30);
        int iconWidth = (int)AppUtil.convertDpToPixel(30);

        int upDownPadding = (int)AppUtil.convertDpToPixel(10);


//        Bitmap bitmap2 = ((BitmapDrawable) icon).getBitmap();
//        icon = new BitmapDrawable(resources, Bitmap.createScaledBitmap(bitmap2, iconWidth, iconHeigth, true));

        icon.setBounds(0, 0, iconWidth, iconHeigth);
        Drawable[] drawarray = {image, icon};
        LayerDrawable layerdrawable = new LayerDrawable(drawarray);
        layerdrawable.setLayerInset(1, 0, 0, iconWidth, upDownPadding);

        layerdrawable.setBounds(0, 0, width, heigth);

        super.setCompoundDrawables(left, top, layerdrawable, bottom);
    }



    private void init(Context context) {
        this.context = context;
        resources = context.getResources();

//        Typeface face=Typeface.createFromAsset(context.getAssets(), "Helvetica_Neue.ttf");
//        this.setTypeface(face);

//        marginPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        marginPaint.setColor(Color.RED);
//        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        linePaint.setColor(Color.BLUE);
//        paperColor = Color.YELLOW;
//        margin = Color.CYAN;
    }



    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText("Hello", type);
    }



    @Override
    public void setBackgroundColor(int color) {
        super.setBackgroundColor(Color.WHITE);
    }

    @Override
    public void onDraw(Canvas canvas) {
//        canvas.drawColor(paperColor);
//        canvas.drawLine(0, 0, getMeasuredHeight(), 0, linePaint);
//        canvas.drawLine(0, getMeasuredHeight(), getMeasuredWidth(),
//                getMeasuredHeight(), linePaint);
//        canvas.drawLine(margin, 0, margin, getMeasuredHeight(), marginPaint);
//        canvas.save();
//        canvas.translate(margin, 0);
        super.onDraw(canvas);
//        canvas.restore();
    }

    //    public void setShowText(boolean showText) {
//        mShowText = showText;
//        invalidate();
//        requestLayout();
//    }




}
