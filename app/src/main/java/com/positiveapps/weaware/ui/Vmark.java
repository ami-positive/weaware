package com.positiveapps.weaware.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.Helper;

/**
 * Created by Izakos on 09/12/2015.
 */
public class Vmark {

    private View mainView;
    private int markGreenRes = R.drawable.mark;
    private int markGrayRes = R.drawable.mark_gray;
    private Context context = MyApp.appContext;
    private Resources resources = context.getResources();


    public void set(Button button, int orgImageRes, boolean isGreen, boolean withSound){

        int markRes = isGreen ? markGreenRes : markGrayRes;

        int width = button.getWidth();
        int hight = button.getHeight();

        Drawable myIcon;
        Drawable marker;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            myIcon = resources.getDrawable(orgImageRes, context.getTheme());
            marker = resources.getDrawable(markRes, context.getTheme());
        } else {
            myIcon = resources.getDrawable(orgImageRes);
            marker = resources.getDrawable(markRes);
        }

        int size = (int)AppUtil.convertDpToPixel(30f, context);
        Bitmap bitmap = ((BitmapDrawable) marker).getBitmap();
        marker = new BitmapDrawable(resources, Bitmap.createScaledBitmap(bitmap, size, size, true));

        Drawable[] drawarray = {myIcon, marker};
        LayerDrawable layerdrawable = new LayerDrawable(drawarray);
        layerdrawable.setLayerInset(1, (width - size), (hight - size), 0, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            button.setBackground(layerdrawable);
        } else {
            button.setBackgroundResource(orgImageRes);
        }

        if(isGreen && withSound) Helper.play(R.raw.click);
    }

    public void set(ImageView imageView, int orgImageRes, boolean isGreen, boolean withSound){

        int markRes = isGreen ? markGreenRes : markGrayRes;

        int width = imageView.getWidth();
        int hight = imageView.getHeight();

//        BitmapFactory.Options dimensions = new BitmapFactory.Options();
//        dimensions.inJustDecodeBounds = true;
//        Bitmap mBitmap = BitmapFactory.decodeResource(resources, markRes, dimensions);
//        int markHeight = dimensions.outHeight;
//        int MarkWidth =  dimensions.outWidth;

        Drawable myIcon = resources.getDrawable(orgImageRes);
        Drawable marker = resources.getDrawable(markRes);
        int size = (int)AppUtil.convertDpToPixel(10f, context);
        Bitmap bitmap = ((BitmapDrawable) marker).getBitmap();
        marker = new BitmapDrawable(resources, Bitmap.createScaledBitmap(bitmap, size, size, true));

        Drawable[] drawarray = {myIcon, marker};
        LayerDrawable layerdrawable = new LayerDrawable(drawarray);
        layerdrawable.setLayerInset(1, (width - size), (hight - size), 0, 0);
        imageView.setImageDrawable(layerdrawable);

        if(isGreen && withSound) Helper.play(R.raw.click);
    }

    public void remove(Button button, int orgImageRes){
        button.setBackgroundResource(orgImageRes);
    }

    public void remove(ImageView imageView, int orgImageRes){
        imageView.setImageResource(orgImageRes);
    }




}
