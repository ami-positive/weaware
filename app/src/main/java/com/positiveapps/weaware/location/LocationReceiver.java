/**
 * 
 */
package com.positiveapps.weaware.location;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.positiveapps.weaware.main.MainActivity;
import com.positiveapps.weaware.main.MyApp;

/**
 * @author natiapplications
 *
 */
public class LocationReceiver extends BroadcastReceiver {

    double latitude, longitude;

    @Override
    public void onReceive(final Context context, final Intent calledIntent)
    {
        Log.d("LOC_RECEIVER", "Location RECEIVED!");

        updateUserLocation(context);

    }

    
    private synchronized void updateUserLocation (Context context){

    	//TODO send user location to the remote server



        Coordinates coordinates =
                new Coordinates(MyApp.generalSettings.getLat(),MyApp.generalSettings.getLon());

        Message message = new Message();
        message.what = 1;
        message.obj = coordinates;



	}
}
