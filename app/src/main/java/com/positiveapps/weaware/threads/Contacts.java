package com.positiveapps.weaware.threads;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;

import android.util.Log;
import android.widget.ImageView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.util.TextUtil;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

/**
 * Created by Izakos on 07/10/2015.
 */
public class Contacts extends Thread{

    @Override
    public void run() {

        ContentResolver cr = MyApp.appContext.getContentResolver(); //Activity/Application android.content.Context
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        if(cursor.moveToFirst())
        {
            do
            {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                {
                    Person person = new Person();
                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));

                    person.setUserName(displayName);
//                    person.setContactId(Long.parseLong(id));
                    //InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(MapOApp.appContext.getContentResolver(),
                    //        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id)));
                    Uri path = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id));
//	            	 if (path != null){
//	            		 Uri photoUri = Uri.withAppendedPath(path, Contacts.Photo.CONTENT_DIRECTORY);
//	            		 friend.setImagePath(photoUri.getPath());
//	            	 }


//		        	 if(inputStream != null){
//		        		friend.setProfileImage(inputStream);
//		        	 }

                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{ id }, null);
                    while (pCur.moveToNext())
                    {

                        String friendNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //friendNumber = friendNumber.replaceAll("-", "");
                        Log.d("normalizePhoneLog", "current phone: " + friendNumber);
                        friendNumber = TextUtil.normalizePhoneNumber(friendNumber);
                        if (friendNumber == null){
                            Log.e("normalizePhoneLog", "phone is not valid !");
                            friendNumber = "";
                        }else{

//                            friendNumber = friendNumber.replace("+", "");
//                            if(friendNumber.substring(0, 3).matches("9720")){
//                                friendNumber = TextUtil.removeCharAt(friendNumber, 3);
//                            }

                        }
                        Log.i("normalizePhoneLog", "apter norm: " + friendNumber);
//                        person.setPhone1(friendNumber);

                        break;
                    }

//                    Cursor cur1 = MyApp.appContext.getContentResolver().query(
//                            ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
//                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
//                            new String[]{id}, null);
//                    if (cur1 != null){
//                        while (cur1.moveToNext()) {
//                            String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//                            if (email!= null){
//                                friend.setEmail(email);
//                            }
//                        }
//                        cur1.close();
//                    }
                    MyApp.tablePersons.insertOrUpdate(person);
                    pCur.close();
                }


            } while (cursor.moveToNext()) ;
        }
        cursor.close();




    }



}
