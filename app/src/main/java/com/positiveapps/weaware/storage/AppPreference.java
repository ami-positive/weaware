/**
 * 
 */
package com.positiveapps.weaware.storage;



import android.content.Context;
import android.content.SharedPreferences;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.util.DateUtil;


/**
 * @author Nati Gabay
 *
 */
public class AppPreference {

	
	public static final int UNITS_TYPE_KM = 1;
	public static final int UNITS_TYPE_MILES = 2;
	public static final int SKI_PASS_TYPE_AREA = 1;
	public static final int SKI_PASS_TYPE_RESORT = 2;
	
	
	private static AppPreference instance;
	private static Context context = MyApp.appContext;
	
	private static PreferenceUserProfil userProfil;
	private static PreferenceUserSettings userSettings;
	private static PreferenceGeneralSettings generalSettings;

	private AppPreference (Context context){
		userSettings = PreferenceUserSettings.getUserSettingsInstance();
		userProfil = PreferenceUserProfil.getUserProfileInstance();
		generalSettings = PreferenceGeneralSettings.getGeneralSettingsInstance();
	}
	
	public static AppPreference getInstans(Context context){
		if(instance == null){
			instance = new AppPreference(context);
		}
		setInstancContext(context);
		return instance;
	}
	
	public static void removeInstance(){
		userSettings.removeInstance();
		userProfil.removeInstance();
		generalSettings.removeInstance();
		instance = null;
	}
	
	private static void setInstancContext (Context context){
		AppPreference.context = context;
	}
	
	
	
	/**
	 * @return the userProfil
	 */
	public PreferenceUserProfil getUserProfil() {
		return userProfil;
	}

	/**
	 * @param userProfil the userProfil to set
	 */
	public void setUserProfil(PreferenceUserProfil userProfil) {
		this.userProfil = userProfil;
	}

	/**
	 * @return the userSettings
	 */
	public PreferenceUserSettings getUserSettings() {
		return userSettings;
	}

	/**
	 * @param userSettings the userSettings to set
	 */
	public void setUserSettings(PreferenceUserSettings userSettings) {
		this.userSettings = userSettings;
	}
	
	/**
	 * @return the generalSettings
	 */
	public PreferenceGeneralSettings getGeneralSettings() {
		return generalSettings;
	}

	/**
	 * @param generalSettings the generalSettings to set
	 */
	public void setGeneralSettings(PreferenceUserSettings generalSettings) {
		this.userSettings = generalSettings;
	}


	




	public static class PreferenceGeneralSettings {

		public final String GENERAL_SETTINGS = "GeneralSettings";

		public static final String LAT = "Lat";
		public static final String LON = "Lon";
		public static final String GCM_KEY = MyApp.appVersion + "GcmKey";
		public static final String SCREEN_WIDTH = "ScreenWidth";
		public static final String SCREEN_HIGHT = "ScreenHight";
		public static final String LAST_READ_MESSAGE_COUNT = "LastReadMessageCount";
		public static final String USER_LOGIN_LEVEL = "userLoginLevel";
		/* refine feed */
		public static final String PICUP_RADIUS = "picupRadius";
		public static final String DELIVERY_TIME = "deliveryTime";
		public static final String DELIVERY_PRICE = "deliveryPrice";
		public static final String DIMENSION = "dimension";
		public static final String PROPERTIES_FRAGILE = "propertiesFragile";
		public static final String PROPERTIES_WEIGHT = "propertiesWeight";
		public static final String PROPERTIES_TEMPERTURE = "propertiesTemperture";
		
		private static final String ADMIN_PHONE = "adminPhone";
		private static final String ADMIN_MAIL = "adminMail";
		private static final String HAS_GENERATE_DB = "has_generate_db12";
		private static final String APPLICATION_NAME = "ApplicationName"; 
		private static final String VERSION = "Version"; 
		private static final String SERVER_TIME = "ServerTime"; 
		private static final String LAST_FAQ_UPDATED = "LastFAQUpdate"; 
		private static final String PACKAGE_EXPIRATION = "PackageExpiration"; 
		private static final String DELIVERIES_REFESH_RATE = "DeliveriesRefreshRate"; 
		
		private static final String UNVIEW_NEGO_COUNT = "UnViewNegoCount"; 
		
		private SharedPreferences generalSettings;
		public static PreferenceGeneralSettings generalSettingInstance;

		private PreferenceGeneralSettings() {
			generalSettings = context.getSharedPreferences(GENERAL_SETTINGS,
					Context.MODE_PRIVATE);
		}

		public static PreferenceGeneralSettings getGeneralSettingsInstance() {
			if (generalSettingInstance == null) {
				generalSettingInstance = new PreferenceGeneralSettings();
			}
			return generalSettingInstance;
		}
		
		public void removeInstance(){
			generalSettings.edit().clear().commit();
			generalSettingInstance = null;
		}
		
		public void setApplicationName(String name) {
			generalSettings.edit().putString(APPLICATION_NAME, name).commit();
		}
		
		public String getApplicationName(){
			return generalSettings.getString(APPLICATION_NAME, "PikPack");
		}
		
		public void setAppServerVersion(String version) {
			generalSettings.edit().putString(VERSION, version).commit();
		}
		
		public String getAppServerVersion(){
			return generalSettings.getString(VERSION, "1.0.0");
		}
		
		
		
		public void setServerTime(String time) {
			generalSettings.edit().putString(SERVER_TIME, time).commit();
		}
		
		public String getServerTime(){
			return generalSettings.getString(SERVER_TIME, DateUtil.getCurrentDateAsString
					(DateUtil.FORMAT_AMERICAN_SERVER));
		}
		
		
		public void setLasFaqUpdated(long set) {
			generalSettings.edit().putLong(LAST_FAQ_UPDATED, set).commit();
		}
		
		public long getLastFaqUpdated(){
			return generalSettings.getLong(LAST_FAQ_UPDATED, DateUtil.getCurrentDateInMilli());
		}
		
		
		public void setPackageExprationCount(long count) {
			generalSettings.edit().putLong(PACKAGE_EXPIRATION, count).commit();
		}
		
		public void setUserLoginLevel(int set) {
			generalSettings.edit().putInt(USER_LOGIN_LEVEL, set).commit();
		}
		
		public long getPackageExprationCount(){
			return /*generalSettings.getLong(PACKAGE_EXPIRATION,*/3*(60*60*1000);
		}
		
		
		public void setDeliveriesRefreshRate(boolean set) {
			generalSettings.edit().putBoolean(DELIVERIES_REFESH_RATE, set).commit();
		}
		
		public boolean isDeliveriesNeedRefreshRate(){
			return generalSettings.getBoolean(DELIVERIES_REFESH_RATE, false);
		}

		public void setPicupRadius(String picupRadius) {
			generalSettings.edit().putString(PICUP_RADIUS, picupRadius).commit();
		}
		public String getPicupRadius() {
			return generalSettings.getString(PICUP_RADIUS, "");
		}
		public void setDeliveryTime(String deliveryTime) {
			generalSettings.edit().putString(DELIVERY_TIME, deliveryTime).commit();
		}
		public String getDeliveryTime() {
			return generalSettings.getString(DELIVERY_TIME, "");
		}
		public void setDeliveryPrice(String deliveryPrice) {
			generalSettings.edit().putString(DELIVERY_PRICE, deliveryPrice).commit();
		}
		public String getDeliveryPrice() {
			return generalSettings.getString(DELIVERY_PRICE, "");
		}
		public void setDimension(String dimension) {
			generalSettings.edit().putString(DIMENSION, dimension).commit();
		}
		public String getDimension() {
			return generalSettings.getString(DIMENSION, "");
		}
		public void setPropertiesFragile(String propertiesFragile) {
			generalSettings.edit().putString(PROPERTIES_FRAGILE, propertiesFragile).commit();
		}
		public String getPropertiesFragile() {
			return generalSettings.getString(PROPERTIES_FRAGILE, "");
		}
		public void setPropertiesWeight(String propertiesWeight) {
			generalSettings.edit().putString(PROPERTIES_WEIGHT, propertiesWeight).commit();
		}
		public String getPropertiesWeight() {
			return generalSettings.getString(PROPERTIES_WEIGHT, "");
		}
		public void setPropertiesTemperture(String propertiesTemperture) {
			generalSettings.edit().putString(PROPERTIES_TEMPERTURE, propertiesTemperture).commit();
		}
		public String Temperture() {
			return generalSettings.getString(PROPERTIES_TEMPERTURE, "");
		}

		
		public void setLat(String toSet) {
			generalSettings.edit().putString(LAT, toSet).commit();
		}

		public void setLon(String toSet) {
			generalSettings.edit().putString(LON, toSet).commit();
		}
		
		public void setGCMKey(String gcmKey) {
			generalSettings.edit().putString(GCM_KEY, gcmKey).commit();
		}
		
		
		
		public void setScreenWidth(int width) {
			generalSettings.edit().putInt(SCREEN_WIDTH, width).commit();
		}
		
		public void setScreenHight(int hight) {
			generalSettings.edit().putInt(SCREEN_HIGHT, hight).commit();
		}

		public void setLastReadMessageCount(String deliveryID,int count) {
			generalSettings.edit().putInt(deliveryID + LAST_READ_MESSAGE_COUNT, count).commit();
		}
		
		public String getLat() {
			return generalSettings.getString(LAT, "0.0");
		}

		public String getLon() {
			return generalSettings.getString(LON, "0.0");
		}
		
		
		public String getLocation () {
			return getLat()+","+getLon();
		}
		
		public String getGCMKey () {
			return generalSettings.getString(GCM_KEY, "");
		}
		
		public int getUserLoginLevel () {
			return generalSettings.getInt(USER_LOGIN_LEVEL, 0);
		}
		
		public int getScreenWidth () {
			return generalSettings.getInt(SCREEN_WIDTH, 0);
		}
		
		public int getScreenHight () {
			return generalSettings.getInt(SCREEN_HIGHT, 0);
		}
		
		public int getLastReadMessageCount (String deliveryID) {
			return generalSettings.getInt(deliveryID + LAST_READ_MESSAGE_COUNT, 0);
		}
		
		public String getAdminPhone(){
			return generalSettings.getString(ADMIN_PHONE, "");
		}
		
		public void setAdminPhone(String adminPhone) {
			generalSettings.edit().putString(ADMIN_PHONE, adminPhone).commit();
		}
		public String getAdminMail(){
			return generalSettings.getString(ADMIN_MAIL, "");
		}
		
		public void setAdminMail(String adminMail) {
			generalSettings.edit().putString(ADMIN_MAIL, adminMail).commit();
		}
		
		
		public void setHasGenerateDB(boolean set) {
			generalSettings.edit().putBoolean(HAS_GENERATE_DB, set).commit();
		}
		
		public boolean isHasGenerateDB(){
			return generalSettings.getBoolean(HAS_GENERATE_DB, false);
		}
		
		
		public void setUnviewNegoValue(int set) {
			generalSettings.edit().putInt(UNVIEW_NEGO_COUNT, set).commit();
		}
		
		public int getUnviewNegoValue(){
			return generalSettings.getInt(UNVIEW_NEGO_COUNT, 0);
		}

	}



	public static class PreferenceUserProfil {

		public  final String USER_PROFILE = "UserProfile";

		private SharedPreferences userProfile;
		private static PreferenceUserProfil userProfileInstance;
		
		private  PreferenceUserProfil (){
			userProfile = context.getSharedPreferences(USER_PROFILE, Context.MODE_PRIVATE);
		}
		
		public  static PreferenceUserProfil getUserProfileInstance (){
			if (userProfileInstance == null){
				userProfileInstance = new PreferenceUserProfil();
			}
			return userProfileInstance;
		}


		
		public  void removeInstance(){
			userProfile.edit().clear().commit();
			userProfileInstance = null;
		}

		public static final String USER_NAME = "userName";

		public void setUserName(String userName) {
			userProfile.edit().putString(USER_NAME, userName).commit();
		}

		public String getUserName(){
			return userProfile.getString(USER_NAME, "");
		}

		public static final String FIRST_NAME = "firstName";

		public void setFirstName(String firstName) {
			userProfile.edit().putString(FIRST_NAME, firstName).commit();
		}

		public String getFirstName(){
			return userProfile.getString(FIRST_NAME, "");
		}

		public static final String LAST_NAME = "lastName";

		public void setLastName(String lastName) {
			userProfile.edit().putString(LAST_NAME, lastName).commit();
		}

		public String getLastName(){
			return userProfile.getString(LAST_NAME, "");
		}

		public static final String FULL_NAME = "fullName";

		public void setFullName(String fullName) {
			userProfile.edit().putString(FULL_NAME, fullName).commit();
		}

		public String getFullName(){
			return userProfile.getString(FULL_NAME, "");
		}

		public static final String CATEGORY = "category";

		public void setCategory(int category) {
			userProfile.edit().putInt(CATEGORY, category).commit();
		}

		public int getCategory(){
			return userProfile.getInt(CATEGORY, 0);
		}

		public static final String LEVEL_COLOR = "levelColor";

		public void setLevelColor(String levelColor) {
			userProfile.edit().putString(LEVEL_COLOR, levelColor).commit();
		}

		public String geLevelColor(){
			return userProfile.getString(LEVEL_COLOR, "#ffffff");
		}

		public static final String TWITTER_AVATAR = "twitterAvatar";

		public void setTwitterAvatar(String twitterAvatar) {
			userProfile.edit().putString(TWITTER_AVATAR, twitterAvatar).commit();
		}

		public String getTwitterAvatar(){
			return userProfile.getString(TWITTER_AVATAR, "");
		}

		public static final String TWITTER_ID = "twitterId";

		public void setTwitterId(long twitterId) {
			userProfile.edit().putLong(TWITTER_ID, twitterId).commit();
		}

		public long getTwitterId(){
			return userProfile.getLong(TWITTER_ID, 0);
		}

		public static final String PHONE = "phone";

		public void setPhone(String phone) {
			userProfile.edit().putString(PHONE, phone).commit();
		}

		public String getPhone(){
			return userProfile.getString(PHONE, "");
		}

		public static final String START_ALARM = "startAlarm";

		public void setStartAlarm(String startAlarm) {
			userProfile.edit().putString(START_ALARM, startAlarm).commit();
		}

		public String getStartAlarm(){
			return userProfile.getString(START_ALARM, "");
		}

		public static final String TWITTER_USER_NAME = "twitterUserName";

		public void setTwitterUserName(String twitterUserName) {
			userProfile.edit().putString(TWITTER_USER_NAME, twitterUserName).commit();
		}

		public String getTwitterUserName(){
			return userProfile.getString(TWITTER_USER_NAME, context.getResources().getString(R.string.full_name));
		}

		public static final String FACEBOOK_ID = "facebookId";

		public void setFacebookId(long facebookId) {
			userProfile.edit().putLong(FACEBOOK_ID, facebookId).commit();
		}

		public long getFacebookId(){
			return userProfile.getLong(FACEBOOK_ID, 0);
		}

		public static final String FACEBOOK_AVATAR = "facebookAvatar";

		public void setFacebookAvatar(String facebookAvatar) {
			userProfile.edit().putString(FACEBOOK_AVATAR, facebookAvatar).commit();
		}

		public String getFacebookAvatar(){
			return userProfile.getString(FACEBOOK_AVATAR, "");
		}

		public static final String FACEBOOK_USER_NAME = "facebookUserName";

		public void setFacebookUserName(String facebookUserName) {
			userProfile.edit().putString(FACEBOOK_USER_NAME, facebookUserName).commit();
		}

		public String getFacebookUserName(){
			return userProfile.getString(FACEBOOK_USER_NAME, "");
		}

		public static final String SERVER_ID = "serverId";

		public void setServerId(long serverId) {
			userProfile.edit().putLong(SERVER_ID, serverId).commit();
		}

		public long getServerId(){
			return userProfile.getLong(SERVER_ID, 0);
		}

		public static final String PROFILE_IMAGE_BASE_64 = "profileImageBase64";

		public void setProfileImageBase64(String profileImageBase64) {
			userProfile.edit().putString(PROFILE_IMAGE_BASE_64, profileImageBase64).commit();
		}

		public String getProfileImageBase64(){
			return userProfile.getString(PROFILE_IMAGE_BASE_64, "");
		}

		public static final String ACCESS_TOKEN = "accessToken";

		public void setAccessToken(String accessToken) {
			userProfile.edit().putString(ACCESS_TOKEN, accessToken).commit();
		}

		public String getAccessToken(){
			return userProfile.getString(ACCESS_TOKEN, "");
		}

		public static final String CITY = "city";

		public void setCity(String city) {
			userProfile.edit().putString(CITY, city).commit();
		}

		public String getCity(){
			return userProfile.getString(CITY, "");
		}

		public static final String PROFILE_IMAGE = "profileImage";

		public void setProfileImage(String profileImage) {
			userProfile.edit().putString(PROFILE_IMAGE, profileImage).commit();
		}

		public String getProfileImage(){
			return userProfile.getString(PROFILE_IMAGE, "");
		}

		public static final String CURRENT_GRADE = "currentGrade";

		public void setCurrentGrade(int currentGrade) {
			userProfile.edit().putInt(CURRENT_GRADE, currentGrade).commit();
		}

		public int getCurrentGrade(){
			return userProfile.getInt(CURRENT_GRADE, 0);
		}

		public static final String NUMBER_OF_STARS = "numberOfStars";

		public void setNumberOfStars(int numberOfStars) {
			userProfile.edit().putInt(NUMBER_OF_STARS, numberOfStars).commit();
		}

		public int getNumberOfStars(){
			return userProfile.getInt(NUMBER_OF_STARS, 3);
		}

		public static final String DATE_SIGN_UP = "dateSignUp";

		public void setDateSignUp(String dateSignUp) {
			userProfile.edit().putString(DATE_SIGN_UP, dateSignUp).commit();
		}

		public String getDateSignUp(){
			return userProfile.getString(DATE_SIGN_UP, "");
		}

		public static final String MODE = "mode";

		public void setMode(int mode) {
			userProfile.edit().putInt(MODE, mode).commit();
		}

		public int getMode(){
			return userProfile.getInt(MODE, 0);
		}

		public static final String REPORT_ID = "reportID";

		public void setReportID(long reportID) {
			userProfile.edit().putLong(REPORT_ID, reportID).commit();
		}

		public long getReportID(){
			return userProfile.getLong(REPORT_ID, 0);
		}
		public void removeAll() {
			userProfile.edit().clear().commit();
		}
	}


	
	public static class PreferenceUserSettings {

		public  final String USER_SETTINGS = "UserSettings";


		private SharedPreferences userSettings;
		public static  PreferenceUserSettings userSettingInstance;
		
		private PreferenceUserSettings() {
			userSettings = context.getSharedPreferences(USER_SETTINGS, Context.MODE_PRIVATE);
		}
		
		public static PreferenceUserSettings getUserSettingsInstance() {
			if (userSettingInstance == null){
				userSettingInstance = new PreferenceUserSettings();
			}
			return userSettingInstance;
		}
		
		public void removeInstance(){
			userSettings.edit().clear().commit();
			userSettingInstance = null;
		}


		public void removeAllSettings (){
			userSettings.edit().clear().commit();
		}
	}
	
	
	
	

	
	
	
	
	

}
