/**
 * 
 */
package com.positiveapps.weaware.storage;

import java.io.Serializable;

import com.positiveapps.weaware.main.MyApp;

import android.util.Log;


/**
 * @author Nati Gabay
 *
 */
public class StorageManager {
	
	private final String TAG = "storatelog";


	public static final String FILE_NAME_PERSON = "fileNamePerson";
	
	
	private static StorageManager instance;
	
	private StorageManager () {
		
	}
	
	public static StorageManager getInstance () {
		if (instance == null){
			instance = new StorageManager();
		}
		
		
		return instance;
	}
	
	public static void removeInstance () {
		instance = null;
	}
	
	public void readFromStorage (String fileName, StorageListener callback) {
		Log.d(TAG, fileName);
		new ReadFromStorageThread(MyApp.appContext, fileName, callback).start();
	}
	
	public void writeToStorage (String fileName, Serializable toWrite){
		Log.e(TAG, fileName);
		new WriteToStorageThread(MyApp.appContext, toWrite, fileName).start();
	}

}
