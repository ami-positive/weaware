/**
 * 
 */
package com.positiveapps.weaware.dialogs;

import android.support.v4.app.DialogFragment;

/**
 * @author Maor
 *
 */
public class DialogCallback {

	

	protected void onDialogButtonPressed(int buttonID, DialogFragment dialog){
		
	};
	
	protected void onDialogButtonPressed(int buttonID, DialogFragment dialog, Object Extra){
		
	};
	
    protected void onDialogOptionPressed(int which, DialogFragment dialog){
		
	};
	
    protected void onDismiss(DialogFragment dialog,Object Extra){
		
	};
	
}
