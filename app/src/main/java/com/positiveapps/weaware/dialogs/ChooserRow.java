/**
 * 
 */
package com.positiveapps.weaware.dialogs;

/**
 * @author natiapplications
 *
 */
public class ChooserRow {
	public int icon;
	public String desc;
	public ChooserRow(int iconRes,String desc){
		this.icon = iconRes;
		this.desc = desc;
	}
}
