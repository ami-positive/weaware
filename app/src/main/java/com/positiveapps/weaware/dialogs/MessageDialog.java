package com.positiveapps.weaware.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.positiveapps.weaware.R;

/**
 * Created by Izakos on 19/10/2015.
 */
public class MessageDialog extends BaseDialogFragment implements View.OnClickListener {

    public static final String DIALOG_NAME = "MessageDialog";


    private Fragment parent;


    private TextView subTitle;
    private Button dissmisBtn;
    private String titleString;

    private DialogCallback callback;

    /**
     *
     * @param callback
     */
    public MessageDialog(Fragment parent,String title,DialogCallback callback){
        //this.mDescription = description;
        this.callback = callback;
        this.parent = parent;
        this.titleString = title;
    }

    public MessageDialog(String title,DialogCallback callback){
        //this.mDescription = description;
        this.callback = callback;
        this.titleString = title;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_error, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        subTitle = (TextView)view.findViewById(R.id.sub_title);
        dissmisBtn = (Button)view.findViewById(R.id.btn_dissmis);
        subTitle.setText(titleString);

        dissmisBtn.setOnClickListener(this);

        return view;
    }




    private void dismissDialog(){

        this.dismiss();
    }



    @Override
    public void onClick(View v) {

        dismissDialog();

        if(callback != null){
            callback.onDialogButtonPressed(v.getId(), this);
        }
    }





}

