/**
 * 
 */
package com.positiveapps.weaware.dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.positiveapps.weaware.fragments.ProfileFragment;


/**
 * @author Maor
 *
 */
public class DialogManager {

	
	public static void showDialogChooser(FragmentManager fm,String title,ChooserRow[] optionsRows,
			View footerView,DialogCallback callback){
		
		CooserDialog dialog = new CooserDialog(title,optionsRows,footerView,callback);
		dialog.show(fm, CooserDialog.DIALOG_NAME);
	}

	public static void showDialogChooser(Fragment parentFragment, String[] optionsText,
										 DialogCallback callback){

		LoadImageDialog dialog = new LoadImageDialog(parentFragment,optionsText,callback);
		dialog.show(parentFragment.getFragmentManager(), LoadImageDialog.DIALOG_NAME);
	}

	public static void showDialogChooser(FragmentManager fragment, String[] optionsText,
										 DialogCallback callback){

		LoadImageDialog dialog = new LoadImageDialog(optionsText,callback);
		dialog.show(fragment, LoadImageDialog.DIALOG_NAME);
	}
	
	public static void showAppDialog(FragmentManager fm ,String title,
			String message,int iconBg, int icon,AppDialogButton[] buttons,boolean cancelable,
			DialogCallback callback){
		
		AppDialog dialog = new AppDialog(title, message, iconBg, icon, buttons, callback);
		dialog.setCancelable(cancelable);
		dialog.show(fm, AppDialog.DIALOG_NAME);
	}
	

	public static void showErrorDialog(FragmentManager fm ,String desc,
			DialogCallback callback){
		
		ErrorDialog dialog = new ErrorDialog(desc,callback);
		
		dialog.show(fm, ErrorDialog.DIALOG_NAME);
	}

	public static void showMessageDialog(FragmentManager fm ,String desc,
									   DialogCallback callback){

		MessageDialog dialog = new MessageDialog(desc,callback);

		dialog.show(fm, MessageDialog.DIALOG_NAME);
	}





}
