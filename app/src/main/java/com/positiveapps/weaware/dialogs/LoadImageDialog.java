package com.positiveapps.weaware.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.util.AppUtil;

/**
 * Created by Izakos on 08/10/2015.
 */
public class LoadImageDialog extends BaseDialogFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    public static final String DIALOG_NAME = "dialog_load_image";


    private Fragment parent;
    private DialogCallback callback;

    private View dialogView;
    private Button cancelBtn;
    private ListView options;
    private String[] optionsText;



    public LoadImageDialog() {
        super();
        // TODO Auto-generated constructor stub
    }

    public LoadImageDialog(Fragment parent,String[] optionsText,DialogCallback callback){
        //this.mDescription = description;
        this.callback = callback;
        this.parent = parent;
        this.optionsText = optionsText;
    }

    public LoadImageDialog(String[] optionsText,DialogCallback callback){
        //this.mDescription = description;
        this.callback = callback;
        this.optionsText = optionsText;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_load_image, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        cancelBtn = (Button)view.findViewById(R.id.cancel_btn);
        options = (ListView)view.findViewById(R.id.option_list);

        cancelBtn.setOnClickListener(this);



        options.setAdapter(new optionsAdapter(getActivity(), 0, 0, optionsText));
        options.setOnItemClickListener(this);

        WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;

        getDialog().getWindow().getAttributes().windowAnimations = R.style.CustomAlertDialogStyle;
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.anim_in_dialog);

        view.startAnimation(bottomUp);
        this.dialogView = view;
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        }
    }

    private void dismissDialog(){

        this.dismiss();
    }



    @Override
    public void onClick(View v) {

        dismissDialog();

        if(callback != null){
            callback.onDialogButtonPressed(v.getId(), this);
        }
    }



    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        dismissDialog();
        if (callback != null){
            callback.onDialogOptionPressed(arg2, this);
        }

    }

    class optionsAdapter extends ArrayAdapter<String> {

        String[] options;
        /**
         * @param context
         * @param resource
         * @param textViewResourceId
         * @param objects
         */


        public optionsAdapter(Context context, int resource,
                              int textViewResourceId, String[] objects) {

            super(context, resource, textViewResourceId, objects);

            this.options = objects;


        }





        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getActivity().getLayoutInflater().inflate(R.layout.item_dialog_load_image, null);
            AppUtil.setTextFonts(getActivity(), convertView);
            ((TextView)convertView.findViewById(R.id.text_item_list_chooser))
                    .setBackgroundColor(position == 0 ? getResources().getColor(R.color.material_blue_grey_800) : getResources().getColor(R.color.material_blue_grey_900));
            ((TextView)convertView.findViewById(R.id.text_item_list_chooser)).setText(options[position]);

            return convertView;
        }

    }




}
