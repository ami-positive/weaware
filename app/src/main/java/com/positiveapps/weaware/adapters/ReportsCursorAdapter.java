package com.positiveapps.weaware.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.main.ReportMapActivity;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.closeRequest;
import com.positiveapps.weaware.network.requests.confirmRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.closeResponse;
import com.positiveapps.weaware.network.response.confirmResponse;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.ui.CircleImageView;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.Static;
import com.positiveapps.weaware.util.ToastUtil;

/**
 * Created by Izakos on 11/10/2015.
 */
public class ReportsCursorAdapter  extends CursorAdapter {

    Activity activity;

    public ReportsCursorAdapter(Context context, Cursor c, boolean autoRequery, Activity activity) {
        super(context, c, autoRequery);
        this.activity = activity;
    }

    public ReportsCursorAdapter(Context context, Cursor c, int flags, Activity activity) {
        super(context, c, flags);
        this.activity = activity;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater vi = LayoutInflater.from(context);
        View v = vi.inflate(R.layout.item_list_report, parent, false);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Report report = MyApp.tableReports.cursorToEntity(cursor);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView time = (TextView) view.findViewById(R.id.time);
        TextView desc = (TextView) view.findViewById(R.id.desc);
        LinearLayout confirm = (LinearLayout) view.findViewById(R.id.LLconfirm);
        LinearLayout confirmed = (LinearLayout) view.findViewById(R.id.LLconfirmed);
        LinearLayout cancel = (LinearLayout) view.findViewById(R.id.LLcancel);
        ImageView reportImage = (ImageView) view.findViewById(R.id.reportImage);
        CircleImageView contactImage = (CircleImageView) view.findViewById(R.id.contactImage);

        setConfirmationClickListener(report.getServerId(), report.getServerId(), cancel, confirm, confirmed);
        view.setOnClickListener(reportClickListener(report));

        if(report.getServerId() != Report.getReportID()){
            cancel.setVisibility(View.GONE);
            if(report.getConfirm() == 1) {
                changeLayouts(1, confirm, confirmed); //set confirmed button
            }else{
                changeLayouts(0, confirm, confirmed);//set confirmed button
            }
        }



        if(!report.getDescription().isEmpty()) {
            desc.setText(report.getDescription());
        }

        name.setText(report.getReporterName());
        time.setText(report.getDateEndCustom());

        ImageLoader.ByUrl(report.getReporterImage(), contactImage, R.drawable.friends_icon_map);
        ImageLoader.ByUrl(report.getPhotoAttached(), reportImage, R.drawable.app_icon);
    }

    private View.OnClickListener reportClickListener(final Report report){
        View.OnClickListener onReportClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ReportMapActivity.class);
                intent.putExtra(Static.REPORT_ID, report.getDatabaseId());
                activity.startActivity(intent);
                activity.finish();
            }
        };
        return onReportClick;
    }


    public void setConfirmationClickListener(final long serverId, long dbId, final View...views){
        // constant

        changeLayouts(0, views); // init
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == views[0].getId()){  // cancel
                    cancelReport();
                    return;
                }else if(view.getId() == views[1].getId()){ // confirm
                    confirmReport(serverId, views[1], views[2]);
                    return;
                }else if(view.getId() == views[2].getId()){ // confirmed
                    ToastUtil.toster("You already confirmed the report", false);
                    return;
                }
            }
        };

        for (int i = 0; i < views.length; i++) {
            views[i].setOnClickListener(listener);
        }
    }

    // first set the mode(start with zero) and then the participate layout sorted
    private void changeLayouts(int mode, View...view){
        for (int i = 0; i < view.length; i++) {
            if(i == mode){
                view[i].animate().alpha(1f).translationX(0f);
            }else{
                view[i].animate().alpha(0f).translationX(view[i].getWidth());
            }
        }
    }

    public void cancelReport(){
        MyApp.networkManager.makeRequest(new closeRequest(String.valueOf(Report.getReportID())/*ID*/), new NetworkCallback<closeResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<closeResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    Report report = Report.getCurrentReport();
                    MyApp.tableReports.delete(report.getDatabaseId(), report);
                    Report.setMode(Static.NORMAL_MODE);
                    Report.setReportID(0);
                    ToastUtil.toster("report canceled", true);
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }
    
    public void confirmReport(final long reportId, final View confirm, final View confirmed){
        MyApp.networkManager.makeRequest(new confirmRequest(String.valueOf(reportId)/*ID*/), new NetworkCallback<confirmResponse>() {
                    @Override
                    public void onResponse(boolean success, String errorDesc, ResponseObject<confirmResponse> response) {
                        super.onResponse(success, errorDesc, response);
                        if (success) {
                            changeLayouts(1, confirm, confirmed);
                            Report report = Report.getReportById(reportId);
                            report.setConfirm(1);
                            MyApp.tableReports.insertOrUpdate(report);
                            ToastUtil.toster("Report confirmed", true);
                        } else {
                            ToastUtil.toster(errorDesc, true);
                        }
                    }
                });
    }
}