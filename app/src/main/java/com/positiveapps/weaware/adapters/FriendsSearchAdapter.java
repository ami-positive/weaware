package com.positiveapps.weaware.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.addFriendRequest;
import com.positiveapps.weaware.network.requests.unfriendRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.addFriendResponse;
import com.positiveapps.weaware.network.response.unfriendResponse;
import com.positiveapps.weaware.objects.Friend;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.ui.CircleImageView;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.ToastUtil;

import java.util.ArrayList;

/**
 * Created by Izakos on 29/11/2015.
 */
public class FriendsSearchAdapter extends ArrayAdapter<Friend> {

    Context context;
    int layoutResourceId;
    ArrayList<Friend> friends;

    public FriendsSearchAdapter(Context context, int layoutResourceId, ArrayList<Friend> friends) {
        super(context, layoutResourceId, friends);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.friends = friends;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        viewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new viewHolder();
            holder.contactImage = (CircleImageView) row.findViewById(R.id.contactImage);
            holder.name = (TextView) row.findViewById(R.id.name);
            holder.removeFriend = (Button) row.findViewById(R.id.removeFriend);
            holder.addFriend = (Button) row.findViewById(R.id.addFriend);

            row.setTag(holder);
        } else {
            holder = (viewHolder) row.getTag();
        }

        Friend friend = getItem(position);

        if (friend != null) {

            if (friend.getIsFriend().matches("1")) {
                holder.removeFriend.setVisibility(View.VISIBLE);
                holder.addFriend.setVisibility(View.GONE);
            } else {
                holder.removeFriend.setVisibility(View.GONE);
                holder.addFriend.setVisibility(View.VISIBLE);
            }

            holder.addFriend.setOnClickListener(addFriendship(friend.getId()));
            holder.removeFriend.setOnClickListener(cacnelFriendship(friend.getId()));

            holder.name.setText(friend.getFullName());
            ImageLoader.ByUrl(friend.getAvatar(), holder.contactImage, R.drawable.avatar);

        }
        return row;
    }

    static class viewHolder {
        CircleImageView contactImage;
        TextView name;
        Button removeFriend;
        Button addFriend;
    }

    private View.OnClickListener cacnelFriendship(final long friendId) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApp.networkManager.makeRequest(new unfriendRequest(String.valueOf(friendId)/*FriendID*/), new NetworkCallback<unfriendResponse>() {
                            @Override
                            public void onResponse(boolean success, String errorDesc, ResponseObject<unfriendResponse> response) {
                                super.onResponse(success, errorDesc, response);
                                if (success) {
                                    removeFriend(friendId);
                                    ToastUtil.toster("Friend Removed", true);
                                } else {
                                    ToastUtil.toster(errorDesc, true);
                                }
                            }
                        });

            }
        };
        return listener;
    }

    private View.OnClickListener addFriendship(final long friendId) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApp.networkManager.makeRequest(new addFriendRequest(String.valueOf(friendId)/*FriendID*/), new NetworkCallback<addFriendResponse>() {
                    @Override
                    public void onResponse(boolean success, String errorDesc, ResponseObject<addFriendResponse> response) {
                        super.onResponse(success, errorDesc, response);
                        if (success) {
                            addFriend(friendId);
                            ToastUtil.toster("Friend Added", true);
                        } else {
                            ToastUtil.toster(errorDesc, true);
                        }
                    }
                });

            }
        };
        return listener;
    }

    public void removeFriend(long friendId){
        for (int i = 0; i < friends.size() ; i++) {
             if(friends.get(i).getId() == friendId){
                 friends.get(i).setIsFriend("0");
                 notifyDataSetChanged();
                 Person.getFriends(null);
                 return;
             }
        }
    }

    public void addFriend(long friendId){
        for (int i = 0; i < friends.size() ; i++) {
            if (friends.get(i).getId() == friendId) {
                friends.get(i).setIsFriend("1");
                notifyDataSetChanged();
                Person.getFriends(null);
                return;
            }
        }
    }

}
