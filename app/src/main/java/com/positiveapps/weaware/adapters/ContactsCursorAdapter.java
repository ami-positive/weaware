package com.positiveapps.weaware.adapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.unfriendRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.unfriendResponse;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.ui.CircleImageView;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.ToastUtil;

/**
 * Created by Izakos on 04/10/2015.
 */
public class ContactsCursorAdapter extends CursorAdapter {

    private Activity activity;

    public ContactsCursorAdapter(Activity activity, Cursor c, boolean autoRequery) {
        super(activity, c, autoRequery);
        this.activity = activity;
    }

    public ContactsCursorAdapter(Activity activity, Cursor c, int flags) {
        super(activity, c, flags);
        this.activity = activity;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater vi = LayoutInflater.from(context);
        View v = vi.inflate(R.layout.item_contact, parent, false);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Person person = MyApp.tablePersons.cursorToEntity(cursor);

        CircleImageView contactImage = (CircleImageView) view.findViewById(R.id.contactImage);
        TextView name = (TextView) view.findViewById(R.id.name);
        Button addFriend = (Button) view.findViewById(R.id.addFriend);
        Button removeFriend = (Button) view.findViewById(R.id.removeFriend);
        Button confirmFriend = (Button) view.findViewById(R.id.confirmFriend);
        addFriend.setVisibility(View.GONE);

        name.setText(person.getUserName());
        ImageLoader.ByUrl(person.getProfileImage(), contactImage, R.drawable.avatar);
        System.out.println("AS " + person.getStatus());
        if(person.getStatus().matches("waiting")){
            confirmFriend.setVisibility(View.VISIBLE);
        }else{
            confirmFriend.setVisibility(View.GONE);
        }

        confirmFriend.setOnClickListener(confirmFriendship(confirmFriend, person.getServerId()));
        removeFriend.setOnClickListener(cacnelFriendship(person.getServerId()));

    }


    private View.OnClickListener cacnelFriendship(final long friendId) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCancelDialog(friendId);
            }
        };
        return listener;
    }

    private View.OnClickListener confirmFriendship(final TextView textView, final long friendId) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Person.approveFriends(textView, friendId);
            }
        };
        return listener;
    }

    public void showCancelDialog(final long friendId) {

        if (activity.isFinishing()) {return;}

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(activity)
                        .title(R.string.cancel_report)
                        .content(R.string.are_you_sure_you_want_to_remove_friend)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                MyApp.networkManager.makeRequest(new unfriendRequest(String.valueOf(friendId)/*FriendID*/), new NetworkCallback<unfriendResponse>() {
                                    @Override
                                    public void onResponse(boolean success, String errorDesc, ResponseObject<unfriendResponse> response) {
                                        super.onResponse(success, errorDesc, response);
                                        if (success) {
                                            ToastUtil.toster("Friend Removed", true);
                                            Person.getFriends(null);
                                        } else {
                                            ToastUtil.toster(errorDesc, true);
                                        }
                                    }
                                });
                            }
                        })
                        .show();
            }
        });
    }

}

