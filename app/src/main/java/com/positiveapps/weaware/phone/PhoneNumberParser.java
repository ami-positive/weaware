package com.positiveapps.weaware.phone;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.positiveapps.weaware.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.TreeSet;


/**
 * @author Andrey Tretiakov 06.02.2015.
 */
@SuppressWarnings("unchecked")
public class PhoneNumberParser {
    private static final String TAG = PhoneNumberParser.class.getSimpleName();

    protected final TreeSet<String> CANADA_CODES = new TreeSet();
    {
        CANADA_CODES.add("204");
        CANADA_CODES.add("236");
        CANADA_CODES.add("249");
        CANADA_CODES.add("250");
        CANADA_CODES.add("289");
        CANADA_CODES.add("306");
        CANADA_CODES.add("343");
        CANADA_CODES.add("365");
        CANADA_CODES.add("387");
        CANADA_CODES.add("403");
        CANADA_CODES.add("416");
        CANADA_CODES.add("418");
        CANADA_CODES.add("431");
        CANADA_CODES.add("437");
        CANADA_CODES.add("438");
        CANADA_CODES.add("450");
        CANADA_CODES.add("506");
        CANADA_CODES.add("514");
        CANADA_CODES.add("519");
        CANADA_CODES.add("548");
        CANADA_CODES.add("579");
        CANADA_CODES.add("581");
        CANADA_CODES.add("587");
        CANADA_CODES.add("604");
        CANADA_CODES.add("613");
        CANADA_CODES.add("639");
        CANADA_CODES.add("647");
        CANADA_CODES.add("672");
        CANADA_CODES.add("705");
        CANADA_CODES.add("709");
        CANADA_CODES.add("742");
        CANADA_CODES.add("778");
        CANADA_CODES.add("780");
        CANADA_CODES.add("782");
        CANADA_CODES.add("807");
        CANADA_CODES.add("819");
        CANADA_CODES.add("825");
        CANADA_CODES.add("867");
        CANADA_CODES.add("873");
        CANADA_CODES.add("902");
        CANADA_CODES.add("905");
    }

    private Activity context;

    private Spinner countries;
    private TextView countryCode;
    private EditText phoneInput;
    private String editPhone;

    private SparseArray<ArrayList<Country>> countriesMap = new SparseArray<>();

    private PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    private CountryAdapter adapter;

    public PhoneNumberParser(Activity context, Spinner countries, TextView countryCode, EditText currentPhoneInput) {
        this.context = context;
        this.countries = countries;
        this.countryCode = countryCode;
        this.phoneInput = currentPhoneInput;
        this.editPhone = currentPhoneInput.getText().toString();

        adapter = new CountryAdapter(context);

        this.countries.setOnItemSelectedListener(onItemSelectedListener);
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                PhoneNumberParser.this.countries.setAdapter(adapter);
                phoneInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE)
                            attemptLogin();

                        return false;
                    }

                });
            }
        });

        initCodes();
    }

    protected void initCodes() {
        int mSpinnerPosition = -1;
        ArrayList<Country> data = new ArrayList(233);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open("countries.dat"), "UTF-8"));

            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                Country c = new Country(line, i);
                data.add(c);
                ArrayList<Country> list = countriesMap.get(c.countryCode);
                if (list == null) {
                    list = new ArrayList();
                    countriesMap.put(c.countryCode, list);
                }
                list.add(c);
                i++;
            }
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                }
            }
        }
        String countryRegion = PhoneUtils.getCountryRegionFromPhone(context, countryCode.getText() + phoneInput.getText().toString());
        int code = phoneNumberUtil.getCountryCodeForRegion(countryRegion);
        ArrayList<Country> list = countriesMap.get(code);
        if (list != null) {
            for (Country c : list) {
                if (c.priority == 0) {
                    mSpinnerPosition = c.num;
                    break;
                }
            }
        }
        setData(mSpinnerPosition, data);
    }

    private void setData(final int mSpinnerPosition, final ArrayList<Country> data) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                adapter.addAll(data);
                countries.setSelection(mSpinnerPosition > 0 ? mSpinnerPosition : -1);
            }

        });
    }

    protected String[] validate() {
        String region = null;
        String phone = null;
        try {
            Phonenumber.PhoneNumber p = phoneNumberUtil.parse(countryCode.getText() + phoneInput.getText().toString(), null);
            phone = String.valueOf(p.getCountryCode()) + p.getNationalNumber();
            region = phoneNumberUtil.getRegionCodeForNumber(p);
        } catch (NumberParseException ignore) {}

        if (region != null) {
            return new String[] {phone, region};
        } else {
            return null;
        }
    }

    protected OnPhoneChangedListener onPhoneChangedListener = new OnPhoneChangedListener() {

        @Override
        public void onPhoneChanged(String phone) {
            try {
                editPhone = phone;
                Phonenumber.PhoneNumber p = phoneNumberUtil.parse(phone, null);
                ArrayList<Country> list = countriesMap.get(p.getCountryCode());
                Country country = null;

                if (list != null) {
                    if (p.getCountryCode() == 1) {
                        String num = String.valueOf(p.getNationalNumber());
                        if (num.length() >= 1) {
                            String code = num.substring(0, 1);
                            if (CANADA_CODES.contains(code)) {
                                for (Country c : list) {
                                    if (c.priority == 1) {
                                        country = c;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (country == null) {
                        for (Country c : list) {
                            if (c.priority == 0) {
                                country = c;
                                break;
                            }
                        }
                    }
                }

                if (country != null) {
                    final int position = country.num;
                    countries.post(new Runnable() {

                        @Override
                        public void run() {
                            countries.setSelection(position);
                        }

                    });
                }
            } catch (NumberParseException ignore) {}
        }
    };

    private final AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Country c = (Country) countries.getItemAtPosition(position);
            if (editPhone != null && phoneInput.getText().toString().startsWith(c.countryCodeStr))
                return;

            phoneInput.getText().clear();
            countryCode.setText("+" + String.valueOf(c.countryCode));
            editPhone = null;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    };

    public boolean attemptLogin() {
        String[] phone = validate();

        if (phone == null) {
            customBlinkInput();
            return false;
        }


        if (phone[0] == null) {
            customBlinkInput();
            return false;
        }

        if (phone[0].length() < 3) {
            customBlinkInput();
            return false;
        }

        if (getParsedPhone(phone[0], phone[1]) != null && phoneNumberUtil.isValidNumber(getParsedPhone(phone[0], phone[1]))) {
            return true;
        } else {
            phoneInput.requestFocus();
            customBlinkInput();
        }

        return false;
    }



    private Phonenumber.PhoneNumber getParsedPhone(String phone, String region) {
        try {
            return phoneNumberUtil.parse(phone, region);
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void customBlinkInput() {
        new AsyncTask<Void, Integer, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                int count = 8;
                while (count != 0) {
                    try {
                        publishProgress(Color.RED);
                        Thread.sleep(35);
                        publishProgress(Color.BLACK);
                        Thread.sleep(35);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    count--;
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... color) {
                super.onProgressUpdate(color);
                phoneInput.setTextColor(color[0]);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                phoneInput.setTextColor(context.getResources().getColor(R.color.blue));
            }
        }.execute();
    }

    public String getCountryCode() {
        try {
            Phonenumber.PhoneNumber p = phoneNumberUtil.parse(countryCode.getText() + phoneInput.getText().toString(), null);
            return phoneNumberUtil.getRegionCodeForNumber(p);
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
