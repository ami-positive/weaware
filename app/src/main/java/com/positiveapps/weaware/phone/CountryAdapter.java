/*
 * Copyright (c) 2014 Amberfog.
 *
 * This source code is Amberfog Confidential Proprietary
 * This software is protected by copyright. All rights and titles are reserved.
 * You shall not use, copy, distribute, modify, decompile, disassemble or reverse
 * engineer the software. Otherwise this violation would be treated by law and
 * would be subject to legal prosecution. Legal use of the software provides
 * receipt of a license from the right holder only.
 */

package com.positiveapps.weaware.phone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.positiveapps.weaware.R;


public class CountryAdapter extends ArrayAdapter<Country> {

    private LayoutInflater layoutInflater;

    public CountryAdapter(Context context) {
        super(context, 0);
        layoutInflater = LayoutInflater.from(context);
    }

    private static class Holder {
        TextView name;

        Holder(View v) {
            name = (TextView) v.findViewById(R.id.country_name);
        }
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        Country country = getItem(position);
        Holder h;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_country_drop, parent, false);
            h = new Holder(convertView);

            convertView.setTag(h);
        } else {
            h = (Holder) convertView.getTag();
        }

        if (country != null)
            h.name.setText(country.name);

        return convertView;
    }

    @SuppressLint("InflateParams")
    public View getView(int position, View convertView, ViewGroup parent) {
        Country country = getItem(position);

        if (convertView == null)
            convertView = layoutInflater.inflate(R.layout.spinner_item_country, null);

        ((TextView)convertView.findViewById(R.id.country_name)).setText(country.name);

        return convertView;
    }
}
