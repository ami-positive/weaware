package com.positiveapps.weaware.phone;

import android.util.Patterns;

import java.util.regex.Pattern;

/**
 * @author Andrey Tretiakov 16.02.2015.
 */
public class Validator {

    public static boolean validPhone(String phone) {
        Pattern pattern = Patterns.PHONE;
        return pattern.matcher(phone).matches();
    }

    public static boolean israelPhoneValid(String phone) {
        Pattern pattern = Pattern.compile("/^972\\d([\\d]{0,1})([-]{0,1})\\d{7}$/");
        return pattern.matcher(phone).matches();
    }

    public static boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
