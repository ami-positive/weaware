package com.positiveapps.weaware.phone;

public class Country {

    public String name;
    public String countryISO;
    public int countryCode;
    public String countryCodeStr;
    public int priority;
    public int num;

    public Country(String str, int num) {
        String[] data = str.split(",");
        this.num = num;
        name = data[0];
        countryISO = data[1];
        countryCode = Integer.parseInt(data[2]);
        countryCodeStr = "+" + data[2];
        if (data.length > 3) {
            priority = Integer.parseInt(data[3]);
        }
    }
}
