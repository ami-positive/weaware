package com.positiveapps.weaware.phone;

public interface OnPhoneChangedListener {
    void onPhoneChanged(String phone);
}
