package com.positiveapps.weaware.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.util.FragmentsUtil;
import com.positiveapps.weaware.util.Static;

/**
 * Created by Izakos on 11/10/2015.
 */
public class UniversityFragment extends  BaseFragment{

    private String url = "";

    public UniversityFragment() {
        // TODO Auto-generated constructor stub
    }

    public static UniversityFragment newInstance(String url) {
        UniversityFragment instance = new UniversityFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Static.URL, url);
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_webview;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WebView webview = (WebView) view.findViewById(R.id.universityWebView);
        final ProgressBar webviewProgressBar = (ProgressBar) view.findViewById(R.id.webviewProgressBar);

        if(getArguments() != null){
           url = (String) getArguments().get(Static.URL);
        }



        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                handleProgressBar(webviewProgressBar,progress );
            }



        });
        webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        webview.loadUrl(url);

    }

    public static void open(FragmentManager fm, String url){
        FragmentsUtil.openFragment(fm, newInstance(url), R.id.main_container);
    }

    private void handleProgressBar(ProgressBar bar, int progress){
        bar.setProgress(progress);
        if(progress == 100){
            bar.setVisibility(View.GONE);
        }
    }
}
