package com.positiveapps.weaware.fragments.sidescreen;

/**
 * Created by Izakos on 15/10/2015.
 */
public interface OnDrawerClose {
    public void whenDrawerClosed();
}
