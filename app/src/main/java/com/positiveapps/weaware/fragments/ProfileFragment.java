package com.positiveapps.weaware.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.dialogs.DialogCallback;
import com.positiveapps.weaware.dialogs.DialogManager;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.saveRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.saveResponse;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.ui.CircleImageView;
import com.positiveapps.weaware.util.BitmapUtil;
import com.positiveapps.weaware.util.FragmentsUtil;
import com.positiveapps.weaware.util.Helper;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.Static;
import com.positiveapps.weaware.util.ToastUtil;

import java.io.File;

/**
 * Created by Izakos on 08/10/2015.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    private Button profileSaveBT;
    private Button profileFriendsBT;
    private CircleImageView profileImageCIV;
    private Switch profilePolicemanSW;
    private TextView profilePolicemanTX;
    private EditText profileCityEditET;
    private EditText profileNameEditET;
    private String imageBase64String = "";
    private EditText profilePhoneEditET;


    public ProfileFragment(){
        // TODO Auto-generated constructor stub
    }

    public static ProfileFragment newInstance() {
        ProfileFragment instance = new ProfileFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_profile;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profileSaveBT = (Button) view.findViewById(R.id.profileSaveBT);
        profileFriendsBT = (Button) view.findViewById(R.id.profileFriendsBT);
        profileImageCIV = (CircleImageView) view.findViewById(R.id.profileImageCIV);
        profilePolicemanSW = (Switch) view.findViewById(R.id.profilePolicemanSW);
        profilePolicemanTX = (TextView) view.findViewById(R.id.profilePolicemanTX);
        profileNameEditET = (EditText) view.findViewById(R.id.profileNameEditET);
        profilePhoneEditET = (EditText) view.findViewById(R.id.profilePhoneEditET);
        profileCityEditET = (EditText) view.findViewById(R.id.profileCityEditET);

        profileSaveBT.setOnClickListener(this);
        profileFriendsBT.setOnClickListener(this);
        profileImageCIV.setOnClickListener(this);

        initUI();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap image = null;
        if(resultCode != -1){
            return;
        }

        switch (requestCode) {
            case Static.GALLARY_REQUEST_CODE:
                image = ImageLoader.byUri(ImageLoader.IMAGE_TYPE_FROM_GALLERAY, data.getData(), profileImageCIV);

                break;
            case Static.CAMERA_REQUEST_CODE:
                File file = new File(Environment.getExternalStorageDirectory().getPath(), Static.CAMERA_PROFILE_PATH);
                image = BitmapUtil.loadImageIntoByStringPath(file.getAbsolutePath(), profileImageCIV, 300, 2, ImageLoader.DEAFULT_PLACE_HOLDER);
                break;
            default:
                break;
        }
        imageBase64String = Helper.getProfileImageAsBase64(image);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.profileSaveBT:
                User.setFullName(profileNameEditET);
                User.setCategory(profilePolicemanSW);
                User.setCity(profileCityEditET);
                User.setPhone(profilePhoneEditET);
                User.setProfileImageB64(imageBase64String);
                saveProfileQuery();

                break;
            case R.id.profileFriendsBT:
                FragmentsUtil.openFragment(getFragmentManager(), ContactsFragment.newInstance(), R.id.main_container);
                break;
            case R.id.profileImageCIV:
                openDialogeWhenClickOnProfileImage();
                break;
        }
    }

    private void saveProfileQuery() {
        showProgressDialog(getString(R.string.saving_profile));
        MyApp.networkManager.makeRequest(new saveRequest(
                User.getFullName()/*FullName*/,
                User.getCity()/*City*/,
                ""/*Email*/,
                User.getPhone()/*Phone*/,
                User.getProfileImageB64()/*Avatar*/,
                User.isPolicemanInt()/*IsPoliceman*/), new NetworkCallback<saveResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<saveResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ToastUtil.toster(getString(R.string.profile_saved), true);
                    getFragmentManager().popBackStack();
                    dismisProgressDialog();
                } else {
                    ToastUtil.toster(errorDesc, true);
                    dismisProgressDialog();
                }
            }
                });

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    private void openDialogeWhenClickOnProfileImage() {
        final int CAMERA = 0;
        final int GALLERY = 1;
        String[] options = {getString(R.string.camera),getString(R.string.gallery)};
        DialogManager.showDialogChooser(this, options, new DialogCallback() {
            protected void onDialogOptionPressed(int which, android.support.v4.app.DialogFragment dialog) {
                switch (which) {
                    case CAMERA:
                        Helper.openCamera(ProfileFragment.this);
                        break;
                    case GALLERY:
                        Helper.openGallery(ProfileFragment.this);
                        break;
                }
            }

            ;
        });
    }

    private void initUI(){
        profileNameEditET.setText(User.getFullName());
        profileCityEditET.setText(User.getCity());
        profilePhoneEditET.setText(User.getPhone());
        profilePolicemanSW.setChecked(User.isPoliceman());
        User.setImage(profileImageCIV);

    }

}
