//package com.positiveapps.weaware.fragments.login;
//
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.EditorInfo;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//
//import com.digits.sdk.android.DigitsAuthButton;
//import com.positiveapps.weaware.R;
//import com.positiveapps.weaware.fragments.BaseFragment;
//import com.positiveapps.weaware.main.LoginActivity;
//import com.positiveapps.weaware.main.MyApp;
//import com.positiveapps.weaware.network.twitter.MyTwitterApiClient;
//import com.positiveapps.weaware.objects.User;
//import com.positiveapps.weaware.util.AppUtil;
//import com.positiveapps.weaware.util.FragmentsUtil;
//import com.positiveapps.weaware.util.Helper;
//import com.positiveapps.weaware.util.ImageLoader;
//import com.positiveapps.weaware.util.ToastUtil;
//import com.twitter.sdk.android.core.Callback;
//import com.twitter.sdk.android.core.Result;
//import com.twitter.sdk.android.core.TwitterException;
//import com.twitter.sdk.android.core.TwitterSession;
//import com.twitter.sdk.android.core.identity.TwitterAuthClient;
//import com.twitter.sdk.android.core.identity.TwitterLoginButton;
//
//
//import roboguice.inject.InjectView;
//
///**
// * Created by Izakos on 08/10/2015.
// */
//public class LoginFragment extends BaseFragment implements View.OnClickListener , TextView.OnEditorActionListener{
//
//    private Button loginSignUpBT;
//    private Button loginForgetPassBT;
//    private Button loginFacebookBT;
//    private Button loginTweeterBT;
//    private Button RegistrationBT;
//    private EditText loginUserET;
//    private EditText loginPassET;
////
//    private TextView check;
////
//    private TwitterLoginButton twitterLoginButton;
//
//    public LoginFragment() {
//        // TODO Auto-generated constructor stub
//    }
//
//
//    public static LoginFragment newInstance() {
//        LoginFragment instance = new LoginFragment();
//        return instance;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        int layoutRes = R.layout.fragment_login;
//        View rootView = inflater.inflate(layoutRes, container, false);
//        return rootView;
//    }
//
//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        twitterLoginButton = (TwitterLoginButton)view.findViewById(R.id.tw_login_button);
//        loginSignUpBT = (Button) view.findViewById(R.id.loginSignUpBT);
//        RegistrationBT = (Button) view.findViewById(R.id.RegistrationBT);
//        loginForgetPassBT = (Button) view.findViewById(R.id.loginForgetPassBT);
//        loginFacebookBT = (Button) view.findViewById(R.id.loginFacebookBT);
//        loginTweeterBT = (Button) view.findViewById(R.id.loginTweeterBT);
//        loginUserET = (EditText) view.findViewById(R.id.loginUserET);
//        loginPassET = (EditText) view.findViewById(R.id.loginPassET);
//
//        //
//        check = (TextView) view.findViewById(R.id.check);
//        //
//
//        RegistrationBT.setOnClickListener(this);
//        loginSignUpBT.setOnClickListener(this);
//        loginForgetPassBT.setOnClickListener(this);
//        loginFacebookBT.setOnClickListener(this);
//        loginTweeterBT.setOnClickListener(this);
//        loginPassET.setOnEditorActionListener(this);
//
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        switch (User.currentLoginType){
//            case User.LOGIN_TYPE_TWITTER:
//                twitterLoginButton.onActivityResult(requestCode, resultCode, data);
//                break;
//        }
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.RegistrationBT:
//                goToRegistrationFragment();
//                break;
//            case R.id.loginSignUpBT:
//                login(loginUserET, loginPassET, getFragmentManager());
//                check.setText(User.printProfile());
//            break;
//            case R.id.loginForgetPassBT:
//                User.forgotPassword(getFragmentManager());
//                break;
//            case R.id.loginFacebookBT:
//                User.logToFacebook(LoginActivity.mSimpleFacebook);
//                break;
//            case R.id.loginTweeterBT:
//                User.logToTwitter(twitterLoginButton);
//                break;
//        }
//    }
//
//    @Override
//    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//        if (i == EditorInfo.IME_ACTION_NEXT) {
//            login(loginUserET, loginPassET, getFragmentManager());
//            return true;
//        }
//        return false;
//    }
//
//
//    public void goToRegistrationFragment(){
//            FragmentsUtil.openFragment(getFragmentManager(), RegistrationFragment.newInstance(), R.id.main_login_container);
//    }
//
//
//}
