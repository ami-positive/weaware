package com.positiveapps.weaware.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.getImageRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.getImageResponse;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.ToastUtil;
import com.squareup.picasso.Picasso;

/**
 * Created by Izakos on 04/12/2015.
 */
public class ScreenSlidePageFragment extends BaseFragment {

    private static final String IMAGE_URL = "imageUrl";

    public ScreenSlidePageFragment(){
        // TODO Auto-generated constructor stub
    }

    public static ScreenSlidePageFragment newInstance(int imageUrl) {
        ScreenSlidePageFragment instance = new ScreenSlidePageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(IMAGE_URL, imageUrl);
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView imageView = (ImageView) view.findViewById(R.id.galleryPic);
        if(getArguments() != null){
            showImageQuery(getArguments().getInt(IMAGE_URL), imageView);
        }
    }

    private void showImageQuery(int ImageId, final ImageView imageView){
        showProgressDialog(getString(R.string.loading_image));
        MyApp.networkManager.makeRequest(new getImageRequest(String.valueOf(ImageId)), new NetworkCallback<getImageResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<getImageResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    Picasso.with(getActivity()).load(response.getData().getImages().getUrl()).error(ImageLoader.DEAFULT_PLACE_HOLDER)
                            .placeholder(ImageLoader.DEAFULT_PLACE_HOLDER).into(imageView);
                    dismisProgressDialog();
                } else {
                    dismisProgressDialog();
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }
}
