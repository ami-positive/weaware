package com.positiveapps.weaware.fragments.sidescreen;

/**
 * Created by Izakos on 15/10/2015.
 */
public interface CloseDrawerRequest {
    public void closeDrawer();
}
