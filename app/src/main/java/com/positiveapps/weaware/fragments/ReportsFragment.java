package com.positiveapps.weaware.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.adapters.ReportsCursorAdapter;
import com.positiveapps.weaware.database.tables.TableReports;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.FragmentsUtil;
import com.positiveapps.weaware.util.Helper;

/**
 * Created by Izakos on 11/10/2015.
 */
public class ReportsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>,Report.onReportsUpdated {


    private ListView repoertsListView;

    public ReportsCursorAdapter adapter;
    private EditText searchInReports;
    private String searchTerm = "";

    public ReportsFragment() {
        // TODO Auto-generated constructor stub
    }

    public static ReportsFragment newInstance() {
        ReportsFragment instance = new ReportsFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_reports;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        repoertsListView = (ListView) view.findViewById(R.id.repoertsListView);
        searchInReports = (EditText) view.findViewById(R.id.searchInReports);

        adapter = new ReportsCursorAdapter(getActivity(), null, false, getActivity());
        repoertsListView.setAdapter(adapter);

        Report.getReports(this, null); // interface

        repoertsListView.setOnScrollListener(onScroll(searchInReports));
    }

    public AbsListView.OnScrollListener onScroll(final EditText editText){
        AbsListView.OnScrollListener listener = new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                AppUtil.hideKeyBoard(editText);
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

            }
        };
        return listener;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(!searchTerm.isEmpty()){
            return MyApp.tableReports.readLouderCursor(TableReports.REPORTER_NAME + " like ?", new String[]{"%" + searchTerm + "%"}, TableReports.COLUMN_ID);
        }
        return MyApp.tableReports.readLouderCursor(TableReports.COLUMN_ID);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static void open(FragmentManager fm){
        FragmentsUtil.openFragment(fm, newInstance(), R.id.main_container);
    }


    @Override
    public void reportsUpdated() {
        getLoaderManager().initLoader(0, null, this);
        Helper.textWatcherWithFilter(searchInReports, adapter);

        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                searchTerm = constraint.toString();
                getLoaderManager().restartLoader(0, null, ReportsFragment.this);
                return null;
            }
        });
    }
}
