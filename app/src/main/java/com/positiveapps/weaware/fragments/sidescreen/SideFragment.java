package com.positiveapps.weaware.fragments.sidescreen;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.fragments.BaseFragment;
import com.positiveapps.weaware.fragments.ContactsFragment;
import com.positiveapps.weaware.fragments.ProfileFragment;
import com.positiveapps.weaware.fragments.ReportsFragment;
import com.positiveapps.weaware.fragments.UniversityFragment;
import com.positiveapps.weaware.main.LoginActivity;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.util.FragmentsUtil;
import com.positiveapps.weaware.util.Static;
import com.positiveapps.weaware.util.ToastUtil;

/**
 * Created by Izakos on 11/10/2015.
 */
public class SideFragment extends BaseFragment implements AdapterView.OnItemClickListener{

    private ListView SideListView;
    private int positionClicked;

    public SideFragment() {
        // TODO Auto-generated constructor stub
    }

    public static SideFragment newInstance() {
        SideFragment instance = new SideFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_side;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SideListView = (ListView) view.findViewById(R.id.SideListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.sidebar_text, getResources().getStringArray(R.array.sideMenu));
        SideListView.setAdapter(adapter);

        SideListView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(getBaseActivity().mDrawerLayout == null){
            return;
        }
        getBaseActivity().mDrawerLayout.closeDrawers();
        switch (i){
            case 0:
                FragmentsUtil.openFragment(getFragmentManager(), ContactsFragment.newInstance(), R.id.main_container);
                break;
            case 1:
                FragmentsUtil.openFragment(getFragmentManager(), ReportsFragment.newInstance(), R.id.main_container);
                break;
            case 2:
                FragmentsUtil.openFragment(getFragmentManager(), UniversityFragment.newInstance(Static.URL_TUTORIAL_VID), R.id.main_container);
                break;
            case 3:
                FragmentsUtil.openFragment(getFragmentManager(), ProfileFragment.newInstance(), R.id.main_container);
                break;
            case 4:
                User.logOut();
                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                MyApp.userProfile.removeInstance();
                MyApp.deleteDatabase();
                getActivity().finish();
                break;
            case 5:
                ToastUtil.toster("Show Credits", false);
                break;
            default:
                break;
        }
    }


}
