/**
 * 
 */
package com.positiveapps.weaware.fragments;



import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;

import com.positiveapps.weaware.main.BaseActivity;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.database.tables.TableObserver;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.BackStackUtil;
import com.positiveapps.weaware.util.ProgressDialogUtil;

import roboguice.fragment.RoboFragment;


/**
 * @author Nati Gabay
 *
 */
public class BaseFragment extends RoboFragment implements OnGlobalLayoutListener,TableObserver {

	protected String TAG = "lypeCycleLog";

	protected ProgressDialog progressDialog;
	public boolean fragIsOn;
	protected String screenName;

	public boolean onBackPressed () {
		return false;
	}



	public void showProgressDialog(String message) {
		this.progressDialog =
				ProgressDialogUtil.showProgressDialog(getActivity(), message);
	}

	public void showProgressDialog() {
		try {
			this.progressDialog =
					ProgressDialogUtil.showProgressDialog(getActivity(),
							getString(R.string.deafult_dialog_messgae));
		} catch (Exception e) {}
	}

	public void dismisProgressDialog() {
		ProgressDialogUtil.dismisDialog(this.progressDialog);
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		//TAG = getClass().getSimpleName() + "Log";
		Log.d(TAG, "ovViewCreated");
		view.getViewTreeObserver().addOnGlobalLayoutListener(this);
		fragIsOn = true;
		BackStackUtil.addToBackStack(getClass().getSimpleName());
		AppUtil.setTextFonts(getActivity(), view);
		if (getBaseActivity() != null){
			getBaseActivity().setCurrentFragment(this);
		}

		MyApp.addTablesObservers(this);


	}


	@Override
	public View onCreateView(LayoutInflater inflater,
							 @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.e(TAG, getClass().getSimpleName() + " onCreateView");
		return super.onCreateView(inflater, container, savedInstanceState);
	}


	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, getClass().getSimpleName() + " onResume");
		removeKeyboard();
	}


	@Override
	public void onStart() {
		super.onStart();
		Log.i(TAG, getClass().getSimpleName() + " onStart");
	}


	@Override
	public void onPause() {
		super.onPause();
		removeKeyboard();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("onDestroyLog", "onDestroy");
		fragIsOn = false;
		BackStackUtil.removeFromBackStack(getClass().getSimpleName());
		MyApp.removeTablesObservers(this);
	}



	@SuppressLint("NewApi")
	@Override
	public void onGlobalLayout() {
		try {
			getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
			onViewRendered(getView());
		} catch (Exception e) {} catch (Error e){}

	}

	public void onViewRendered(View view){
		Log.d(TAG, getClass().getSimpleName() + " onViewRendered");
	}


	public BaseActivity getBaseActivity() {
		BaseActivity activity = null;

		try {
			activity = (BaseActivity) getActivity();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}

		return activity;
	}

	public void onNewIntent(Intent intent){

	}

	@Override
	public void onTableChanged(String tableName, int action) {

	}

	public void removeKeyboard(){
		View view = getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}


	
}