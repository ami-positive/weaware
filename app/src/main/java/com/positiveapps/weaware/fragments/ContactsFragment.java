package com.positiveapps.weaware.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.adapters.ContactsCursorAdapter;
import com.positiveapps.weaware.adapters.FriendsSearchAdapter;
import com.positiveapps.weaware.database.tables.TablePersons;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.searchRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.searchResponse;
import com.positiveapps.weaware.objects.Friend;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.ToastUtil;

import java.util.ArrayList;


/**
 * Created by Izakos on 04/10/2015.
 */
public class ContactsFragment extends BaseFragment implements LoaderCallbacks<Cursor>, View.OnClickListener {

    private final int REGULAR_MODE = 1;
    private final int ADD_FRIENDS_MODE = 2;

    private TextView screenHeader;
    private ListView listView;
    public ContactsCursorAdapter adapter;
    private EditText searchContactET;
    private String searchTerm = "";
    private Button friendsAddButton;
    private Button friendsDoneButton;
    private EditText searchContactQueryET;

    private ArrayList<Friend> friendsList;
    private FriendsSearchAdapter adapter2;

    public ContactsFragment() {
        // TODO Auto-generated constructor stub
    }

    public static ContactsFragment newInstance() {
        ContactsFragment instance = new ContactsFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_contacts;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        listView = (ListView) view.findViewById(R.id.listView);
        screenHeader = (TextView) view.findViewById(R.id.screenHeader);
        friendsDoneButton = (Button) view.findViewById(R.id.friendsDoneButton);
        friendsAddButton = (Button) view.findViewById(R.id.friendsAddButton);
        searchContactET = (EditText) view.findViewById(R.id.searchContactET);
        searchContactQueryET = (EditText) view.findViewById(R.id.searchContactQueryET);

        friendsList = new ArrayList<>(); // initialize the list for search query

        setListViewAnimation(listView);

        adapter = new ContactsCursorAdapter(getActivity(), null, false);
        adapter2 = new FriendsSearchAdapter(getActivity(), R.layout.item_contact, friendsList);

        textWatcherWithQuery(searchContactQueryET, adapter);
        textWatcherWithFilter(searchContactET, adapter);

        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                searchTerm = constraint.toString();
                getLoaderManager().restartLoader(0, null, ContactsFragment.this);
                return null;
            }
        });

        friendsDoneButton.setOnClickListener(this);
        friendsAddButton.setOnClickListener(this);
        listView.setOnScrollListener(onScroll(searchContactET));
        setListMode(REGULAR_MODE);

        Person.getFriends(null);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (!searchTerm.isEmpty()) {
            return MyApp.tablePersons.readLouderCursor(TablePersons.USER_NAME + " like ?", new String[]{"%" + searchTerm + "%"}, TablePersons.COLUMN_ID);
        }
        return MyApp.tablePersons.readLouderCursor(TablePersons.COLUMN_ID);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void textWatcherWithFilter(EditText editText, final ContactsCursorAdapter adapter) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void textWatcherWithQuery(EditText editText, final ContactsCursorAdapter adapter) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                MyApp.networkManager.makeRequest(new searchRequest(s.toString()/*Keyword*/), new NetworkCallback<searchResponse>() {
                            @Override
                            public void onResponse(boolean success, String errorDesc, ResponseObject<searchResponse> response) {
                                super.onResponse(success, errorDesc, response);
                                if (success) {
                                    friendsList = response.getData().getFriends();
                                    System.out.println("friendsList.size " + friendsList.size());
                                    adapter2 = new FriendsSearchAdapter(getActivity(), R.layout.item_contact, friendsList);
                                    listView.setAdapter(adapter2);
//                                    adapter2.notifyDataSetChanged();

                                } else {
                                    ToastUtil.toster(errorDesc, true);
                                }
                            }
                        });

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setListMode(int displayMode) {
        switch (displayMode) {
            case ADD_FRIENDS_MODE:
                friendsAddButton.animate().alpha(0f).translationY(friendsAddButton.getHeight());
                friendsDoneButton.animate().alpha(1f).translationY(0f);
                screenHeader.setText(getString(R.string.add_friends));
                searchContactET.setVisibility(View.GONE);
                searchContactQueryET.setVisibility(View.VISIBLE);
                listView.setAdapter(adapter2);
                break;
            case REGULAR_MODE:
                friendsAddButton.animate().alpha(1f).translationY(0f);
                friendsDoneButton.animate().alpha(0f).translationY(friendsDoneButton.getHeight());
                screenHeader.setText(getString(R.string.freinds_list));
                searchContactET.setVisibility(View.VISIBLE);
                searchContactQueryET.setVisibility(View.GONE);
                listView.setAdapter(adapter);
                getLoaderManager().initLoader(0, null, this);
                break;
        }
    }

    public AbsListView.OnScrollListener onScroll(final EditText editText) {
        AbsListView.OnScrollListener listener = new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                AppUtil.hideKeyBoard(editText);
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

            }
        };
        return listener;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.friendsAddButton:
                setListMode(ADD_FRIENDS_MODE);
                break;
            case R.id.friendsDoneButton:
                setListMode(REGULAR_MODE);
                break;
        }
    }

    public void setListViewAnimation(ListView listView){
            AnimationSet set = new AnimationSet(true);
            Animation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(300);
            set.addAnimation(animation);
//
//            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 50.0f,
//                    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
//                    0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
//            animation.setDuration(300);
//            set.addAnimation(animation);

            LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);

            listView.setLayoutAnimation(controller);
        }
}
