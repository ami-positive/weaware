package com.positiveapps.weaware.fragments.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.fragments.BaseFragment;

/**
 * Created by Izakos on 06/10/2015.
 */
public class RegistrationFragment extends BaseFragment{

    private Button RegistrationSignUpBT;
    private EditText RegistrationUserNameET;
    private EditText RegistrationPassET;

    public RegistrationFragment() {
        // TODO Auto-generated constructor stub
    }



    public static RegistrationFragment newInstance() {
        RegistrationFragment instance = new RegistrationFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_registration;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RegistrationSignUpBT = (Button) view.findViewById(R.id.RegistrationSignUpBT);
        RegistrationPassET = (EditText) view.findViewById(R.id.RegistrationPassET);
        RegistrationUserNameET = (EditText) view.findViewById(R.id.RegistrationUserNameET);

//        RegistrationPassET.setOnEditorActionListener(this);
//        RegistrationSignUpBT.setOnClickListener(this);
    }


//    @Override
//    public void onClick(View view) {
//        User.register(RegistrationUserNameET, RegistrationPassET);
//    }
//
//    @Override
//    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//        if (i == EditorInfo.IME_ACTION_NEXT) {
//            User.register(RegistrationUserNameET, RegistrationPassET);
//            return true;
//        }
//        return false;
//    }


}
