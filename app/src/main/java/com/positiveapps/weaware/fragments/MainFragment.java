package com.positiveapps.weaware.fragments;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.database.tables.TableObserver;
import com.positiveapps.weaware.main.MapActivity;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.UploadAudioRequest;
import com.positiveapps.weaware.network.requests.closeRequest;
import com.positiveapps.weaware.network.requests.saveReportRequest;
import com.positiveapps.weaware.network.requests.uploadImageRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.UploadAudioResponse;
import com.positiveapps.weaware.network.response.closeResponse;
import com.positiveapps.weaware.network.response.saveReportResponse;
import com.positiveapps.weaware.network.response.uploadImageResponse;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.objects.TimerListener;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.ui.StarBitmapDrawable;
import com.positiveapps.weaware.ui.Vmark;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.BitmapUtil;
import com.positiveapps.weaware.util.DateUtil;
import com.positiveapps.weaware.util.Helper;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.Static;
import com.positiveapps.weaware.util.ToastUtil;

import java.io.File;
import java.io.IOException;

import retrofit.mime.TypedFile;

/**
 * Created by Izakos on 11/10/2015.
 */
public class MainFragment extends BaseFragment implements View.OnClickListener, TimerListener, TableObserver, Helper.onGetSetting {

    private static final int REPORT_AUDIO_OPEN = 1;
    private static final int REPORT_AUDIO_CLOSE = 2;


    private static final String LOG_TAG = "LOG";
    private Button mainBT;
    private Button mainPresentBT;
    private Button mainReportsBT;
    private Button mainUniversityBT;
    private Button mainMapBT;
    private RatingBar LinearLayout;

    private View includeReport;
    private Button reportAudio;
    private Button reportCamera;
    private Button reportStart;
    private LinearLayout reportCancel;
    private EditText reportDescription;
    private String imageBase64String;
    private LinearLayout RegularModeLayout;
    private LinearLayout MainRatingBar;
    private LinearLayout commentLL;
    private boolean commenting = true;
    private Button reportComment;
    private boolean audioRecording;
    private LinearLayout audioLL;
    private Button reportMap;
    private String mFileName;
    private MediaRecorder mRecorder;
    private Chronometer chronometer;
    private Button stopReportAudio;
    private TextView audioText;
    private Button sendComment;
    private Button counterReports;
    private AnimationSet rollingIn;
    private LinearLayout centerLayout;
    private LinearLayout audioLayout;

    public MainFragment() {
        // TODO Auto-generated constructor stub
    }

    public static MainFragment newInstance() {
        MainFragment instance = new MainFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_main;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mainBT = (Button) view.findViewById(R.id.mainBT);
        mainPresentBT = (Button) view.findViewById(R.id.mainPresentBT);
        mainReportsBT = (Button) view.findViewById(R.id.mainReportsBT);
        mainUniversityBT = (Button) view.findViewById(R.id.mainUniversityBT);
        mainMapBT = (Button) view.findViewById(R.id.mainMapBT);
        counterReports = (Button) view.findViewById(R.id.counterReports);
        MainRatingBar = (LinearLayout) view.findViewById(R.id.MainRatingBar);
        RegularModeLayout = (LinearLayout) view.findViewById(R.id.RegularModeLayout);

        mainBT.setOnClickListener(this);
        mainPresentBT.setOnClickListener(this);
        mainReportsBT.setOnClickListener(this);
        mainUniversityBT.setOnClickListener(this);
        mainMapBT.setOnClickListener(this);

        getViewsForAction(view);

        mode(Report.getmode());

        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audios.3gp";

        initUI();
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.mainBT:
                startReport();
                break;
            case R.id.mainPresentBT:
                GiftsFragment.open(getFragmentManager(), Static.URL_GIFT);
                break;
            case R.id.mainReportsBT:
                ReportsFragment.open(getFragmentManager());
                break;
            case R.id.mainUniversityBT:
                UniversityFragment.open(getFragmentManager(), Static.URL_UNIVERSITY);
                break;
            case R.id.mainMapBT:
                getActivity().startActivity(new Intent(getActivity(), MapActivity.class));
                getActivity().finish();
                break;
        }
    }

    public void getViewsForAction(View view){

        includeReport = view.findViewById(R.id.mapActionInclude);

        reportAudio = (Button)includeReport.findViewById(R.id.reportAudio);
        stopReportAudio = (Button)includeReport.findViewById(R.id.stopReportAudio);
        reportCamera = (Button)includeReport.findViewById(R.id.reportCamera);
        reportMap = (Button)includeReport.findViewById(R.id.reportMap);
        reportCancel = (LinearLayout)includeReport.findViewById(R.id.reportCancel);
        reportDescription = (EditText)includeReport.findViewById(R.id.reportDescription);
        commentLL = (LinearLayout)includeReport.findViewById(R.id.commentLL);
        audioLL = (LinearLayout)includeReport.findViewById(R.id.audioLL);
        reportComment = (Button)includeReport.findViewById(R.id.reportComment);
        sendComment = (Button)includeReport.findViewById(R.id.sendComment);
        chronometer = (Chronometer)includeReport.findViewById(R.id.chronometer);
        audioText = (TextView)includeReport.findViewById(R.id.audioText);
        centerLayout = (LinearLayout)includeReport.findViewById(R.id.centerLayout);
        audioLayout = (LinearLayout)includeReport.findViewById(R.id.audioLayout);


        View.OnClickListener onAction = new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.reportAudio:
                        Helper.play(R.raw.beep_short_on);
                        Log.i(TAG, "onClick reportAudio");
                        reportAudio.setClickable(false);
                        stopReportAudio.setClickable(true);
                        rollingAudioAnimation(centerLayout, audioLayout, reportAudio, stopReportAudio, REPORT_AUDIO_OPEN, false);
                        break;
                    case R.id.stopReportAudio:
                        Helper.play(R.raw.beep_short_off);
                        Log.i(TAG, "onClick reportAudio");
                        reportAudio.setClickable(true);
                        stopReportAudio.setClickable(false);
                        rollingAudioAnimation(centerLayout, audioLayout, reportAudio, stopReportAudio, REPORT_AUDIO_CLOSE, false);
                        break;
                    case R.id.reportCamera:
                        Helper.play(R.raw.click);
                        Helper.openCamera(MainFragment.this);
                        break;
                    case R.id.reportMap:
                        Helper.play(R.raw.click);
                        getActivity().startActivity(new Intent(getActivity(), MapActivity.class));
                        getActivity().finish();
                        break;
                    case R.id.reportCancel:
                        Helper.play(R.raw.beep_short_off);
                        showCancelDialog();
                        break;
                    case R.id.reportComment:
                        Helper.play(R.raw.click);
                        rotateView(0, reportComment, sendComment);
                        reportDescription.setVisibility(View.VISIBLE);
                        reportDescription.animate().alpha(1).rotationX(0f);
                        break;
                    case R.id.sendComment:
                        AppUtil.hideKeyBoard(reportDescription);
                        rotateView(1, reportComment, sendComment);
                        reportDescription.animate().alpha(0).rotationX(90f);
                        reportDescription.setVisibility(View.GONE);
                        if(!reportDescription.getText().toString().isEmpty()){
                            saveReportQuery(Report.getCurrentReport(), reportDescription.getText().toString());
                            reportDescription.setText("");
                        }
                        break;
                }
            }
        };

        reportAudio.setOnClickListener(onAction);
        sendComment.setOnClickListener(onAction);
        stopReportAudio.setOnClickListener(onAction);
        reportCamera.setOnClickListener(onAction);
        reportMap.setOnClickListener(onAction);
        reportCancel.setOnClickListener(onAction);
        reportComment.setOnClickListener(onAction);

    }


    private void rollinAnimation(){

    }
    private void onComment(boolean comment) {
        if (comment) {
            commentLL.animate().translationY(-commentLL.getHeight());
            reportDescription.animate().rotationX(0f);
        } else {
            commentLL.animate().translationY(0);
            reportDescription.animate().rotationX(90f);
        }
    }

    private void onAudio(boolean audio) {
        if (audio) {
            audioLL.animate().translationX(audioLL.getWidth());
            commentLL.animate().translationX(commentLL.getWidth()).translationY(-audioLL.getHeight());
        } else {
            audioLL.animate().translationX(0);
            commentLL.animate().translationX(0).translationY(0);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap image = null;
        if(resultCode != -1){
            return;
        }

        switch (requestCode) {
            case Static.CAMERA_REQUEST_CODE:
                File file = new File(Environment.getExternalStorageDirectory().getPath(), Static.CAMERA_PROFILE_PATH);
                image = BitmapUtil.loadImageIntoByStringPath(file.getAbsolutePath(), new ImageView(getActivity()), 300, 2, ImageLoader.DEAFULT_PLACE_HOLDER);
                imageBase64String = Helper.getProfileImageAsBase64(image);
                uploadImageQuery(Report.getReportID(), imageBase64String);
                break;
            default:
                break;
        }
    }

    public void mode(int mode){
        switch (mode) {
            case Static.ALERT_MODE:
                if(isAdded()) {
                    getBaseActivity().originalActionbarColor(false);
                }
                Report.setReportTimer(this);
                Report.startReportTimer();
                Report.setMode(Static.ALERT_MODE);
                includeReport.setVisibility(View.VISIBLE);
                RegularModeLayout.setVisibility(View.GONE);
                break;
            case Static.NORMAL_MODE:
                if(isAdded()) {
                    getBaseActivity().originalActionbarColor(true);
                }
                Report.stopReportTimer();
                Report.setMode(Static.NORMAL_MODE);
                includeReport.setVisibility(View.GONE);
                RegularModeLayout.setVisibility(View.VISIBLE);
                break;
        }
    }



    private void initReportStatus() {
        Report report = Report.getCurrentReport();
        if(report == null){
            return;
        }
        if(!report.getAudioAttached().isEmpty()){
            new Vmark().set(reportAudio, R.drawable.mic_circle, true, false);
        }
        if(!report.getDescription().isEmpty()){
            System.out.println("--> WTF " + report.getDescription());
            new Vmark().set(reportComment, R.drawable.description_circle, true, false);
        }
        if(!report.getPhotoAttached().isEmpty()){
            new Vmark().set(reportCamera, R.drawable.camera_circle, true, false);
        }
    }

/*    public void showLoadingDialog(String text){
        Dialog loadingDialog = new Dialog(getActivity(), R.style.cancelDialog);
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_cancel_alert, null);
        ((RadioGroup) view.findViewById(R.id.cancelDialogButtons)).setOnCheckedChangeListener(cancelAlertListener(loadingDialog));
        loadingDialog.setContentView(view);
//		loadingDialog.setCancelable(false);
        loadingDialog.show();
    }*/

    public void showCancelDialog() {

        if (getActivity().isFinishing()) {return;}

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.cancel_report)
                        .content(R.string.are_you_sure_you_want_to_cancel_the_current_report)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                Helper.play(R.raw.beep_short_off);
                                cancelReport();
                            }
                        })
                        .show();
            }
        });
    }

    private void initUI() {
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

//        rollingIn = new AnimationSet(true);
//        Animation moving = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 5, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
//        moving.setDuration(1000);
//        final Animation rotating = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        rotating.setDuration(1000);
//
//        rollingIn.addAnimation(rotating);
//        rollingIn.addAnimation(moving);


        starsContainer(getActivity(), MainRatingBar, User.getNumberOfStarts());
        int color = Color.parseColor(MyApp.userProfile.geLevelColor().replace("#", "#59"));
        if (Build.VERSION.SDK_INT >= 16) {
            mainBT.setBackground(Helper.bigButton(color));
        } else {
            mainBT.setBackgroundDrawable(Helper.bigButton(color));
        }
        Helper.setRankColorsQuery(this);

        if(Report.getReportCounts() != 0){
            counterReports.setVisibility(View.VISIBLE);
        }else{
            counterReports.setVisibility(View.GONE);
        }
        counterReports.setText(String.valueOf(Report.getReportCounts()));
    }

    private RadioGroup.OnCheckedChangeListener cancelAlertListener(final Dialog dialog){
        RadioGroup.OnCheckedChangeListener radioClick = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.confirmCancel:
                        Helper.play(R.raw.beep_short_off);
                        cancelReport();
                        dialog.dismiss();
                        break;
                    case R.id.noCancel:
                        dialog.dismiss();
                        break;
                }
            }
        };

        return radioClick;
    }

    private void startReport() {
        YoYo.with(Techniques.Tada).duration(2000).playOn(mainBT);
        Helper.play(R.raw.music_marimba_chord);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Report report = new Report();
                report.setReportDate(DateUtil.getCurrentTimeStamp());
                report.setReporterName(User.getFullName());
                report.setLat(User.getLat());
                report.setLon(User.getLon());
                report.setReporterImage(User.getProfileImage());
                saveReportQuery(report);
                Report.setMode(Static.ALERT_MODE);
                getActivity().startActivity(new Intent(getActivity(), MapActivity.class));
                getActivity().finish();
            }
        }, 2000);



    }

    @Override
    public void onReportTimeOut() {
        mode(Static.NORMAL_MODE);
        Report report = Report.getCurrentReport();
        if(report != null){MyApp.tableReports.delete(report.getDatabaseId(), report);}
        mode(Static.NORMAL_MODE);
        Report.setReportID(0);
        ToastUtil.toster("report canceled", true);
    }

    private void starsContainer(Context context, ViewGroup container, int numStars){
        ImageView image = null;
        int size = (int) AppUtil.convertDpToPixel(35f, MyApp.appContext);
        container.removeAllViews();
        for (int i = 0; i < numStars ; i++) {
            image = new ImageView(context);
            Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);

            bitmap.eraseColor(Color.parseColor(MyApp.userProfile.geLevelColor()));
            image.setImageDrawable(new StarBitmapDrawable(bitmap));
            container.addView(image);
        }

        for (int i = 0; i < (5 - numStars); i++){
            image = new ImageView(context);
            Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(Color.GRAY);
            image.setImageDrawable(new StarBitmapDrawable(bitmap));
            container.addView(image);
        }

    }

    private void startRecording() {
        rotateView(0, reportAudio, stopReportAudio);
        rotateView(0, audioText, chronometer);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isAdded()){getBaseActivity().originalActionbarColor(Report.getmode() == Static.NORMAL_MODE ? true : false);}
    }

    private void stopRecording() {
//        chronometer.animate().translationY(chronometer.getHeight()).alpha(0f);
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

        rotateView(1, reportAudio, stopReportAudio);
        rotateView(1, audioText, chronometer);

        System.out.println("uploadAudio");
        uploadAudio();
    }

    private void saveReportQuery(final Report report){
        MyApp.networkManager.makeRequest(new saveReportRequest("0"/*ID*/, ""/*Description*/, User.getLatString()/*Lat*/, User.getLonString()/*Lng*/), new NetworkCallback<saveReportResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<saveReportResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    report.setServerId(response.getData().getId());
                    MyApp.tableReports.insertOrUpdate(report);
                    Report.setReportID(response.getData().getId());
                    User.setStartAlarm(); // save on shared preferences
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    private void saveReportQuery(final Report report ,final String comment){
        new Vmark().set(reportComment, R.drawable.description_circle, false, false);
        MyApp.networkManager.makeRequest(new saveReportRequest(String.valueOf(Report.getReportID())/*ID*/, comment/*Description*/, User.getLatString()/*Lat*/, User.getLonString()/*Lng*/), new NetworkCallback<saveReportResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<saveReportResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    new Vmark().set(reportComment, R.drawable.description_circle, true, true);
                    Report report = Report.getCurrentReport();
                    if (report != null) {
                        report.setDescription(comment);
                        MyApp.tableReports.insertOrUpdate(report);
                    }
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    @Override
    public void onViewRendered(View view) {
        super.onViewRendered(view);
        if (Build.VERSION.SDK_INT >= 16) {
            rollingAudioAnimation(centerLayout, audioLayout, reportAudio, stopReportAudio, REPORT_AUDIO_CLOSE, true); // init rolling animation
           if(Report.getmode() == Static.ALERT_MODE) {
               initReportStatus();
           }
        }

    }

    private void rollingAudioAnimation(View mainLayout, View audioLayout, View audioPlayButton, View audioStopButton, int openClose, boolean initialAnimation){
        int layoutWidth = mainLayout.getWidth(); // width of the all layout
        int buttonWidth = audioPlayButton.getWidth();
        float difference = (float) (layoutWidth - buttonWidth);

        if(initialAnimation){
            audioLayout.getLayoutParams().width = (int)difference;
            audioLayout.setTranslationX(layoutWidth);
            audioLayout.setAlpha(0);
            return;
        }
        audioLayout.setAlpha(0);
        mainLayout.animate().translationX(openClose == REPORT_AUDIO_OPEN ? difference : 0)
                .setListener(rollingAnimationListener(openClose, layoutWidth));
    }



    private Animator.AnimatorListener rollingAnimationListener(final int openClose, final float layoutWidth){
        Animator.AnimatorListener listenr = new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                switch (openClose){
                    case REPORT_AUDIO_OPEN:
                        startRecording();
                        audioLayout.animate().translationX(0).alpha(1);
                        break;
                    case REPORT_AUDIO_CLOSE:
                        audioLayout.animate().alpha(0).translationX(layoutWidth);
                        stopRecording();
                        break;
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        };
        return  listenr;
    }


    public void uploadAudio(){
        new Vmark().set(reportAudio, R.drawable.mic_circle, false, false);
//        FileUploadService service = ServiceGenerator.createService(FileUploadService.class, FileUploadService.BASE_URL);
        TypedFile typedFile = new TypedFile("audio/3gp", new File(mFileName));
        MyApp.networkManager.makeRequest(new UploadAudioRequest(typedFile/*myfile*/), new NetworkCallback<UploadAudioResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<UploadAudioResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    new Vmark().set(reportAudio, R.drawable.mic_circle, true, true);
                    Report report = Report.getCurrentReport();
                    if (report != null) {
                        report.setAudioAttached("N/A");
                        MyApp.tableReports.insertOrUpdate(report);
                    }
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public void cancelReport(){
        MyApp.networkManager.makeRequest(new closeRequest(String.valueOf(Report.getReportID())/*ID*/), new NetworkCallback<closeResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<closeResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    Report report = Report.getCurrentReport();
                    MyApp.tableReports.delete(report.getDatabaseId(), report);
                    mode(Static.NORMAL_MODE);
                    Report.setReportID(0);
                    ToastUtil.toster(getString(R.string.report_canceled), true);
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    private void rotateView(int mode, View...views) {
        for (int i = 0; i < views.length; i++) {
            if (i == mode) {
                views[i].animate().rotationY(90f).alpha(0);
            } else {
                views[i].animate().rotationY(0).alpha(1).translationX(0);
            }
        }
    }

    public void uploadImageQuery(long reportId, String Image){
        new Vmark().set(reportCamera, R.drawable.camera_circle, false, false);
        MyApp.networkManager.makeRequest(new uploadImageRequest(String.valueOf(reportId)/*ReportID*/,Image/*Image*/), new NetworkCallback<uploadImageResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<uploadImageResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    new Vmark().set(reportCamera, R.drawable.camera_circle, true, true);
                    Report report = Report.getCurrentReport();
                    if (report != null) {
                        report.setPhotoAttached("N/A");
                        MyApp.tableReports.insertOrUpdate(report);
                    }
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    @Override
    public void onTableChanged(String tableName, int action) {
        if(tableName.matches(MyApp.tableReports.TABLE_REPORTS)){
            if(Report.getReportCounts() != 0){
                counterReports.setVisibility(View.VISIBLE);
            }else{
                counterReports.setVisibility(View.GONE);
            }
            counterReports.setText(String.valueOf(Report.getReportCounts()));

            getBaseActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("tableNama");
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mRecorder != null){
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
    }

    @Override
    public void settingDone() {
        if(isAdded()) {
            starsContainer(getActivity(), MainRatingBar, User.getNumberOfStarts());
            int color = Color.parseColor(MyApp.userProfile.geLevelColor().replace("#", "#59"));
            if (Build.VERSION.SDK_INT >= 16) {
                mainBT.setBackground(Helper.bigButton(color));
            } else {
                mainBT.setBackgroundDrawable(Helper.bigButton(color));
            }
        }
    }
}





