package com.positiveapps.weaware.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.database.tables.TableObserver;
import com.positiveapps.weaware.fragments.BaseFragment;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.ProgressDialogUtil;

import roboguice.activity.RoboActionBarActivity;


public  class BaseActivity extends RoboActionBarActivity implements  TableObserver {

	protected Menu menu;
	protected Toolbar mActionBarToolbar = null;
	protected LinearLayout actionbarShadow;


	protected boolean activityIsOn;
	protected ProgressDialog progressDialog;
	protected BaseFragment currentFragment;
	public DrawerLayout mDrawerLayout;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activityIsOn = true;
		MyApp.addTablesObservers(this);

	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
		AppUtil.setTextFonts(this, rootView);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MyApp.currentActivity = this;
		removeKeyboard();
	}

	@Override
	protected void onPause() {
		super.onPause();
		removeKeyboard();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityIsOn = false;
		MyApp.removeTablesObservers(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		return true;
	}



	public void hideOption(int id)
	{
		MenuItem item = menu.findItem(id);
		item.setVisible(false);
	}

	public void showOption(int id)
	{
		MenuItem item = menu.findItem(id);
		item.setVisible(true);
	}

	public void setOptionTitle(int id, String title)
	{
		MenuItem item = menu.findItem(id);
		item.setTitle(title);
	}

	public void setOptionIcon(int id, int iconRes)
	{
		MenuItem item = menu.findItem(id);
		item.setIcon(iconRes);
	}



	public void setupActionBar(int toolbarId) {

		Toolbar toolbar = (Toolbar) findViewById(toolbarId);
		if (toolbar != null) {
			mActionBarToolbar = toolbar;
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);



		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		if (menuItem.getItemId() == android.R.id.home) {
			System.out.println("Home pressed");
		}
		return super.onOptionsItemSelected(menuItem);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

	}

	public void setupActionBar(int toolbarId,String title) {

		Toolbar toolbar = (Toolbar) findViewById(toolbarId);
		if (toolbar != null) {
			mActionBarToolbar = toolbar;
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);
			actionbarDisplayTitle(title);
		}

	}

	public void originalActionbarColor(boolean isNotInAlertMode){
		Window window = getWindow();
		window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		if(isNotInAlertMode){
			mActionBarToolbar.setBackgroundColor(MyApp.appContext.getResources().getColor(R.color.actionbar));
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				window.setStatusBarColor(MyApp.appContext.getResources().getColor(R.color.actionbar));
			}
		}else{
			mActionBarToolbar.setBackgroundColor(MyApp.appContext.getResources().getColor(R.color.red_1));
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				window.setStatusBarColor(MyApp.appContext.getResources().getColor(R.color.red_1));
			}
		}
	}
	public Toolbar getActionbarToolbar(){
		return mActionBarToolbar;
	}

	public void actionbarDisplayTitle(String title) {
		if (title == null){
			actionbarSetActionbarImageVisibility(View.VISIBLE);
			try {
				getActionBarTextView().setText("");
			} catch (Exception e) {}
		}else{
			actionbarSetActionbarImageVisibility(View.GONE);
			try {
				getActionBarTextView().setText(title);
			} catch (Exception e) {}
		}

	}

	public void actionbarSetActionbarImageVisibility (int visible){
		findViewById(R.id.actionbar_image).setVisibility(visible);
	}

	public ImageView actionbarGetActionbarImage () {
		return (ImageView)findViewById(R.id.actionbar_image);
	}

	public void actionbarDisplayLogo() {
		View logo = findViewById(R.id.app_logo);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		logo.setVisibility(View.VISIBLE);
	}

	public TextView getActionBarTextView() {
		return (TextView) findViewById(R.id.app_logo);
	}

	public void setSosPhoneNumber() {
		((ImageView) findViewById(R.id.sos)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse(getString(R.string.phone_number_sos)));
				startActivity(callIntent);
			}
		});
	}



	public void actionbarHideWithAnimation() {
		int toolbarHeight = mActionBarToolbar.getHeight();
		mActionBarToolbar.animate().translationY(toolbarHeight * (-1))
				.setDuration(300).start();
	}

	public void actionbarShowWithAnimation() {
		mActionBarToolbar.animate().translationY(0).setDuration(300).start();
	}



	public void actionbarAddView(View v) {
		LinearLayout linearActionbarView = (LinearLayout)
				findViewById(R.id.linearToolbarContainer);
		linearActionbarView.removeAllViews();
		linearActionbarView.addView(v);
	}


	public void actionbarRemoveView(View v) {
		LinearLayout linearActionbarView = (LinearLayout) findViewById(R.id.linearToolbarContainer);
		linearActionbarView.removeView(v);
	}

	public void actionbarRemoveAllViews() {
		LinearLayout linearActionbarView = (LinearLayout) findViewById(R.id.linearToolbarContainer);
		linearActionbarView.removeAllViews();
	}

	public void actionbarAddView(int layoutRecource) {
		LayoutInflater inflater = LayoutInflater.from(this);
		LinearLayout linearActionbarView = (LinearLayout) mActionBarToolbar.findViewById(R.id.linearToolbarContainer);
		View viewToAdd = inflater.inflate(layoutRecource, linearActionbarView, false);

		linearActionbarView.addView(viewToAdd);
	}


	public void actionbarSetAlpha(float alpha){
		actionbarShadow = (LinearLayout)findViewById(R.id.actionbar_shadow);
		try {
			if (alpha < 1){
				actionbarShadow.setVisibility(View.GONE);
			}else{
				actionbarShadow.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {}

		int originalColor = getResources().getColor(R.color.app_gray);
		int bgWithAlpha = getColorWithAlpha(originalColor, alpha);

		mActionBarToolbar.setBackgroundColor(bgWithAlpha);
	}

	public static int getColorWithAlpha( int baseColor, float alpha) {
		int a = Math.min(255, Math.max(0, (int) (alpha * 255))) << 24;
		int rgb = 0x00ffffff & baseColor;
		return a + rgb;
	}

	public int adjustAlpha(int color, float factor) {
		int alpha = Math.round(Color.alpha(color) * factor);
		int red = Color.red(color);
		int green = Color.green(color);
		int blue = Color.blue(color);
		return Color.argb(alpha, red, green, blue);
	}




	public void showProgressDialog(String message) {
		this.progressDialog =
				ProgressDialogUtil.showProgressDialog(this, message);
	}

	public void showProgressDialog() {
		try {
			this.progressDialog =
					ProgressDialogUtil.showProgressDialog(this,
							getString(R.string.deafult_dialog_messgae));


		} catch (Exception e) {}
	}

	public void dismissProgressDialog() {
		ProgressDialogUtil.dismisDialog(this.progressDialog);
	}

	public void setCurrentFragment (BaseFragment currentFragment){
		this.currentFragment = currentFragment;
	}

	public BaseFragment getCurrentFragment (){
		return this.currentFragment;
	}


	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (currentFragment != null){
			currentFragment.onNewIntent(intent);
		}
	}

	@Override
	public void onTableChanged(String tableName, int action) {

	}

	public void removeKeyboard(){
		View view = getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}



}
