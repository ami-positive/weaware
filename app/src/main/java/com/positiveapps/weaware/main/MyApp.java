package com.positiveapps.weaware.main;

import android.app.Application;
import android.content.Context;
import android.media.MediaPlayer;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.database.tables.TableObserver;
import com.positiveapps.weaware.database.tables.TablePersons;
import com.positiveapps.weaware.database.tables.TableReports;
import com.positiveapps.weaware.network.NetworkManager;
import com.positiveapps.weaware.storage.AppPreference;
import com.positiveapps.weaware.storage.StorageManager;
import com.positiveapps.weaware.util.DeviceUtil;
import com.positiveapps.weaware.util.ToastUtil;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

/**
 * Created by natiapplications on 16/08/15.
 */
public class MyApp extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "7fSkz5g1PoPkmP8xINhB2SbWF";
    private static final String TWITTER_SECRET = "hMJXWY8Cb66ztJnrBYryNgLuzgr9qoqI0zgFk8EgdmnNdtUAc4";

    public static MediaPlayer mMediaPlayer;
    public static BaseActivity mainInstance;
    public static boolean showDialogFlag = true;
    public static boolean showErrorDialogFlag = true;
    private AuthCallback authCallback;
    public static Context appContext;
    public static String appVersion;
    public static AppPreference appPreference;
    public static AppPreference.PreferenceUserSettings userSettings;
    public static AppPreference.PreferenceGeneralSettings generalSettings;
    public static AppPreference.PreferenceUserProfil userProfile;

    Permission[] permissions = new Permission[]
            {
                    Permission.PUBLISH_ACTION
            };


    public static BaseActivity currentActivity;


    //managers
    public static NetworkManager networkManager;
    public static StorageManager storageManager;
    public static TablePersons tablePersons;
    public static TableReports tableReports;

    public static ArrayList<String> currentPlaceIds = new ArrayList<String>();

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig), new Crashlytics());


        appContext = getApplicationContext();
        appVersion = DeviceUtil.getDeviceVersion();

        loadPreferences();
        loadDataBase();

        networkManager = NetworkManager.getInstance(appContext);
        storageManager = StorageManager.getInstance();

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getString(R.string.facebook_app_id))
                .setNamespace("sromkuapp")
                .setPermissions(permissions)
                .build();

        SimpleFacebook.setConfiguration(configuration);

//        TwitterAuthConfig authConfig2 =  new TwitterAuthConfig("consumerKey", "consumerSecret");
//        Fabric.with(this, new TwitterCore(authConfig2), new Digits(), new Crashlytics());

        authCallback = new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                ToastUtil.toster("phoneNumber: " + phoneNumber, false);
            }

            @Override
            public void failure(DigitsException exception) {
                // Do something on failure
            }
        };
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public AuthCallback getAuthCallback(){
        return authCallback;
    }

    private void loadPreferences() {
        appPreference = AppPreference.getInstans(appContext);
        userSettings = appPreference.getUserSettings();
        userProfile = appPreference.getUserProfil();
        generalSettings = appPreference.getGeneralSettings();
    }


    public static void loadDataBase (){
        tablePersons = TablePersons.getInstance(appContext);
        tableReports = TableReports.getInstance(appContext);
    }

    public static void closeDatabase () {
        MyApp.tablePersons.close();
        MyApp.tableReports.close();
    }

    public static void deleteDatabase () {
        MyApp.tablePersons.deleteAll();
        MyApp.tableReports.deleteAll();
    }

    public static void addTablesObservers (TableObserver observer){
        MyApp.tablePersons.addObserver(observer);
        MyApp.tableReports.addObserver(observer);
    }

    public static void removeTablesObservers (TableObserver observer){
        MyApp.tablePersons.removeObserver(observer);
        MyApp.tableReports.removeObserver(observer);
    }

    private Tracker mTracker;

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }
}
