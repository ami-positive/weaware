package com.positiveapps.weaware.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.LoginRequest;
import com.positiveapps.weaware.network.requests.TwiiterLoginRequest;
import com.positiveapps.weaware.network.requests.fbLoginRequest;
import com.positiveapps.weaware.network.response.LoginResponse;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.TwiiterLoginResponse;
import com.positiveapps.weaware.network.response.fbLoginResponse;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.objects.Reports;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.Static;
import com.positiveapps.weaware.util.Validation;
import com.sromku.simple.fb.SimpleFacebook;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

public class LoginActivity extends BaseActivity implements View.OnClickListener , TextView.OnEditorActionListener,  User.onFacebookLogin ,User.onTwitterLogin, Report.onReportsUpdated{

    private Button loginSignUpBT;
    private Button loginForgetPassBT;
    private Button loginFacebookBT;
    private Button loginTweeterBT;
    private Button RegistrationBT;
    private EditText loginUserET;
    private EditText loginPassET;

    private TwitterLoginButton twitterLoginButton;
    private GoogleApiClient mGoogleApiClient;
    
    
    public static SimpleFacebook mSimpleFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, null)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        twitterLoginButton = (TwitterLoginButton)findViewById(R.id.tw_login_button);
        loginSignUpBT = (Button) findViewById(R.id.loginSignUpBT);
        RegistrationBT = (Button) findViewById(R.id.RegistrationBT);
        loginForgetPassBT = (Button) findViewById(R.id.loginForgetPassBT);
        loginFacebookBT = (Button) findViewById(R.id.loginFacebookBT);
        loginTweeterBT = (Button) findViewById(R.id.loginTweeterBT);
        loginUserET = (EditText) findViewById(R.id.loginUserET);
        loginPassET = (EditText) findViewById(R.id.loginPassET);

        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        findViewById(R.id.sign_in_button).setOnClickListener(this);


        RegistrationBT.setOnClickListener(this);
        loginSignUpBT.setOnClickListener(this);
        loginForgetPassBT.setOnClickListener(this);
        loginFacebookBT.setOnClickListener(this);
        loginTweeterBT.setOnClickListener(this);
        loginPassET.setOnEditorActionListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (User.currentLoginType){
            case User.LOGIN_TYPE_FACEBOOK:
                mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
                break;
            case User.LOGIN_TYPE_TWITTER:
                twitterLoginButton.onActivityResult(requestCode, resultCode, data);
                break;
        }

        if (requestCode == User.GOOGLE_PLUS) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("googleSignIn", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
//            ToastUtil.toster("Welcome " + acct.getDisplayName(), true);
            //            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            //            updateUI(false);
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, User.GOOGLE_PLUS);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.RegistrationBT:
                goToRegistrationActivity();
                break;
            case R.id.loginSignUpBT:
                login(loginUserET, loginPassET);
                break;
            case R.id.loginForgetPassBT:
                User.forgotPassword(getSupportFragmentManager());
                break;
            case R.id.loginFacebookBT:
                User.logToFacebook(mSimpleFacebook, this);
                break;
            case R.id.loginTweeterBT:
                User.logToTwitter(twitterLoginButton, this);
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_NEXT) {
            login(loginUserET, loginPassET);
            return true;
        }
        return false;
    }


    public void goToRegistrationActivity(){
        startActivity(new Intent(this, RegistrationActivity.class));
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        AppUtil.startLocationService(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppUtil.stopLocationService(this);
    }

    public void login(EditText name, EditText pass) {
        final String Name = name.getText().toString().trim();
        String Pass = pass.getText().toString().trim();
        if (!Validation.password(pass, Pass)) {
            return;
        }
//        currentLoginType = LOGIN_TYPE_SERVER;

        MyApp.networkManager.makeRequest(new LoginRequest(Name/*Username*/, Pass/*Password*/), new NetworkCallback<LoginResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<LoginResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
//                    ToastUtil.toster("Welcome Back " + Name, false);
                    User.setAccessToken(response.getData().getAccessToken());
                    User.setServerId(Long.parseLong(response.getData().getAppUser().getID()));
                    User.updateLocation(null);
                    User.updateFromServer(LoginActivity.this, new User.onUserUpdate() {
                        @Override
                        public void userUpdated(Reports reportObj) {
                            if(reportObj != null) {
                                Report.setReportID(Long.parseLong(reportObj.getId()));
                                Report.setMode(Static.ALERT_MODE);
                            }else{
                                Report.setReportID(0);
                                Report.setMode(Static.NORMAL_MODE);
                            }

                            Report.getReports(LoginActivity.this, null); // interface
                        }
                    }, null);
//                    showDialog(getString(R.string.success), response.getData().toString());
                } else {
                    showErrorDialog(getString(R.string.failure), errorDesc);
                }
            }
        });



    }

    public void showDialog(final String title, final String msg) {

        if (!MyApp.showDialogFlag) {
//            new Intent(RegistrationActivity.this, MainActivity.class);
            return;
        }

        if (isFinishing()) {return;}

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(LoginActivity.this)
                        .title(title)
                        .content(msg)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
//                                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));

                            }
                        })
                        .show();
            }
        });
    }

    public void showErrorDialog(final String title, final String msg) {

        if (!MyApp.showErrorDialogFlag) {return;}

        if (isFinishing()) {return;}

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(LoginActivity.this)
                        .title(title)
                        .content(msg)
                        .positiveText(android.R.string.ok)
                        .show();
            }
        });
    }


    @Override
    public void onLogin(String accessToken, String id, String udid) {
        MyApp.networkManager.makeRequest(new fbLoginRequest(id/*FBID*/,accessToken/*FBAccessToken*/,udid/*UDID*/), new NetworkCallback<fbLoginResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<fbLoginResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    User.setAccessToken(response.getData().getAccessToken());
                    User.setServerId(Long.parseLong(response.getData().getAppUser().getID()));
                    User.updateLocation(null);
                    User.updateFromServer(LoginActivity.this, new User.onUserUpdate() {
                        @Override
                        public void userUpdated(Reports reportObj) {
                            if(reportObj != null) {
                                Report.setReportID(Long.parseLong(reportObj.getId()));
                                Report.setMode(Static.ALERT_MODE);
                            }else{
                                Report.setReportID(0);
                                Report.setMode(Static.NORMAL_MODE);
                            }

                            Report.getReports(LoginActivity.this, null); // interface
                        }
                    }, null);
                } else {
                    showErrorDialog(getString(R.string.failure), errorDesc);
                }
            }
        });
    }

    @Override
    public void reportsUpdated() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onLoginToTwitter(String Token, String secret, String udid) {
        MyApp.networkManager.makeRequest(new TwiiterLoginRequest(Token,secret,udid/*UDID*/), new NetworkCallback<TwiiterLoginResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<TwiiterLoginResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    showDialog(getString(R.string.success), response.getData().toString());
                } else {
                    showErrorDialog(getString(R.string.failure), errorDesc);
                }
            }
        });
    }
}
