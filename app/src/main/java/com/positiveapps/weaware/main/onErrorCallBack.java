package com.positiveapps.weaware.main;

/**
 * Created by Izakos on 03/12/2015.
 */
public interface onErrorCallBack {
    public void onError(String errorMessage);
}
