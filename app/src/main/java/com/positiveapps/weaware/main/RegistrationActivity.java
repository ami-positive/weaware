package com.positiveapps.weaware.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.RegisterRequest;
import com.positiveapps.weaware.network.response.RegisterResponse;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.phone.PhoneNumberParser;
import com.positiveapps.weaware.util.FilterHelper;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener{

    private PhoneNumberParser mNumberParser;
    private TextView mCountryCodeView;
    private EditText mPhoneInput;
    private TextView mPrivacyTextView;
    private Spinner spinnerCountries;
    private String phone;
    private Button RegistrationSignUpBT;
    private EditText RegistrationUserNameET;
    private EditText RegistrationPassET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);

        RegistrationSignUpBT = (Button) findViewById(R.id.RegistrationSignUpBT);
        RegistrationPassET = (EditText) findViewById(R.id.RegistrationPassET);
        RegistrationUserNameET = (EditText) findViewById(R.id.RegistrationUserNameET);
        spinnerCountries = (Spinner) findViewById(R.id.spinner);
        mCountryCodeView = (TextView) findViewById(R.id.countryCode);
        mPhoneInput = (EditText) findViewById(R.id.phone);

        RegistrationSignUpBT.setOnClickListener(this);

        mPhoneInput.setFilters(new InputFilter[]{FilterHelper.onlyDigits});
        mNumberParser = new PhoneNumberParser(this, spinnerCountries, mCountryCodeView, mPhoneInput);
    }

    @Override
    public void onClick(View view) {

        boolean isOk = User.register(RegistrationUserNameET,
                    RegistrationPassET,
                    mPhoneInput,
                    mCountryCodeView,
                    mNumberParser);

        if(isOk){
            showProgressDialog();
            MyApp.networkManager.makeRequest(new RegisterRequest(
                    RegistrationUserNameET.getText().toString(), /*Username*/
                    RegistrationPassET.getText().toString(),/*Password*/
                    User.getUDID()/*UDID*/), new NetworkCallback<RegisterResponse>() {
                @Override
                public void onResponse(boolean success, String errorDesc, ResponseObject<RegisterResponse> response) {
                    super.onResponse(success, errorDesc, response);
                    if (success) {
                        User.setUserName(RegistrationUserNameET.getText().toString());
                        User.setAccessToken(response.getData().getAccessToken());
                        User.setServerId(Long.parseLong(response.getData().getID()));
                        dismissProgressDialog();
                        startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                        finish();
//                        showDialog(getString(R.string.success), User.getUserName() +
//                                        "\n" + User.getUDID() +
//                                        "\n" + User.getServerId() +
//                                        "\n" + User.getAccessToken() +
//                                        "\n" + User.getPhone() +
//                                        "\n\n" + response.getData()
//                        );
                    } else {
                        dismissProgressDialog();

                        //TODO exception in case of deleted account
                        //TODO remove parameter from dialog

                        showErrorDialog(getString(R.string.failure), /*User.getUserName() +
                                "\n" + User.getUDID() +
                                "\n" + User.getServerId() +
                                "\n" + User.getAccessToken() +
                                "\n" + User.getPhone() +
                                "\n\n" + */errorDesc);
                    }
                }
            });


        }
        }

    public void showDialog(final String title, final String msg) {

        if (!MyApp.showDialogFlag) {
            new Intent(RegistrationActivity.this, MainActivity.class);
            return;
        }

        if (isFinishing()) {return;}

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(RegistrationActivity.this)
                        .title(title)
                        .content(msg)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                                finish();
                            }
                        })
                        .show();
            }
        });
    }

    public void showErrorDialog(final String title, final String msg) {

        if (!MyApp.showErrorDialogFlag) {return;}

        if (isFinishing()) {return;}

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(RegistrationActivity.this)
                        .title(title)
                        .content(msg)
                        .positiveText(android.R.string.ok)
                        .show();
            }
        });
    }

    }

