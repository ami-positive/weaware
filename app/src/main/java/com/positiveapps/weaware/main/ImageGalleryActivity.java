package com.positiveapps.weaware.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;

import com.positiveapps.weaware.R;
import com.positiveapps.weaware.fragments.ScreenSlidePageFragment;
import com.positiveapps.weaware.util.ToastUtil;

public class ImageGalleryActivity extends BaseActivity {

    public static DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery);

        setupActionBar(R.id.app_actionbar);
        setUpDrawerNavigation();
        initPagerAdapter();

        if(getIntent().getExtras() == null){
            ToastUtil.toster(getString(R.string.no_image_to_show), false);
            finish();
        }

    }

    private void setUpDrawerNavigation() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout,getActionbarToolbar(), 0, 0){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void initPagerAdapter(){
        mPager = (ViewPager) findViewById(R.id.pager);

        Bundle b = getIntent().getExtras();
        String[] array = b.getStringArray(ReportMapActivity.IMAGES_ARRAY);
        int[] idsArray = b.getIntArray(ReportMapActivity.IMAGES_IDS_ARRAY);
        int position = b.getInt(ReportMapActivity.IMAGE_POSITION);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),idsArray, position);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(position);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private int[] array;
        private int position;

        public ScreenSlidePagerAdapter(FragmentManager fm, int[] array, int position) {
            super(fm);
            this.position = position;
            this.array = array;
        }

        @Override
        public Fragment getItem(int position) {
            return new ScreenSlidePageFragment().newInstance(array[position]);
        }

        @Override
        public int getCount() {
            return array.length;
        }
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }



}
