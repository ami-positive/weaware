package com.positiveapps.weaware.main;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.database.tables.TablePersons;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.closeRequest;
import com.positiveapps.weaware.network.requests.confirmRequest;
import com.positiveapps.weaware.network.requests.getReportsRequest;
import com.positiveapps.weaware.network.requests.removeImageRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.closeResponse;
import com.positiveapps.weaware.network.response.confirmResponse;
import com.positiveapps.weaware.network.response.getReportsResponse;
import com.positiveapps.weaware.network.response.removeImageResponse;
import com.positiveapps.weaware.objects.Images;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.ui.CircleImageView;
import com.positiveapps.weaware.ui.StarBitmapDrawable;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.Helper;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.Static;
import com.positiveapps.weaware.util.ToastUtil;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

import roboguice.inject.InjectView;

public class ReportMapActivity extends BaseActivity implements OnMapReadyCallback {


    private static final String LOG_TAG = "audio";
    public static final String IMAGES_ARRAY = "imagesArray";
    public static final String IMAGE_POSITION = "imagePosition";
    public static final String IMAGES_IDS_ARRAY = "imagesIdsArray";

    @InjectView(R.id.CopsNumberTV)
    private TextView CopsNumberTV;

    @InjectView(R.id.CitizenNumberTV)
    private TextView CitizenNumberTV;

    @InjectView(R.id.myLocation)
    private ImageView myLocation;

    private int counterCitizen = 1;
    private int counterCops = 1;
    private View includeReport;
    private LinearLayout playAudio;
    private LinearLayout showMap;
    private LinearLayout modeCancel;
    private LinearLayout modeConfirm;
    private LinearLayout modeConfirmed;
    private LinearLayout starscontainer;
    private CircleImageView reporterImage;
    private TextView reporterName;
    private TextView reportDescription;
    private boolean isPlayingAudio;

    private ActionBarDrawerToggle mDrawerToggle;
    private TextView reportTime;
    private MediaPlayer mPlayer;

    private GoogleMap googleMap;
    private String audioUrl;
    private LatLng location;
    private boolean isDescriptionShowing = true;

    private ArrayList<Images> images;
    private LinearLayout mainActionLayout;
    private LinearLayout player;
    private ImageView imageViewAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_map);
        AppUtil.printKeyHash(MyApp.appContext);
        setupActionBar(R.id.app_actionbar);
        setUpDrawerNavigation();
        setSosPhoneNumber();

        if (getIntent().getExtras() == null) {
            finish();
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        getIncludeViewsForAction();

       /* boolean showMap = getIntent().getExtras().getBoolean(Static.SHOW_MAP);

        if (showMap == true) { // show map in case of emergency notification
            if(googleMap != null){
                animateMap(googleMap, getReportLoaction());
            }
            isDescriptionShowing = false;
            includeReport.animate().translationY(includeReport.getHeight()).alpha(0f);
        }*/

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        addingMyLocationToMap(map);
        applyReportFromCursor(map);
        applyFriendsFromCursor(map);

//        onMyLocationClick(map);

    }

//    @Override
//    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//        if(b){
//            descriptionScrollView.setVisibility(View.GONE);
//        }else{
//            descriptionScrollView.setVisibility(View.VISIBLE);
//        }
//    }

    public void getIncludeViewsForAction() {

        includeReport = findViewById(R.id.reportActionInclude);

        playAudio = (LinearLayout) includeReport.findViewById(R.id.playAudio);
        imageViewAudio = (ImageView) includeReport.findViewById(R.id.imageViewAudio);
        showMap = (LinearLayout) includeReport.findViewById(R.id.showMap);
        modeCancel = (LinearLayout) includeReport.findViewById(R.id.modeCancel);
        modeConfirm = (LinearLayout) includeReport.findViewById(R.id.modeConfirm);
        modeConfirmed = (LinearLayout) includeReport.findViewById(R.id.modeConfirmed);
        player = (LinearLayout) includeReport.findViewById(R.id.player);
        mainActionLayout = (LinearLayout) includeReport.findViewById(R.id.mainActionLayout);
        starscontainer = (LinearLayout) includeReport.findViewById(R.id.starscontainer);
        reporterImage = (CircleImageView) includeReport.findViewById(R.id.reporterImage);
        reporterName = (TextView) includeReport.findViewById(R.id.reporterName);
        reportDescription = (TextView) includeReport.findViewById(R.id.reportDescription);
        reportTime = (TextView) includeReport.findViewById(R.id.reportTime);
//        ((org.firezenk.audiowaves.Visualizer) includeReport.findViewById(R.id.visualizer)).startListening();

        long databaseId = getIntent().getExtras().getLong(Static.REPORT_ID);
        final Report report = Report.getReportByDbId(databaseId);
        reportTime.setText(report.getDateEndCustom());
        reporterName.setText(report.getReporterName());

        if(!report.getDescription().isEmpty()) {
            reportDescription.setText(report.getDescription());
        }
        if(!report.getAudioAttached().isEmpty()) {
            imageViewAudio.setImageResource(R.drawable.audio);
        }else{
            imageViewAudio.setImageResource(R.drawable.audio_grey);
        }


        ImageLoader.ByUrl(report.getReporterImage(), reporterImage, R.drawable.friends_icon_map);

        starsContainer(this, starscontainer, report.getReporterNumStars(), Color.parseColor(Helper.getColorByRank(report.getReporterRank())));
        setAudioUrl(report.getAudioAttached());
        setReportLocation(new LatLng(report.getLat(), report.getLon()));

        if(report.getServerId() != Report.getReportID()){
            modeCancel.setVisibility(View.GONE);
//            System.out.println("report.getConfirm() " + report.getConfirm());
            if(report.getConfirm() == 1) {
                Log.i("aa", "getIncludeViewsForAction 1");
                modeConfirm.animate().alpha(0f).rotationY(90f);
                modeConfirmed.animate().alpha(1f).rotationY(0);
            }else{
                modeConfirm.animate().alpha(1f).rotationY(0f);
                modeConfirmed.animate().alpha(0f).rotationY(90f);
                Log.i("aa", "getIncludeViewsForAction 2");
            }
        }
        setConfirmationClickListener(report.getServerId(), report.getDatabaseId(), modeCancel, modeConfirm, modeConfirmed);

        getReportImagesQuery(report.getServerId());

        View.OnClickListener onAction = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.playAudio:
                        if(!getAudioUrl().isEmpty()) {
                            onPlaying(isPlayingAudio, getAudioUrl());
//                            animateAudio(player, playAudio, showMap, mainActionLayout, isPlayingAudio ? true : false);

                        }else{
                            ToastUtil.toster(getString(R.string.no_audio_attached) , false);
                        }
                        break;
                    case R.id.showMap:
                        if(googleMap != null){
                            animateMap(googleMap, getReportLoaction());
                        }
                        isDescriptionShowing = false;
                        includeReport.animate().translationY(includeReport.getHeight()).alpha(0f);
                        break;
                }
            }
        };

        playAudio.setOnClickListener(onAction);
        showMap.setOnClickListener(onAction);

    }

    private void animateAudio(View player, View audio, View map, View mainActionLayout, boolean toOpen){
        if(toOpen) {
            audio.animate().translationX(audio.getWidth() * -1);
            map.animate().translationX(map.getWidth() * -1);
            mainActionLayout.animate().translationX(mainActionLayout.getWidth());
            player.getLayoutParams().width = audio.getWidth() + map.getWidth();
            player.requestLayout();
        }else{
            audio.animate().translationX(0);
            map.animate().translationX(0);
            mainActionLayout.animate().translationX(0);
            player.getLayoutParams().width = 0;
        }
    }

    private void setAudioUrl(String url){
        audioUrl = url;
    }

    private String getAudioUrl(){
        if(audioUrl != null){
            return audioUrl;
        }
        return "";
    }

    private void setReportLocation(LatLng location){
        this.location = location;
    }

    private LatLng getReportLoaction(){
        LatLng myLocation = new LatLng(User.getLat(), User.getLon());
        if(location != null){
            if(location.latitude == 0.0 && location.longitude == 0.0) {
                return myLocation;
            }else {
                return location;
            }
        }
        return myLocation;
    }

    private void onPlaying(boolean isPlaying, String urlAudio){
        if(!isPlaying){
            startPlaying(urlAudio);
        }else{
            stopPlaying();
        }
        isPlayingAudio = !isPlayingAudio;
    }

    private void startPlaying(String url) {
        mPlayer = new MediaPlayer();
        showProgressDialog("loading audio");
        try {
            mPlayer.setDataSource(url);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.prepareAsync();
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    dismissProgressDialog();
                    mPlayer.start();
                }
            });

        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

//    int totalSize;
//    void downloadFile(){
//
//        try {
//            URL url = new URL(dwnload_file_path);
//            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//
//            urlConnection.setRequestMethod("GET");
//            urlConnection.setDoOutput(true);
//
//            //connect
//            urlConnection.connect();
//
//            //set the path where we want to save the file
//            File SDCardRoot = Environment.getExternalStorageDirectory();
//            //create a new file, to save the downloaded file
//            File file = new File(SDCardRoot,"downloaded_file.png");
//
//            FileOutputStream fileOutput = new FileOutputStream(file);
//
//            //Stream used for reading the data from the internet
//            InputStream inputStream = urlConnection.getInputStream();
//
//            //this is the total size of the file which we are downloading
//            totalSize = urlConnection.getContentLength();
//
//            runOnUiThread(new Runnable() {
//                public void run() {
//                    pb.setMax(totalSize);
//                }
//            });
//
//            //create a buffer...
//            byte[] buffer = new byte[1024];
//            int bufferLength = 0;
//
//            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
//                fileOutput.write(buffer, 0, bufferLength);
//                downloadedSize += bufferLength;
//                // update the progressbar //
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        pb.setProgress(downloadedSize);
//                        float per = ((float)downloadedSize/totalSize) * 100;
//                        cur_val.setText("Downloaded " + downloadedSize + "KB / " + totalSize + "KB (" + (int)per + "%)" );
//                    }
//                });
//            }
//            //close the output stream when complete //
//            fileOutput.close();
//            runOnUiThread(new Runnable() {
//                public void run() {
//                    // pb.dismiss(); // if you want close it..
//                }
//            });
//
//        } catch (final MalformedURLException e) {
//            showError("Error : MalformedURLException " + e);
//            e.printStackTrace();
//        } catch (final IOException e) {
//            showError("Error : IOException " + e);
//            e.printStackTrace();
//        }
//        catch (final Exception e) {
//            showError("Error : Please check your internet connection " + e);
//        }
//    }


    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    public void setConfirmationClickListener(final long serverId, long dbId, final View...views){
        // constant

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == views[0].getId()){  // cancel
                    cancelReport();
                    return;
                }else if(view.getId() == views[1].getId()){ // confirm
                    confirmReport(serverId, views[1], views[2]);
                    return;
                }else if(view.getId() == views[2].getId()){ // confirmed
                    ToastUtil.toster("You already confirmed the report", false);
                    return;
                }
            }
        };

        for (int i = 0; i < views.length; i++) {
            views[i].setOnClickListener(listener);
        }
    }

    private void changeLayouts(int mode, View...view){
        for (int i = 0; i < view.length; i++) {
            if(i == mode){
                view[i].animate().translationX(0f);
            }else{
                view[i].animate().translationX(view[i].getWidth());
            }
        }
    }

    private void starsContainer(Context context, ViewGroup container, int numStars, int colorLevel){
        ImageView image = null;
        int size = (int) AppUtil.convertDpToPixel(20f, MyApp.appContext);

        for (int i = 0; i < numStars ; i++) {
            image = new ImageView(context);
                    Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
                    bitmap.eraseColor(colorLevel);
                    image.setImageDrawable(new StarBitmapDrawable(bitmap));
                    container.addView(image);
        }

        for (int i = 0; i < (5 - numStars); i++){
            image = new ImageView(context);
            Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(Color.GRAY);
            image.setImageDrawable(new StarBitmapDrawable(bitmap));
            container.addView(image);
        }

    }

    private void applyReportFromCursor(GoogleMap map) {

        long databaseId = getIntent().getExtras().getLong(Static.REPORT_ID);
        Report report = Report.getReportByDbId(databaseId);
        addingReportLocationToMap(report, map);
//        Cursor reportCursor = MyApp.tableReports.readCursor(TableReports.SERVER_ID + "=" + databaseId, TableReports.COLUMN_ID);
//        if (reportCursor.moveToFirst()) {
//            do {
//                String name = reportCursor.getString(reportCursor.getColumnIndex(TableReports.REPORTER_NAME));
//                String description = reportCursor.getString(reportCursor.getColumnIndex(TableReports.DESCRIPTION));
//                String audio = reportCursor.getString(reportCursor.getColumnIndex(TableReports.AUDIO_ATTACHED));
//                String photo = reportCursor.getString(reportCursor.getColumnIndex(TableReports.PHOTO_ATTACHED));
//                String lat = reportCursor.getString(reportCursor.getColumnIndex(TableReports.LAT));
//                String lng = reportCursor.getString(reportCursor.getColumnIndex(TableReports.LON));
//
//
//                reporterName.setText(name);
//                reportDescription.setText(description);
//                ImageLoader.ByUrl(photo, reporterImage);
//                addingReportLocationToMap(lat, lng, map);
//            } while (reportCursor.moveToNext());
//        }
//        reportCursor.close();
    }

    private void applyFriendsFromCursor(GoogleMap map) {

        Cursor personCursor = MyApp.tablePersons.readCursor(TablePersons.COLUMN_ID);
        if (personCursor.moveToFirst()) {
            do {
                Person person = MyApp.tablePersons.cursorToEntity(personCursor);
                String friendName = personCursor.getString(personCursor.getColumnIndex(TablePersons.USER_NAME));
                int category = personCursor.getInt(personCursor.getColumnIndex(TablePersons.CATEGORY));
                String lat = personCursor.getString(personCursor.getColumnIndex(TablePersons.LAT));
                String lon = personCursor.getString(personCursor.getColumnIndex(TablePersons.LNG));
                addingPersonsToMap(person, lat, lon, category, map);

            } while (personCursor.moveToNext());
        }
        personCursor.close();
    }

    private void addingReportLocationToMap(Report report, GoogleMap map) {
        LatLng location = new LatLng(report.getLat(),  report.getLon());
        System.out.println("latlan " + location);
       final String reportTitle = report.getReporterName() + getString(R.string.report);
        String reportDescription = "for more details press here";
        final Marker reportMarker = map.addMarker(new MarkerOptions()
                .position(location)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.report_location))
                .snippet(reportDescription)
                .title(reportTitle));
        animateMap(map, location);
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                System.out.println("onInfoWindowClick");
                System.out.println("marker " + marker);
                System.out.println("reportMarker " + reportMarker);
                if (marker.getTitle().matches(reportTitle)) {
                    includeReport.animate().translationY(0).alpha(1f);
                }
            }
        });
    }

    private void addingMyLocationToMap(GoogleMap map) {
        LatLng location = new LatLng(User.getLat(), User.getLon());
        map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromResource(R.drawable.my_locatioan_blue)));
    }

    private void addingPersonsToMap(Person person, String lat, String lon, int category, GoogleMap map) {
        if(Double.parseDouble(lat) == 0.0 && Double.parseDouble(lon) == 0.0) {
            return;
        }
        LatLng location = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
        switch (category) {
            case Person.CITIZEN:
                CitizenNumberTV.setText(counterCitizen + "");
                counterCitizen++;
                IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
                View multiProfile = getLayoutInflater().inflate(R.layout.friend_marker, null);
                mIconGenerator.setContentView(multiProfile);
                CircleImageView mClusterImageView = (CircleImageView) multiProfile.findViewById(R.id.profileImage);
                mClusterImageView.setImageDrawable(person.getProfileImageDrawable());
                mIconGenerator.setBackground(new ColorDrawable(Color.TRANSPARENT));
                Bitmap icon = mIconGenerator.makeIcon();
                map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(icon)));
                break;
            case Person.POLICEMAN:
                CopsNumberTV.setText(counterCops + "");
                counterCops++;
                map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromResource(R.drawable.police_location)));
                break;
        }
    }

    public void onMyLocationClick(final GoogleMap map) {
        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng location = new LatLng(User.getLat(), User.getLon());
                map.moveCamera(CameraUpdateFactory.newLatLng(location));
                map.animateCamera(CameraUpdateFactory.zoomTo(15));
            }
        });
    }

    private void buildHorizontalGallery(Context context,boolean addTextView,  ArrayList<Images> imagesArr) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.linear);
        layout.removeAllViews();
        String[] images = new String[imagesArr.size()];
        int[] imagesIds = new int[imagesArr.size()];
        for (int i = 0; i < imagesArr.size() ; i++) {
            images[i] = imagesArr.get(i).getUrl();
            imagesIds[i] = Integer.parseInt(imagesArr.get(i).getId());
        }

        for (int i = 0; i < imagesArr.size(); i++) {

            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            ImageView imageView = new ImageView(context);
            imageView.setId(Integer.parseInt(imagesArr.get(i).getId()));
            int imagePadding = (int) AppUtil.convertDpToPixel(2, context);
            imageView.setPadding(imagePadding, imagePadding, imagePadding, imagePadding);

            int size = (int) AppUtil.convertDpToPixel(120, context);
            Picasso.with(MyApp.appContext).load(imagesArr.get(i).getUrl()).resize(size, size).into(imageView);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            layout.addView(frameLayout);
            frameLayout.addView(imageView);
            if(addTextView) {
                frameLayout.addView(getTextView(this, Integer.parseInt(imagesArr.get(i).getId())));
            }
            setGalleryClickListener(imageView.getId(), images, imagesIds, imageView);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public TextView getTextView(Context context, final int id) {
        GradientDrawable shape = new GradientDrawable();
        float cornerAngle = AppUtil.convertDpToPixel(20, context);
        shape.setCornerRadius(cornerAngle);
        shape.setColor(Color.BLACK);
        TextView textView = new TextView(context);
        textView.setText("x");
        textView.setClickable(true);
        int size = (int) AppUtil.convertDpToPixel(40, context);
        FrameLayout.LayoutParams lp = (new FrameLayout.LayoutParams(size, size));
        int textMargin = (int) AppUtil.convertDpToPixel(5, context);
        lp.setMargins(textMargin, textMargin, textMargin, textMargin);
        lp.gravity = Gravity.RIGHT;
        textView.setLayoutParams(lp);
        textView.setBackground(shape);
        textView.setTextColor(Color.WHITE);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setGravity(Gravity.CENTER);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeImageQuery(id);
            }
        });
        textView.setId(id);
        return textView;
    }

    public void setGalleryClickListener(final int position, final String[] images, final int[] imagesIds,  final View view) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToastUtil.toster("id " + view.getId(), false);

//                if (view.getId() == views[0].getId()) {
//                    ToastUtil.toster("pic " + view.getId(), false);

                    Bundle b= new Bundle();
                    b.putStringArray(IMAGES_ARRAY, images);
                    b.putIntArray(IMAGES_IDS_ARRAY, imagesIds);
                    b.putInt(IMAGE_POSITION, position);
                    Intent i= new Intent(ReportMapActivity.this, ImageGalleryActivity.class);
                    i.putExtras(b);
                    startActivity(i);

                    return;
//                } else if (view.getId() == views[1].getId()) {
//                    ToastUtil.toster("delete " + view.getId(), false);
//                    return;
//                }
            }
        };

        view.setOnClickListener(listener);

    }



    private void animateMap(GoogleMap map, LatLng LatLng){


        // Move the camera instantly to Sydney with a zoom of 15.
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng, 15));

        /*
        // Zoom in, animating the camera.
                map.animateCamera(CameraUpdateFactory.zoomIn());
        */

        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        map.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null);
        /*
        // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(LatLng)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                        .bearing(90)                // Sets the orientation of the camera to east
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        */


    }

    private void setUpDrawerNavigation() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout, getActionbarToolbar(), 0, 0) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }
    public void cancelReport(){
        MyApp.networkManager.makeRequest(new closeRequest(String.valueOf(Report.getReportID())/*ID*/), new NetworkCallback<closeResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<closeResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    Report report = Report.getCurrentReport();
                    MyApp.tableReports.delete(report.getDatabaseId(), report);
                    Report.setMode(Static.NORMAL_MODE);
                    Report.setReportID(0);
                    ToastUtil.toster("report canceled", true);
                    startActivity(new Intent(ReportMapActivity.this, MainActivity.class));
                    finish();
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public void confirmReport(long reportId, final View confirm, final View confirmed){
        MyApp.networkManager.makeRequest(new confirmRequest(String.valueOf(reportId)/*ID*/), new NetworkCallback<confirmResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<confirmResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    modeConfirm.animate().alpha(0f).rotationY(90f);
                    modeConfirmed.animate().alpha(1f).rotationY(0);
                    ToastUtil.toster("Report confirmed", true);
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public void removeImageQuery(final int imageID){
        showProgressDialog();
        MyApp.networkManager.makeRequest(new removeImageRequest(String.valueOf(imageID)/*ImageID*/), new NetworkCallback<removeImageResponse>() {
                    @Override
                    public void onResponse(boolean success, String errorDesc, ResponseObject<removeImageResponse> response) {
                        super.onResponse(success, errorDesc, response);
                        if (success) {
                            Report report = Report.getCurrentReport();
                            for (int i = 0; i < images.size(); i++) {
                                if(Integer.parseInt(images.get(i).getId()) == imageID){
                                    images.remove(i);
                                    buildHorizontalGallery(ReportMapActivity.this, true, images);
                                    ToastUtil.toster("Image deleted", true);
                                    break;
                                }
                            }
                            dismissProgressDialog();
                        } else {
                            ToastUtil.toster(errorDesc, true);
                            dismissProgressDialog();
                        }
                    }
                });
    }

    public void getReportImagesQuery(final long serverID){
        MyApp.networkManager.makeRequest(new getReportsRequest(String.valueOf(serverID)/*ID*/), new NetworkCallback<getReportsResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<getReportsResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    boolean isUserReport = (serverID == Report.getReportID());
                    images = response.getData().getImages();
                    buildHorizontalGallery(ReportMapActivity.this, isUserReport, images);

                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });

    }



    @Override
    public void onBackPressed() {
        if(!isDescriptionShowing) {
            includeReport.animate().translationY(0).alpha(1f);
            isDescriptionShowing = true;
            return;
        }
        startActivity(new Intent(this, MainActivity.class));
        super.onBackPressed();
    }

}
