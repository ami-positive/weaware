package com.positiveapps.weaware.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.dialogs.DialogCallback;
import com.positiveapps.weaware.dialogs.DialogManager;
import com.positiveapps.weaware.fragments.MainFragment;
import com.positiveapps.weaware.fragments.ReportsFragment;
import com.positiveapps.weaware.gcm.GcmLoader;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.Static;

import roboguice.inject.InjectView;


public class MainActivity extends BaseActivity{

    @InjectView (R.id.HHH)
    public TextView HHH;

    public static TextView helloWorld;
    private ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setupActionBar(R.id.app_actionbar);
        setUpDrawerNavigation();
//        Person.populateTable();
        Person.saveImagesAsBlob();
        setSosPhoneNumber();

        GcmLoader.set(this);


        if(getIntent().getExtras() != null){
            long serverID = getIntent().getExtras().getLong(Static.REPORT_SERVER_ID);
            int pushType = getIntent().getExtras().getInt(Static.PUSH_TYPE);

            Report report = Report.getReportById(serverID);
            if(report != null){
                switch (pushType){
                    case Static.OPEN_EVENT:
                        final Intent intent = new Intent(this, ReportMapActivity.class);
                        intent.putExtra(Static.REPORT_ID, report.getDatabaseId());
                        intent.putExtra(Static.SHOW_MAP, true);
                        startActivity(intent);
                        finish();
                        break;
                    case Static.CLOSE_EVENT:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.main_container, ReportsFragment.newInstance())
                                        .commit();
                            }
                        });
                        break;
                    case Static.CONFIRM_EVENT:
                        Intent intent1 = new Intent(this, ReportMapActivity.class);
                        intent1.putExtra(Static.REPORT_ID, report.getDatabaseId());
                        startActivity(intent1);
                        finish();
                        break;
                }
            }
        }

         if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_container, MainFragment.newInstance())
                        .commit();
            }

//        throw new RuntimeException("This is a crash");

    }


    private void setUpDrawerNavigation() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout,getActionbarToolbar(), 0, 0){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void showErrorDialog (String error){
        DialogManager.showErrorDialog(getSupportFragmentManager(), error, new DialogCallback() {
            @Override
            protected void onDialogButtonPressed(int buttonID, DialogFragment dialog) {
                super.onDialogButtonPressed(buttonID, dialog);
                AppUtil.startLocationService(MainActivity.this);
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onStart() {
        super.onStart();
        AppUtil.startLocationService(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppUtil.stopLocationService(this);
    }


    @Override
    public void onBackPressed() {
        if(currentFragment instanceof MainFragment){
            showDialog(getString(R.string.app_name), getString(R.string.are_you_sure_you_want_to_exit));
        }else {
            super.onBackPressed();
        }
    }

    public void showDialog(final String title, final String msg) {

        if (isFinishing()) {return;}

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(MainActivity.this)
                        .title(title)
                        .content(msg)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                               finish();
                            }
                        })
                        .show();
            }
        });
    }
}
