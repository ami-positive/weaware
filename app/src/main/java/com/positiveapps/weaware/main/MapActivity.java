package com.positiveapps.weaware.main;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.database.tables.TablePersons;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.UploadAudioRequest;
import com.positiveapps.weaware.network.requests.closeRequest;
import com.positiveapps.weaware.network.requests.saveReportRequest;
import com.positiveapps.weaware.network.requests.uploadImageRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.UploadAudioResponse;
import com.positiveapps.weaware.network.response.closeResponse;
import com.positiveapps.weaware.network.response.saveReportResponse;
import com.positiveapps.weaware.network.response.uploadImageResponse;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.objects.TimerListener;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.ui.CircleImageView;
import com.positiveapps.weaware.ui.MultiDrawable;
import com.positiveapps.weaware.ui.Vmark;
import com.positiveapps.weaware.util.AppUtil;
import com.positiveapps.weaware.util.BitmapUtil;
import com.positiveapps.weaware.util.DateUtil;
import com.positiveapps.weaware.util.Helper;
import com.positiveapps.weaware.util.ImageLoader;
import com.positiveapps.weaware.util.Static;
import com.positiveapps.weaware.util.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit.mime.TypedFile;

;

public class MapActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener, TimerListener,
        ClusterManager.OnClusterClickListener<Person>, ClusterManager.OnClusterInfoWindowClickListener<Person>, ClusterManager.OnClusterItemClickListener<Person>, ClusterManager.OnClusterItemInfoWindowClickListener<Person> {

    private static final int RED_MARKER = 11;
    private static final int BLUE_MARKER = 22;
    private static final String LOG_TAG = "AudioRecordTest";
    private static String mFileName = null;
    private TextView mapCopsNumberTV;
    private TextView mapCitizenNumberTV;
    private ImageView myLocationMap;
    private ImageView reportAudio;
    private ImageView reportCamera;
    private Button reportStart;
    private ImageView reportComment;
    private LinearLayout reportCancel;
    private EditText reportDescription;
    private int counterCitizen = 1;
    private int counterCops = 1;
    private String imageBase64String;
    private ImageView captureIV;
    private LinearLayout imageLayout;
    private Button cancelPhoto;
    private Button confirmPhoto;
    private ClusterManager<Person> mClusterManager;
    private View includeReport;
    private Random mRandom = new Random(1984);
    private EditText et;
    private ImageView recordStop;
    private Marker myMarker;
    private LinearLayout pushedByET;
    private ImageView sendText;
    private Chronometer myChronometer;
    private GoogleMap gMap;
    //    private RecordButton mRecordButton = null;
    private MediaRecorder mRecorder = null;

    //    private PlayButton   mPlayButton = null;
    private MediaPlayer mPlayer = null;

    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout audioRecordingLayout;
    private LinearLayout mainReportLayout;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        setupActionBar(R.id.app_actionbar);
        setUpDrawerNavigation();
        setSosPhoneNumber();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapCopsNumberTV = (TextView) findViewById(R.id.mapCopsNumberTV);
        mapCitizenNumberTV = (TextView) findViewById(R.id.mapCitizenNumberTV);
        myLocationMap = (ImageView) findViewById(R.id.myLocationMap);
        captureIV = (ImageView) findViewById(R.id.captureIV);
        confirmPhoto = (Button) findViewById(R.id.confirmPhoto);
        cancelPhoto = (Button) findViewById(R.id.cancelPhoto);
        reportStart = (Button) findViewById(R.id.reportStart);
        imageLayout = (LinearLayout) findViewById(R.id.imageLayout);

        confirmPhoto.setOnClickListener(this);
        cancelPhoto.setOnClickListener(this);
        reportStart.setOnClickListener(this);

        mapFragment.getMapAsync(this);

        getViewsForAction();

        mode(Report.getmode()); // set current mode

        InitAnalytic(); //

        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audios.3gp";

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // changing main start report button background
        int color = Color.parseColor(MyApp.userProfile.geLevelColor().replace("#", "#59")); // set transparency
        if (Build.VERSION.SDK_INT >= 16) {
            reportStart.setBackground(Helper.bigButton(color));
        } else {
            reportStart.setBackgroundDrawable(Helper.bigButton(color));
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gMap = map;

        addingMyLocationToMap(map);
        setCluster(map);
//        applyFriendsFromCursor(map);

//        onMyLocationClick(map);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap image = null;
        if (resultCode != -1) {
            return;
        }

        switch (requestCode) {
            case Static.CAMERA_REQUEST_CODE:
                File file = new File(Environment.getExternalStorageDirectory().getPath(), Static.CAMERA_PROFILE_PATH);
                image = BitmapUtil.loadImageIntoByStringPath(file.getAbsolutePath(), captureIV, 300, 2, ImageLoader.DEAFULT_PLACE_HOLDER);
                imageBase64String = Helper.getProfileImageAsBase64(image);
                uploadImageQuery(Report.getReportID(), imageBase64String);
                break;
            default:
                break;
        }


    }

    public void getViewsForAction() {

        includeReport = findViewById(R.id.mapActionInclude);
        includeReport.setAlpha(0f);

        reportAudio = (ImageView) includeReport.findViewById(R.id.reportAudio);
        reportCamera = (ImageView) includeReport.findViewById(R.id.reportCamera);
        reportComment = (ImageView) includeReport.findViewById(R.id.reportComment);
        reportCancel = (LinearLayout) includeReport.findViewById(R.id.reportCancel);
        recordStop = (ImageView) includeReport.findViewById(R.id.recordStop);
        et = (EditText) includeReport.findViewById(R.id.et);
        sendText = (ImageView) includeReport.findViewById(R.id.sendText);
        pushedByET = (LinearLayout) includeReport.findViewById(R.id.pushedByET);
        mainReportLayout = (LinearLayout) includeReport.findViewById(R.id.mainReportLayout);
        audioRecordingLayout = (LinearLayout) includeReport.findViewById(R.id.audioRecordingLayout);
        myChronometer = (Chronometer) findViewById(R.id.chronometer);
//        reportDescription = (EditText) includeReport.findViewById(R.id.reportDescription);

        boolean isEditTextShown;
        View.OnClickListener onAction = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.reportAudio:
                        reportAudio.setClickable(false);
                        recordStop.setClickable(true);
                        animateAudioLayout(true, audioRecordingLayout, mainReportLayout);
                        startRecording();
                        break;
                    case R.id.reportCamera:
                        Helper.openCamera(MapActivity.this);
                        break;
                    case R.id.reportCancel:
                        showCancelDialog();
                        break;
                    case R.id.reportComment:
                        animateEditText(true, reportComment, et, pushedByET, sendText);
                        break;
                    case R.id.sendText:
                        AppUtil.hideKeyBoard(et);
                        if (Report.getCurrentReport() != null && !et.getText().toString().isEmpty()) {
                            saveReportQuery(Report.getCurrentReport(), et.getText().toString());
                        }
                        animateEditText(false, reportComment, et, pushedByET, sendText);
                        break;
                    case R.id.recordStop:
                        reportAudio.setClickable(true);
                        recordStop.setClickable(false);
                        stopRecording();
                        animateAudioLayout(false, audioRecordingLayout, mainReportLayout);
                        break;
                }
            }
        };

        sendText.setOnClickListener(onAction);
        reportAudio.setOnClickListener(onAction);
        reportCamera.setOnClickListener(onAction);
        reportCancel.setOnClickListener(onAction);
        reportComment.setOnClickListener(onAction);
        recordStop.setOnClickListener(onAction);
    }

    private void animateEditText(boolean b, ImageView reportComment, EditText et, LinearLayout pushedByET, ImageView sendText) {
        if (b) {
            reportComment.animate().rotationY(90f).translationY(reportComment.getHeight());
            et.animate().translationX(0).rotationY(0f);
            pushedByET.animate().translationX(-pushedByET.getWidth());
            sendText.animate().rotationY(0f).translationY(0f);
        } else {
            reportComment.animate().rotationY(0f).translationY(0f);
            et.setText("");
            et.animate().translationX(et.getWidth()).rotationY(90f);
            pushedByET.animate().translationX(0f);
            sendText.animate().rotationY(90f).translationY(sendText.getHeight());
        }
    }

    private void rotateView(int mode, View... views) {
        for (int i = 0; i < views.length; i++) {
            if (i == mode) {
                views[i].animate().rotationY(90f).alpha(0);
            } else {
                views[i].animate().rotationY(0).alpha(1).translationX(0);
            }
        }

//        rx.Observable.just("").map(new Func1<String, User>() {
//
//            @Override
//            public User call(String s) {
//                return null;
//            }
//        }).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<User>() {
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onNext(User user) {
//
//            }
//        });
    }

    private void animateAudioLayout(boolean startRec, View stop, View play) {
        if (startRec) {
            stop.animate().rotationX(0).translationY(stop.getHeight());
            play.animate().translationY(play.getHeight());
        } else {
            stop.animate().rotationX(90).translationY(0);
            play.animate().translationY(0);
        }
    }

    private void startReport() {
        Helper.play(R.raw.beep_short_on);
        Report report = new Report();
        report.setReportDate(DateUtil.getCurrentTimeStamp());
        report.setReporterName(User.getFullName());
        report.setLat(User.getLat());
        report.setLon(User.getLon());
        report.setReporterImage(User.getProfileImage());
        saveNewReportQuery(report);
        mode(Static.ALERT_MODE);
    }

    public void mode(int mode) {
        switch (mode) {
            case Static.ALERT_MODE:
                originalActionbarColor(false);
                Report.setReportTimer(this);
                Report.startReportTimer();
                Report.setMode(Static.ALERT_MODE);
                setMarker(RED_MARKER);
                includeReport.animate().alpha(1f).translationY(0);
                reportStart.animate().alpha(0f).rotationY(90f);
//                reportStart.setVisibility(View.GONE);
//                reportCancel.setVisibility(View.VISIBLE);

                final View view = findViewById(android.R.id.content);
                view.getViewTreeObserver().addOnGlobalLayoutListener(
                        new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                initReportStatus();
                            }

                        });


                break;
            case Static.NORMAL_MODE:
                originalActionbarColor(true);
                Report.stopReportTimer();
                Report.setMode(Static.NORMAL_MODE);
                setMarker(BLUE_MARKER);
                includeReport.animate().alpha(0f).translationY(includeReport.getHeight());
                reportStart.animate().alpha(1f).rotationY(0f);
                ;
//                reportStart.setVisibility(View.VISIBLE);
//                reportCancel.setVisibility(View.GONE);

                new Vmark().remove(reportAudio, R.drawable.mic);
                new Vmark().remove(reportCamera, R.drawable.camera);
                new Vmark().remove(reportComment, R.drawable.description);

                break;
        }
    }

    private void initReportStatus() {
        Report report = Report.getCurrentReport();
        if (report == null) {
            return;
        }
        if (!report.getAudioAttached().isEmpty()) {
            new Vmark().set(reportAudio, R.drawable.mic, true, false);
        }
        if (!report.getDescription().isEmpty()) {
            new Vmark().set(reportComment, R.drawable.description, true, false);
        }
        if (!report.getPhotoAttached().isEmpty()) {
            new Vmark().set(reportCamera, R.drawable.camera, true, false);
        }
    }

    private void setMarker(int markerColor) {
        if (gMap != null) {
            addingMyLocationToMap(gMap);
        }
    }

    private void addingMyLocationToMap(GoogleMap map) {
        if (myMarker != null) {
            myMarker.remove();
        }
        LatLng location = new LatLng(User.getLat(), User.getLon());
        int res = R.drawable.my_locatioan_blue;
        if (Report.getmode() == Static.ALERT_MODE) {
            res = R.drawable.my_locatioan_red;
        }
        myMarker = map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromResource(res)));

        animateMap(map, location, Report.getmode() == Static.ALERT_MODE ? true : false);
    }

    private void applyFriendsFromCursor(GoogleMap map) {
        Cursor personCursor = MyApp.tablePersons.readCursor(TablePersons.COLUMN_ID);
        if (personCursor.moveToFirst()) {
            do {
                Person person = MyApp.tablePersons.cursorToEntity(personCursor);
                addingPersonsToMap(person, map);

            } while (personCursor.moveToNext());
        }
        personCursor.close();
    }

    private void addItemes() {
        mClusterManager.clearItems();
        Cursor personCursor = MyApp.tablePersons.readCursor(TablePersons.COLUMN_ID);
        if (personCursor.moveToFirst()) {
            do {
                Person person = MyApp.tablePersons.cursorToEntity(personCursor);
                if (!(person.getLat() == 0.0 && person.getLng() == 0.0)) {
                    if (person.getCategory() == Person.POLICEMAN) {
                        mapCopsNumberTV.setText(String.valueOf(counterCops));
                        counterCops++;
                    } else {
                        mapCitizenNumberTV.setText(String.valueOf(counterCitizen));
                        counterCitizen++;
                    }
                    mClusterManager.addItem(person);
                }

            } while (personCursor.moveToNext());
        }
        personCursor.close();
    }

    private void addingPersonsToMap(Person person, GoogleMap map) {
        if (person.getLat() == 0.0 && person.getLng() == 0.0) {
            return;
        }
        LatLng location = new LatLng(person.getLat(), person.getLng());
        System.out.println("location == >> " + location);
        switch (person.getCategory()) {
            case Person.CITIZEN:
                mapCitizenNumberTV.setText(counterCitizen + "");
                counterCitizen++;
                IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
                View multiProfile = getLayoutInflater().inflate(R.layout.friend_marker, null);
                mIconGenerator.setContentView(multiProfile);
                CircleImageView mClusterImageView = (CircleImageView) multiProfile.findViewById(R.id.profileImage);
                mClusterImageView.setImageDrawable(person.getProfileImageDrawable());
                mIconGenerator.setBackground(new ColorDrawable(Color.TRANSPARENT));
                Bitmap icon = mIconGenerator.makeIcon();
                map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(icon)));
                break;
            case Person.POLICEMAN:
                mapCopsNumberTV.setText(counterCops + "");
                counterCops++;
                map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromResource(R.drawable.police_location)));
                break;
        }
    }

    public void onMyLocationClick(final GoogleMap map) {
        myLocationMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng location = new LatLng(User.getLat(), User.getLon());
                map.moveCamera(CameraUpdateFactory.newLatLng(location));
                map.animateCamera(CameraUpdateFactory.zoomTo(15));
            }
        });
    }


//    public void showLoadingDialog() {
//        Dialog loadingDialog = new Dialog(this, R.style.cancelDialog);
//        final View view = getLayoutInflater().inflate(R.layout.dialog_cancel_alert, null);
//        ((RadioGroup) view.findViewById(R.id.cancelDialogButtons)).setOnCheckedChangeListener(cancelAlertListener(loadingDialog));
//        loadingDialog.setContentView(view);
////		loadingDialog.setCancelable(false);
//        loadingDialog.show();
//    }

    private RadioGroup.OnCheckedChangeListener cancelAlertListener(final Dialog dialog) {
        RadioGroup.OnCheckedChangeListener radioClick = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.confirmCancel:
                        Helper.play(R.raw.beep_short_off);
                        cancelReport();
                        dialog.dismiss();
                        break;
                    case R.id.noCancel:
                        dialog.dismiss();
                        break;
                }
            }
        };

        return radioClick;
    }

    public void showCancelDialog() {

        if (isFinishing()) {
            return;
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(MapActivity.this)
                        .title(R.string.cancel_report)
                        .content(R.string.are_you_sure_you_want_to_cancel_the_current_report)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                Helper.play(R.raw.beep_short_off);
                                cancelReport();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelPhoto:
                break;
            case R.id.confirmPhoto:
                imageLayout.setVisibility(View.GONE);
                break;
            case R.id.reportStart:
                startReport();
                break;
        }
    }

    @Override
    public void onReportTimeOut() {
        mode(Static.NORMAL_MODE);
    }

    public void capturePicture(long reportId, String reportBase64Pic) {
        //TODO send the picture to server with reportId
    }

    public void captureAudio(long reportId, String reportBase64Audio) {
        //TODO send the audio to server with reportId
    }

    public void captureText(long reportId, String text) {
        //TODO send the text to server with reportId
    }

    public void updateFriendsMarkersEvery(long millisecond, GoogleMap map) {
        //TODO update Friends Markers Every X seconds
    }

    public void updateMyMarker(long millisecond, GoogleMap map) {
        //TODO update My Marker Every X seconds
    }

    public void sendMyLocationToServerEvery(long millisecond) {
        //TODO update My Marker On server Every X seconds
    }

    public void confirmReport(long reportId) {
        //TODO confirm Report
    }

    public void cancelReport(long reportId) {
        //TODO cancel Report
    }

    @Override
    public boolean onClusterClick(Cluster<Person> cluster) {
        // Show a toast with some info when the cluster is clicked.
        String firstName = cluster.getItems().iterator().next().getUserName();
        Toast.makeText(this, cluster.getSize() + " (" + getString(R.string.including) + " " + firstName + ")", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Person> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(Person item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Person item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }


    private void setCluster(GoogleMap map) {
        LatLng location = new LatLng(User.getLat(), User.getLon());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 9.5f));

        mClusterManager = new ClusterManager<Person>(this, map);
        mClusterManager.setRenderer(new PersonRenderer(map));
        map.setOnCameraChangeListener(mClusterManager);
        map.setOnMarkerClickListener(mClusterManager);
        map.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItemes();
        mClusterManager.cluster();
    }

    private void InitAnalytic() {
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    protected void onResume() {
        super.onResume();
        originalActionbarColor(Report.getmode() == Static.ALERT_MODE ? false : true);
        if (mTracker != null) {
            mTracker.setScreenName("Image~" + "screen name");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    private void setAnalyticEvent(String category, String action) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .build());
    }

    private void animateMap(GoogleMap map, LatLng LatLng, boolean zoomOut) {


        // Move the camera instantly to Sydney with a zoom of 15.
        if (zoomOut) map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng, 11));
        if (!zoomOut) map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng, 15));

      /*  // Zoom in, animating the camera.
             map.animateCamera(CameraUpdateFactory.zoomIn());*/


        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        if (zoomOut) map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        if (!zoomOut) map.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null);
        /*
        // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(LatLng)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                        .bearing(90)                // Sets the orientation of the camera to east
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        */


    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        myChronometer.setBase(SystemClock.elapsedRealtime());
        myChronometer.animate().translationY(0).alpha(1f);
        ;
        myChronometer.start();
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        myChronometer.animate().translationY(myChronometer.getHeight()).alpha(0f);
        myChronometer.stop();
        myChronometer.setBase(SystemClock.elapsedRealtime());
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        System.out.println("uploadAudio");
        uploadAudio();
    }

    private void setUpDrawerNavigation() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout, getActionbarToolbar(), 0, 0) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
    }

    public void uploadAudio() {
        new Vmark().set(reportAudio, R.drawable.mic, false, false);
//        FileUploadService service = ServiceGenerator.createService(FileUploadService.class, FileUploadService.BASE_URL);
        TypedFile typedFile = new TypedFile("audio/3gp", new File(mFileName));
        if (typedFile == null) {
            ToastUtil.toster(getString(R.string.problem_uploading_audio), false);
            return;
        }
        MyApp.networkManager.makeRequest(new UploadAudioRequest(typedFile/*myfile*/), new NetworkCallback<UploadAudioResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<UploadAudioResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    new Vmark().set(reportAudio, R.drawable.mic, true, true);
                    Report report = Report.getCurrentReport();
                    if (report != null) {
                        report.setAudioAttached("N/A");
                        MyApp.tableReports.insertOrUpdate(report);
                    }
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public void uploadImageQuery(long reportId, String Image) {
        new Vmark().set(reportCamera, R.drawable.camera, false, false);
        MyApp.networkManager.makeRequest(new uploadImageRequest(String.valueOf(reportId)/*ReportID*/, Image/*Image*/), new NetworkCallback<uploadImageResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<uploadImageResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    new Vmark().set(reportCamera, R.drawable.camera, true, true);
                    Report report = Report.getCurrentReport();
                    if (report != null) {
                        report.setPhotoAttached("N/A");
                        MyApp.tableReports.insertOrUpdate(report);
                    }
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public void saveReportQuery(final Report report, final String comment) {
        new Vmark().set(reportComment, R.drawable.description, false, false);
        MyApp.networkManager.makeRequest(new saveReportRequest(String.valueOf(Report.getReportID())/*ID*/, comment/*Description*/, User.getLatString()/*Lat*/, User.getLonString()/*Lng*/), new NetworkCallback<saveReportResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<saveReportResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    new Vmark().set(reportComment, R.drawable.description, true, true);
                    Report report = Report.getCurrentReport();
                    if (report != null) {
                        report.setDescription(comment);
                        MyApp.tableReports.insertOrUpdate(report);
                    }
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public void saveNewReportQuery(final Report report) {
        MyApp.networkManager.makeRequest(new saveReportRequest("0"/*ID*/, ""/*Description*/, User.getLatString()/*Lat*/, User.getLonString()/*Lng*/), new NetworkCallback<saveReportResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<saveReportResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    report.setServerId(response.getData().getId());
                    MyApp.tableReports.insertOrUpdate(report);
                    Report.setReportID(response.getData().getId());
                    User.setStartAlarm(); // save on shared preferences
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    public void cancelReport() {
        MyApp.networkManager.makeRequest(new closeRequest(String.valueOf(Report.getReportID())/*ID*/), new NetworkCallback<closeResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ResponseObject<closeResponse> response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    Report report = Report.getCurrentReport();
                    MyApp.tableReports.delete(report.getDatabaseId(), report);
                    Report.setReportID(0);
                    mode(Static.NORMAL_MODE);
                } else {
                    ToastUtil.toster(errorDesc, true);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        super.onBackPressed();
    }

    private class PersonRenderer extends DefaultClusterRenderer<Person> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mIconGenerator2 = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final CircleImageView mCircleClusterImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        public PersonRenderer(GoogleMap map) {
            super(getApplicationContext(), map, mClusterManager);


            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            View multiProfile2 = getLayoutInflater().inflate(R.layout.friend_marker, null);
            mIconGenerator2.setContentView(multiProfile2);
            mCircleClusterImageView = (CircleImageView) multiProfile2.findViewById(R.id.profileImage);


            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);


        }

        @Override
        protected void onBeforeClusterItemRendered(Person person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
//            mImageView.setImageDrawable(person.getProfileImageDrawable());
////            ImageLoader.ByUrl(person.getProfileImage(), mImageView);
//            Bitmap icon = mIconGenerator.makeIcon();
//            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.getUserName());


            mCircleClusterImageView.setImageDrawable(person.getProfileImageDrawable());
            mIconGenerator2.setBackground(new ColorDrawable(Color.TRANSPARENT));
            Bitmap icon = mIconGenerator2.makeIcon();

            if (person.getCategory() == Person.POLICEMAN) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.police_location)).title(person.getUserName());
            } else {
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.getUserName());
            }


//            map.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(icon)));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Person p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
//                Drawable drawable = getResources().getDrawable(R.drawable.avatar);
//                Drawable drawable = new BitmapDrawable(BitmapFactory.decodeByteArray(p.getProfileImageBlob(), 0, p.getProfileImageBlob().length));
                Drawable drawable = getResources().getDrawable(p.getCategory() == 1 ? R.drawable.police_icon_map : R.drawable.friends_icon_map);
                if (p.getProfileImageBlob().length != 0) {
                    drawable = p.getProfileImageDrawable();
                }
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));


        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

}
