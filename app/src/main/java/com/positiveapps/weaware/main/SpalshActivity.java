package com.positiveapps.weaware.main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.objects.Person;
import com.positiveapps.weaware.objects.Report;
import com.positiveapps.weaware.objects.Reports;
import com.positiveapps.weaware.objects.User;
import com.positiveapps.weaware.storage.AppPreference;
import com.positiveapps.weaware.util.Static;

public class SpalshActivity extends Activity implements Runnable, Report.onReportsUpdated, onErrorCallBack {

    private boolean showWarnDialogFlag = false;
    private boolean showDialogFlag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);

        initGoogleCampaign();

        Intent intent = this.getIntent();
        Uri uri = intent.getData();


        System.out.println("AAAAAc " + uri);

        if(MyApp.mMediaPlayer != null){
            MyApp.mMediaPlayer.stop();
        }

        MyApp.tableReports.deleteAll();
        if(!User.isNew()){
            User.updateLocation(SpalshActivity.this);
            Person.getFriends(SpalshActivity.this);
            User.updateFromServer(SpalshActivity.this, new User.onUserUpdate() {
                @Override
                public void userUpdated(Reports reportObj) {
                    if (reportObj != null) {
                        Report.setReportID(Long.parseLong(reportObj.getId()));
                        Report.setMode(Static.ALERT_MODE);
                        User.setStartAlarm(reportObj.getCreated());
                    } else {
                        Report.setReportID(0);
                        Report.setMode(Static.NORMAL_MODE);
                    }

                    Report.getReports(SpalshActivity.this, SpalshActivity.this); // interface
                }
            }, SpalshActivity.this);
//            showDialog("Welcome Back " + User.getUserName(), "");
        }else{
            warnDialog(getString(R.string.welcome_new_user));
        }

    }



    public void showDialog(final String title, final String msg) {

            if (!showDialogFlag) {
                startActivity(new Intent(SpalshActivity.this, MainActivity.class));
                return;
            }

            if (isFinishing()) {return;}

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    new MaterialDialog.Builder(SpalshActivity.this)
                            .title(title)
                            .content(msg)
                            .positiveText("continue")
                            .negativeText("start as A new User")
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    User.updateLocation(SpalshActivity.this);
                                    Person.getFriends(SpalshActivity.this);
                                    User.updateFromServer(SpalshActivity.this, new User.onUserUpdate() {
                                        @Override
                                        public void userUpdated(Reports reportObj) {
                                            if (reportObj != null) {
                                                Report.setReportID(Long.parseLong(reportObj.getId()));
                                                Report.setMode(Static.ALERT_MODE);
                                            } else {
                                                Report.setReportID(0);
                                                Report.setMode(Static.NORMAL_MODE);
                                            }

                                            Report.getReports(SpalshActivity.this, SpalshActivity.this); // interface
                                        }
                                    }, SpalshActivity.this);

                                }

                                @Override
                                public void onNegative(MaterialDialog dialog) {
                                    AppPreference.removeInstance();
                                    new Handler().postDelayed(SpalshActivity.this, 1000);
                                }
                            })
                            .show();
                }
            });
    }


    public void warnDialog(final String msg) {
        if (!showWarnDialogFlag) {
            new Handler().postDelayed(this, 1000);
            return;
        }

        if (isFinishing())
            return;

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(SpalshActivity.this)
                        .title("New User")
                        .content(msg)
                        .positiveText(android.R.string.ok)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                new Handler().postDelayed(SpalshActivity.this, 1000);
                            }
                        })
                        .show();
            }
        });
    }

    public void serverErrorDialog(final String msg) {

        if (isFinishing())
            return;

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(SpalshActivity.this)
                        .title(getString(R.string.server_error))
                                .content(msg)
                                .positiveText(getString(R.string.server_error))
                                .callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {
                                        Intent intent = getIntent();
                                        finish();
                                        startActivity(intent);
                                    }
                                })
                                .show();
            }
        });
    }

    public void progressDialog() {
        if (isFinishing()) return;

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                new MaterialDialog.Builder(MyApp.appContext)
                        .title(R.string.app_name)
                        .content(R.string.please_wait)
                        .progress(true, 0)
                        .show();
            }
        });
    }


    @Override
    public void run() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void reportsUpdated() {
        Intent intent = new Intent(SpalshActivity.this, MainActivity.class);
        if(getIntent().getExtras() != null){
            long serverID = getIntent().getExtras().getLong(Static.REPORT_SERVER_ID);
            int pushType = getIntent().getExtras().getInt(Static.PUSH_TYPE);
            intent.putExtra(Static.REPORT_SERVER_ID, serverID);
            intent.putExtra(Static.PUSH_TYPE, pushType);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void onError(String error) {
        serverErrorDialog(error);
    }

    private void initGoogleCampaign(){
        // Get tracker.
        MyApp application = (MyApp) getApplication();
        Tracker t = application.getDefaultTracker();
// Set screen name.
        t.setScreenName("salamat");

// In this example, campaign information is set using
// a url string with Google Analytics campaign parameters.
// Note: This is for illustrative purposes. In most cases campaign
//       information would come from an incoming Intent.
        String campaignData = "http://examplepetstore.com/index.html?" +
                "utm_source=email&utm_medium=email_marketing&utm_campaign=summer" +
                "&utm_content=email_variation_1";

// Campaign data sent with this hit.
        t.send(new HitBuilders.ScreenViewBuilder()
                        .setCampaignParamsFromUrl(campaignData)
                        .build()
        );
    }


}


