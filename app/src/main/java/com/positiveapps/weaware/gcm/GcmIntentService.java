/**
 * 
 */
package com.positiveapps.weaware.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.main.SpalshActivity;
import com.positiveapps.weaware.util.Static;

import java.util.Set;


/**
 * @author Nati Application
 *
 */
public class GcmIntentService extends IntentService {
	
	
	public static final int NOTIFICATION_ID = 1;
	
	public static final String TAG = "pushNotificationTag";

	NotificationCompat.Builder builder;
	
	

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);
		
		Log.e(GcmIntentService.TAG,"**********************");
		Log.e(GcmIntentService.TAG,"new push notification");
		
		if (extras != null){
			Log.e(GcmIntentService.TAG,"new push notification " + describePushContent(extras));
			int pushType = dettectPushType (extras);
			switch (pushType){
				case Static.OPEN_EVENT:
					handelNotification(extras, Static.OPEN_EVENT);
					break;
				case Static.CLOSE_EVENT:
					handelNotification(extras, Static.CLOSE_EVENT);
					break;
				case Static.CONFIRM_EVENT:
					handelNotification(extras, Static.CONFIRM_EVENT);
					break;
			}


		}else{
			Log.i(GcmIntentService.TAG,"no extras");
		}
		
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}


	/**
	 * @param extras
	 * @return
	 */





	private int dettectPushType(Bundle extras) {
		String code = extras.getString("CategoryID");
		try {
			return Integer.parseInt(code);
		} catch (Exception e) {
			int intCode = extras.getInt("CategoryID");
			return intCode;
		}
	}
	
	private String describePushContent(Bundle extras) {
		StringBuilder builder = new StringBuilder();

		Set<String> keys = extras.keySet();
		for (String key : keys) {
			builder.append(key + " --- " + extras.get(key));
			builder.append("\n");
			
		}
		return builder.toString();
	}

	/*public void showLoadingDialog(String text, Activity activity){
		Dialog loadingDialog = new Dialog(activity, R.style.loadingDialog);
		final View view = activity).getLayoutInflater().inflate(R.layout.dialog_loading, null);
		((TextView) view.findViewById(R.id.loading_text)).setText(text);
		loadingDialog.setContentView(view);
		loadingDialog.setCancelable(false);
		loadingDialog.show();
	}*/

	private void handelNotification (Bundle extras, int pushType){


		String message = extras.getString("message");
		if(message == null){
			Log.i(TAG, "handelNotification ---------- (message == null)");return;
		}
		String reportID = extras.getString("ReportID");
		if(reportID == null){
			Log.i(TAG, "handelNotification ---------- (reportID == null)");return;
		}

		NotificationManager mNotificationManager = (NotificationManager) MyApp.appContext
				.getSystemService(Context.NOTIFICATION_SERVICE);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MyApp.appContext)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
				.setContentTitle(MyApp.appContext.getString(R.string.app_name))
				.setContentText(message)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
//				.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION),
//						AudioManager.STREAM_NOTIFICATION).setAutoCancel(true)

		if(pushType == Static.OPEN_EVENT) {
			MyApp.mMediaPlayer = new MediaPlayer();
			MyApp.mMediaPlayer = MediaPlayer.create(this, R.raw.beep);
			MyApp.mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			MyApp.mMediaPlayer.start();
		}

		Intent intent = new Intent(MyApp.appContext,SpalshActivity.class);
		intent.putExtra(Static.REPORT_SERVER_ID, Long.parseLong(reportID));
		intent.putExtra(Static.PUSH_TYPE, pushType);

		PendingIntent contentIntent = PendingIntent.getActivity(
				MyApp.appContext, 0, intent,
				Intent.FLAG_ACTIVITY_NEW_TASK);

		mBuilder.setContentIntent(contentIntent);

		Notification notification = mBuilder.build();
		notification.flags |= Notification.FLAG_SHOW_LIGHTS| Notification.FLAG_AUTO_CANCEL;

		mNotificationManager.notify(NOTIFICATION_ID, notification);

	}

}
