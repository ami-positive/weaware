/**
 * 
 */
package com.positiveapps.weaware.gcm;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.positiveapps.weaware.R;
import com.positiveapps.weaware.main.MyApp;
import com.positiveapps.weaware.network.NetworkCallback;
import com.positiveapps.weaware.network.requests.setPushTokenRequest;
import com.positiveapps.weaware.network.response.ResponseObject;
import com.positiveapps.weaware.network.response.setPushTokenResponse;
import com.positiveapps.weaware.util.ToastUtil;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author Nati Gabay
 *
 */
public class GcmManager {
	
	// keys for saved registration id in preference
	public static final String KEY_REG_PREFERECE = "RegistrationPref";
	public static final String KEY_REG_ID = "RegistrationID";
	
	// GCM client sender id
	private String SENDER_ID;
	
	// context of current application
	private Context context;
	
	// google cloud messaging manger object that  handle the communication whit GCM API
	private GoogleCloudMessaging googleCloudMessaging;
	
	// single instance from this class
	private static GcmManager instance;
	
	// registration id which accepted from GCM registration
	private String registrationID;
	
	// atomic integer to create unique id for the outgoing messages
	AtomicInteger msgId = new AtomicInteger();
	
	
	/**
	 * private constructor that initial the instance parameters
	 * 
	 * @param context - context of current application
	 */
	private GcmManager (Context context){
		this.context = context;
		this.SENDER_ID = context.getString(R.string.gcm_id);
		googleCloudMessaging = GoogleCloudMessaging.getInstance(context);
		this.registrationID = getRegistrationIDFromPrefernce();
	}
	
	
	/**
	 * implementation of singleton design pattern
	 * this static method used to get the single instance from this class
	 * 
	 * @param context - context of current application
	 * @return the single instace from this class
	 */
	public static GcmManager getInstace (Context context){
		if (instance == null){
			instance = new GcmManager(context);
		}
		return instance;
	}
	
	/**
	 * remove the single instance when application has destroyed
	 */
	public  void removeInstance (){
		instance = null;
	}
	
	
	/**
	 * save the specify registration id to application preference
	 * 
	 * @param registrationId - to save
	 */
	private void saveRegistrationIDToPreference (String registrationId){
		MyApp.generalSettings.setGCMKey(registrationId);
	}
	
	/**
	 * load the registration id from the application preference when the instance has initiated
	 * 
	 * @return the registration id saved in preferences or empty string if is dose not saved yet
	 */
	private String getRegistrationIDFromPrefernce () {
		
		return MyApp.generalSettings.getGCMKey();
	}
	
	/**
	 * indicate if has registration id saved at now
	 * 
	 * @return true if registration id is not empty string
	 */
	public boolean isRegistrationIDEmpty () {
		if (this.registrationID.isEmpty()){
			return true;
		}
		return false;
	}
	
	
	/**
	 * in case that there is not registration id yet, we performing the registration process 
	 * and we get the registration id to save it into application preference.
	 * when the registration id has excepted we send it to the backup server
	 */
	public void registerInBackground() {
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
	            try {
	              registrationID = googleCloudMessaging.register(SENDER_ID);


					//TODO send gcm key registration to server side

	            } catch (IOException ex) {}finally{
	            	Log.i("gcmLog", "registeration id is : " + registrationID);
	            	saveRegistrationIDToPreference(registrationID);
					updateGcmQuery(registrationID);
	            }
			}
		}).start();
	}

	private void updateGcmQuery(String registrationID) {
		MyApp.networkManager.makeRequest(new setPushTokenRequest(registrationID/*PushToken*/), new NetworkCallback<setPushTokenResponse>() {
			@Override
			public void onResponse(boolean success, String errorDesc, ResponseObject<setPushTokenResponse> response) {
				super.onResponse(success, errorDesc, response);
				if (success) {

				} else {
					ToastUtil.toster(errorDesc, true);
				}
			}
		});
	}
	 
	/**
	 * @return the registrationID
	 */
	public String getRegistrationID() {
		return registrationID;
	}

	/**
	 * @param registrationID the registrationID to set
	 */
	public void setRegistrationID(String registrationID) {
		this.registrationID = registrationID;
	}
	
	
	
	

}
