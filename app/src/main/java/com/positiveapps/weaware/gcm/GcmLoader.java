package com.positiveapps.weaware.gcm;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by Izakos on 19/10/2015.
 */
public class GcmLoader {

    /* GCM final Variables*/
    private static final String TAG = "GCM log";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Activity activity;

    public GcmLoader(Activity activity) {
        this.activity = activity;

        if (checkPlayServices()) {
            checkForGCMRegistrationID();
        }

    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    private void checkForGCMRegistrationID() {
        // if(WingsApplication.getGcmManager().isRegistrationIDEmpty()){
        GcmManager.getInstace(activity).registerInBackground();
        // }
    }

    public static GcmLoader set(Activity activity){
        return new GcmLoader(activity);
    }
}
